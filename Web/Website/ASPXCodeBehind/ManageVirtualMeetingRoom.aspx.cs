﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Specialized;//ZD 100263
using System.Collections.Generic;


namespace ns_MyVRM
{
    public partial class en_ManageVirtualMeetingRoom : System.Web.UI.Page
    {
        #region protected Members
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.Label errLabel;
        //protected System.Web.UI.WebControls.Label lblMUser;//ZD 100522
        //protected System.Web.UI.WebControls.Label lblMdate;

        protected System.Web.UI.WebControls.TextBox txtRoomName;
        //protected System.Web.UI.WebControls.TextBox Assistant;
        protected System.Web.UI.WebControls.TextBox txtExternalnum;
        protected System.Web.UI.WebControls.TextBox txtInternalnum;
        protected System.Web.UI.WebControls.TextBox txtVMRLink;//FB 2727

        protected System.Web.UI.WebControls.ListBox DepartmentList;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMultipleDept;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden AssistantID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Approver3ID, Approver4ID, Approver5ID;//ALLDEV-807
        protected System.Web.UI.WebControls.TextBox Approver3, Approver4, Approver5; //ALLDEV-807
		//FB 2994 Start
        protected System.Web.UI.HtmlControls.HtmlInputFile roomfileimage; 
        protected System.Web.UI.WebControls.Button BtnUploadRmImg;
        protected System.Web.UI.WebControls.DataGrid dgItems;
        protected System.Web.UI.WebControls.Button btnUploadImages; 
		//FB 2994 End

        //FB 2670
        protected System.Web.UI.WebControls.Button btnSubmitAddNew;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.Button btnReset;//ZD 100263
        //ZD 100753 Starts
        protected System.Web.UI.WebControls.TextBox txtPassword1;
        protected System.Web.UI.WebControls.TextBox txtPassword2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPasschange;
        //ZD 100753 Ends
        //FB 2994 Start
        string sessionKey = "";
        string rmImg = "";
        DataTable roomImageDt;
        String fName; 
        string fileext;
        String pathName;
        myVRMNet.ImageUtil imageUtilObj = null;
        byte[] imgArray = null;
        //FB 2994 End

        //ZD 100664 Starts
        bool hasReports = false;
        protected System.Web.UI.WebControls.DataGrid dgChangedHistory;
        protected System.Web.UI.HtmlControls.HtmlTableRow trHistory;
        //ZD 100664 End

        //ZD 100522 Start
        protected System.Web.UI.WebControls.TextBox txtVMRID;
        protected System.Web.UI.WebControls.DropDownList lstMCU;
        protected System.Web.UI.WebControls.CheckBox chkCreateMCU;
        protected System.Web.UI.WebControls.Button btnGenPassword;
        protected System.Web.UI.WebControls.CheckBox chkCreateTemplate;
        protected System.Web.UI.WebControls.CheckBox ChkDialoutLoc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPermanetConfid;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVRMConfid;
        protected System.Web.UI.HtmlControls.HtmlSelect RoomList;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLocation; 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDialLoc; 
        protected System.Web.UI.HtmlControls.HtmlTableCell tdVMRLink;
        protected int DefaultConfDuration = 0;
        protected int PasswordCharLength = 0;
        //ZD 100522 End
        //ZD 103263 Start
        protected System.Web.UI.WebControls.CheckBox chkEnableBJN;
        protected System.Web.UI.HtmlControls.HtmlInputHidden AssistantEmail;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVMRIntExtNumber;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableBJN;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdcreateMCU;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdchkMCU;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEPID;
        //ZD 103263 End
        #endregion

        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log = null;

        protected String language = "";
        protected String dtFormatType = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        //protected Enyim.Caching.MemcachedClient memClient = null;//ZD 103496 //ZD 104482
        //protected int memcacheEnabled = 0;//ZD 103496

        public en_ManageVirtualMeetingRoom()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            imageUtilObj = new myVRMNet.ImageUtil(); //FB 2994
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("ManageVirtualMeetingRoom.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            lblTitle.Text = obj.GetTranslatedText("Virtual Meeting Room");//FB 2994

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() == "")
                    Session["FormatDateType"] = "MM/dd/yyyy";
                else
                    dtFormatType = Session["FormatDateType"].ToString();
            }
            //FB 2670
            if (Session["admin"].ToString() == "3")
            {
                
                //btnSubmit.ForeColor = System.Drawing.Color.Gray;
                //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796

                
                //btnSubmitAddNew.ForeColor = System.Drawing.Color.Gray;
                //btnSubmitAddNew.Attributes.Add("Class", "btndisable");// FB 2796
                //ZD 100263
                btnSubmit.Visible = false;
                btnSubmitAddNew.Visible = false;
                btnReset.Visible = false;
            }
            //FB 2796 Start
            else
            {
                btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                btnSubmitAddNew.Attributes.Add("Class", "altMedium0BlueButtonFormat");
            }
            //FB 2796 End



            if (Session["language"] == null)
                Session["language"] = "en";

            if (Session["language"].ToString() != "")
                language = Session["language"].ToString();

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");


            //ZD 100664 start
            if (Session["timeFormat"].ToString().Equals("2"))
                tformat = "HHmmZ";

            //ZD 103496 //commented for ZD 104482
            //if (Session["MemcacheEnabled"] != null && !string.IsNullOrEmpty(Session["MemcacheEnabled"].ToString()))
            //    int.TryParse(Session["MemcacheEnabled"].ToString(), out memcacheEnabled);

            string[] mary = Session["sMenuMask"].ToString().Split('-');
            string[] mmary = mary[1].Split('+');
            string[] ccary = mary[0].Split('*');
            int topMenu = Convert.ToInt32(ccary[1]);
            int adminMenu = Convert.ToInt32(mmary[10].Split('*')[1]); // Convert.ToInt32(mmary[1].Split('*')[1]); ZD 101233 103095

            if (Convert.ToBoolean(topMenu & 16))
            {
                hasReports = Convert.ToBoolean(adminMenu & 4);
            }

            if (Session["admin"] != null)
            {
                if (Session["admin"].ToString().Equals("2") && (hasReports == true))
                {
                    trHistory.Attributes.Add("Style", "visibility:visible;");
                }
                else
                {
                    trHistory.Attributes.Add("Style", "visibility:hidden;");
                }
            }
            //ZD 100664 End
            //ZD 100522 Starts
            if (Session["DefaultConfDuration"] != null)
                int.TryParse(Session["DefaultConfDuration"].ToString(), out DefaultConfDuration);

            if (Session["PasswordCharLength"] != null)
                int.TryParse(Session["PasswordCharLength"].ToString(), out PasswordCharLength);
            if (PasswordCharLength <= 0)
                PasswordCharLength = 4; //Default
            //ZD 100522 End

            //ZD 103263 Start
            if (chkEnableBJN.Checked == true)
            {
                trVMRIntExtNumber.Attributes.Add("Style", "visibility:hidden;");
                tdcreateMCU.Attributes.Add("Style", "visibility:hidden;");
                tdchkMCU.Attributes.Add("Style", "visibility:hidden;");
                chkCreateMCU.Checked = false;
            }
            else
            {
                trVMRIntExtNumber.Attributes.Add("Style", "visibility:visible;");
                tdcreateMCU.Attributes.Add("Style", "visibility:visible;");
                tdchkMCU.Attributes.Add("Style", "visibility:visible;");
            }

            if (chkCreateMCU.Checked == true)
            {
                trVMRIntExtNumber.Attributes.Add("Style", "visibility:visible;");
                trEnableBJN.Attributes.Add("Style", "visibility:hidden;");
                chkEnableBJN.Checked = false;
            }
            else
            {
                trEnableBJN.Attributes.Add("Style", "visibility:visible;");
            }
			// ZD 103550
            if (Session["EnableBlueJeans"] != null && Session["EnableBlueJeans"].ToString() == "1")
                trEnableBJN.Attributes.Add("Style", "visibility:visible;");
            else
                trEnableBJN.Attributes.Add("Style", "visibility:hidden;");

            //ZD 103263 End

            if (!IsPostBack) //if page loads for the first time
            {
                if (Request.QueryString["rID"] != null)
                    hdnRoomID.Value = Request.QueryString["rID"].ToString();
                else
                {
                    ViewState["ConfTemplateId"] = "new";//ZD 100522
                    hdnRoomID.Value = "new";
                }

                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.ShowSuccessMessage();
                        errLabel.Visible = true;
                    }
                
                //Assistant.Attributes.Add("readonly", "");
                Approver3.Attributes.Add("readonly", "");
                Approver4.Attributes.Add("readonly", ""); //ALLDEV-807
                Approver5.Attributes.Add("readonly", "");

                obj.BindBridges(lstMCU); //ZD 100522

                LoadDepartments();
                
                if (hdnRoomID.Value.ToLower().Equals("new")) //ZD 100753
                {
                    txtPassword1.Style.Add("background-image", "");
                    txtPassword2.Style.Add("background-image", "");
                    Session.Add("VMRPW", "");
                }
               
                if (!hdnRoomID.Value.ToLower().Equals("new"))
                    LoadRoomProfile();
                else
                    trHistory.Attributes.Add("Style", "visibility:hidden;"); //ZD 100664
            }
			//ZD 103263 Start
            if (hdnPasschange.Value == "true")
            {
                txtPassword1.Style.Add("background-image", "");
                txtPassword2.Style.Add("background-image", "");
            }
			//ZD 103263 End
            if (hdnRoomID.Value.ToLower().Equals("new"))
            {
                if (!IsPostBack) // FB 2586
                    lblTitle.Text = obj.GetTranslatedText("Create New") + " " + lblTitle.Text; //ZD 100288
            }
            else
                lblTitle.Text = obj.GetTranslatedText("Edit") + " " + lblTitle.Text; //ZD 100288
            //FB 2670
            if (Session["admin"].ToString() == "3")
                lblTitle.Text = obj.GetTranslatedText("View Virtual Meeting Rooms");

            //ZD 100522 Starts
            if (hdnDialLoc.Value != "") 
            {
                locstrname.Value = hdnDialLoc.Value; 
                BindRoomToList();
                hdnLocation.Value = selectedloc.Value;
            }
            //ZD 100522 Ends
        }

        #region LoadDepartments
        /// <summary>
        /// LoadDepartments
        /// </summary>
        protected void LoadDepartments()
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<GetDepartmentsForLocation>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("  <UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("</GetDepartmentsForLocation>");

                String outXML = obj.CallMyVRMServer("GetDepartmentsForLocation", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    hdnMultipleDept.Value = xmldoc.SelectSingleNode("//GetDepartmentsForLocation/multiDepartment").InnerText;
                    if (hdnMultipleDept.Value == "1")
                    {
                        XmlNodeList nodes = xmldoc.SelectNodes("//GetDepartmentsForLocation/departments/department");
                        if (nodes.Count > 0)
                            obj.LoadList(DepartmentList, nodes, "id", "name");
                    }
                }
                else
                {
                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                        errLabel.Text = obj.ShowErrorMessage(outXML);

                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadEndpoints: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region LoadRoomProfile
        /// <summary>
        /// LoadRoomProfile
        /// </summary>
        protected void LoadRoomProfile()
        {
            DateTime deactDate = DateTime.MinValue;
            Session.Add("VMRPW", ""); //ZD 100753
            XmlNode node = null;//ZD 100522
            try
            {
                string inXML = "<GetRoomProfile>";
                inXML += obj.OrgXMLElement();
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <RoomID>" + hdnRoomID.Value + "</RoomID>";
                inXML += "</GetRoomProfile>";
                
                string outXML = obj.CallMyVRMServer("GetRoomProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                //ZD 100664 Start
                XmlNodeList changedHistoryNodes = xmldoc.SelectNodes("//GetRoomProfile/LastModifiedDetails/LastModified");
                LoadHistory(changedHistoryNodes);
                //ZD 100664 End

                txtRoomName.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomName").InnerText;
                hdnRoomID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/RoomID").InnerText;

                //ZD 100522 Start
                node = xmldoc.SelectSingleNode("//GetRoomProfile/RoomVMRID");
                if (node != null)
                    txtVMRID.Text = node.InnerText;

                node = xmldoc.SelectSingleNode("//GetRoomProfile/PermanetConfid");
                if (node != null)
                    hdnPermanetConfid.Value = node.InnerText;

                node = xmldoc.SelectSingleNode("//GetRoomProfile/VRMConfid");
                if (node != null)
                    hdnVRMConfid.Value = xmldoc.SelectSingleNode("//GetRoomProfile/VRMConfid").InnerText;

                int IsChecked = 0;
                node = xmldoc.SelectSingleNode("//GetRoomProfile/IsCreateOnMCU");
                if (node != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetRoomProfile/IsCreateOnMCU").InnerText, out IsChecked);
                if (IsChecked == 1)
                    chkCreateMCU.Checked = true;
                else
                    chkCreateMCU.Checked = false;

                IsChecked = 0;
                node = xmldoc.SelectSingleNode("//GetRoomProfile/IsCreateTemplate");
                if (node != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetRoomProfile/IsCreateTemplate").InnerText, out IsChecked);
                if (IsChecked == 1)
                    chkCreateTemplate.Checked = true;
                else
                    chkCreateTemplate.Checked = false;

                IsChecked = 0;
                node = xmldoc.SelectSingleNode("//GetRoomProfile/IsDialOutLoc");
                if (node != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetRoomProfile/IsDialOutLoc").InnerText, out IsChecked);
                if (IsChecked == 1)
                    ChkDialoutLoc.Checked = true;
                else
                    ChkDialoutLoc.Checked = false;

                //ZD 103263 Start
                IsChecked = 0;
                node = xmldoc.SelectSingleNode("//GetRoomProfile/EnableBJN");
                if (node != null)
                    int.TryParse(xmldoc.SelectSingleNode("//GetRoomProfile/EnableBJN").InnerText, out IsChecked);
                if (IsChecked == 1)
                    chkEnableBJN.Checked = true;
                else
                    chkEnableBJN.Checked = false;

                if (chkEnableBJN.Checked)
                {
                    chkCreateMCU.Checked = false;
                    trEnableBJN.Attributes.Add("style", "visibility:visible;");
                    trVMRIntExtNumber.Attributes.Add("style", "visibility:hidden;");
                    tdcreateMCU.Attributes.Add("style", "visibility:hidden;");
                    tdchkMCU.Attributes.Add("style", "visibility:hidden;");
                }

                node = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointID");
                if (node != null)
                    hdnEPID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointID").InnerText;
                else
                    hdnEPID.Value = "0";

                //ZD 103263 End

                lstMCU.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetRoomProfile/BridgeId") != null)
                    lstMCU.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/BridgeId").InnerText.Trim()).Selected = true;

                int ConfTemplateId = 0;
                node = xmldoc.SelectSingleNode("//GetRoomProfile/ConfTemplateId");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out ConfTemplateId);
                if (ConfTemplateId > 0)
                    ViewState["ConfTemplateId"] = ConfTemplateId;
                else
                    ViewState["ConfTemplateId"] = "new";

                if (chkCreateMCU.Checked)
                {
                    txtRoomName.Enabled = false;
                    chkCreateMCU.Enabled = false;
                    lstMCU.Enabled = false;
                    txtRoomName.CssClass = txtRoomName.CssClass.Remove(0);
                    //ZD 103263 Start
                    chkEnableBJN.Visible = false;
                    chkEnableBJN.Checked = false;
                    trVMRIntExtNumber.Attributes.Add("style", "visibility:visible;");
                    trEnableBJN.Attributes.Add("style", "visibility:hidden;");
                    //ZD 103263 End
                }
                int BridgeTypeId = 0;
                node = xmldoc.SelectSingleNode("//GetRoomProfile/BridgeTypeId");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out BridgeTypeId);

                if (BridgeTypeId == 4 || BridgeTypeId == 5 || BridgeTypeId == 6 || BridgeTypeId == 12 || BridgeTypeId == 15)
                {
                    tdVMRLink.Visible = false;
                    txtVMRLink.Visible = false;
                }
                else
                {
                    tdVMRLink.Visible = true;
                    txtVMRLink.Visible = true;
                }
                //ZD 100522 End

                if (xmldoc.SelectSingleNode("//GetRoomProfile/UserStaus") != null)
                {
                    errLabel.Text = "The Assistant-In-Charge \"" + xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText + "\" is not found in Active User List.";//FB 2974
                    errLabel.Visible = true;
                }
                else
                {
                    //ALLDEV-807 Starts
                    //AssistantID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeID").InnerText;
                    //Assistant.Text = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText;
                    //AssistantEmail.Value = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeEmail").InnerText;//ZD 103263

                    XmlNodeList nodesloc = xmldoc.SelectNodes("//GetRoomProfile/Assistants/Assistant");
                    int cn = 1;
                    for (int i = 0; i < nodesloc.Count; i++)
                    {
                        if (cn == 1)
                        {
                            if (nodesloc[i].SelectSingleNode("AssistantInchargeID") != null)
                                Approver3ID.Value = nodesloc[i].SelectSingleNode("AssistantInchargeID").InnerText;
                            if (nodesloc[i].SelectSingleNode("AssistantInchargeName") != null)
                                Approver3.Text = nodesloc[i].SelectSingleNode("AssistantInchargeName").InnerText;
                        }
                        else if (cn == 2)
                        {
                            if (nodesloc[i].SelectSingleNode("AssistantInchargeID") != null)
                                Approver4ID.Value = nodesloc[i].SelectSingleNode("AssistantInchargeID").InnerText;
                            if (nodesloc[i].SelectSingleNode("AssistantInchargeName") != null)
                                Approver4.Text = nodesloc[i].SelectSingleNode("AssistantInchargeName").InnerText;
                        }
                        else if (cn == 3)
                        {
                            if (nodesloc[i].SelectSingleNode("AssistantInchargeID") != null)
                                Approver5ID.Value = nodesloc[i].SelectSingleNode("AssistantInchargeID").InnerText;
                            if (nodesloc[i].SelectSingleNode("AssistantInchargeName") != null)
                                Approver5.Text = nodesloc[i].SelectSingleNode("AssistantInchargeName").InnerText;
                        }
                        cn++;
                    }

                   //ALLDEV-807 Ends
                }


                //FB 2994 starts here...
                rmImg = "";
                XmlNodeList rmImgNodes = xmldoc.SelectNodes("//GetRoomProfile/RoomImages/ImageDetails");

                if (rmImgNodes != null)
                    LoadRoomImageGrid(rmImgNodes);
                //FB 2994 End here...


                XmlNodeList nodes = xmldoc.SelectNodes("//GetRoomProfile/Departments/Department");
                Int32 c = nodes.Count;
                for (Int32 d = 0; d < nodes.Count; d++)
                {
                    foreach (ListItem item in DepartmentList.Items)
                    {
                        if (c == 0)
                            break;
                        if (item.Value == nodes[d].SelectSingleNode("ID").InnerText)
                        {
                            item.Selected = true;
                            c--;
                            break;
                        }
                    }
                }

                if (xmldoc.SelectSingleNode("//GetRoomProfile/InternalNumber") != null)
                    txtInternalnum.Text = xmldoc.SelectSingleNode("//GetRoomProfile/InternalNumber").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetRoomProfile/ExternalNumber") != null)
                    txtExternalnum.Text = xmldoc.SelectSingleNode("//GetRoomProfile/ExternalNumber").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetRoomProfile/VMRLink") != null)//FB 2727
                    txtVMRLink.Text = xmldoc.SelectSingleNode("//GetRoomProfile/VMRLink").InnerText.Trim().Replace("&amp;", "&");

                Session.Remove("VMRPW");
                if (xmldoc.SelectSingleNode("//GetRoomProfile/Password") != null)//ZD 100753
                {
                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Password").InnerText.Trim() != "")
                        Session["VMRPW"] = xmldoc.SelectSingleNode("//GetRoomProfile/Password").InnerText;

                    //txtPassword1.Attributes.Add("value", Session["VMRPW"].ToString());
                    //txtPassword2.Attributes.Add("value", Session["VMRPW"].ToString());
                }
                /*if (xmldoc.SelectSingleNode("//GetRoomProfile/LastModifiedDate") != null)
                {
                    DateTime.TryParse(xmldoc.SelectSingleNode("//GetRoomProfile/LastModifiedDate").InnerText, out deactDate);

                    if (deactDate != DateTime.MinValue)
                        lblMdate.Text = deactDate.ToString(dtFormatType) + " " + deactDate.ToString(tformat);
                }
                lblMUser.Text = xmldoc.SelectSingleNode("//GetRoomProfile/LastModififeduserName").InnerText;*/
                XmlNodeList nds = xmldoc.SelectNodes("GetRoomProfile/LocationList/Location");

                if (nds != null && nds.Count > 0)
                {
                    XmlNode roomNm = null, roomID = null;
                    foreach (XmlNode nd in nds)
                    {
                        roomNm = nd.SelectSingleNode("LocationName");
                        roomID = nd.SelectSingleNode("LocationId");

                        if (roomNm != null && roomID != null)
                        {
                            if (selectedloc.Value == "")
                            {
                                selectedloc.Value = roomID.InnerText;
                                locstrname.Value = roomID.InnerText + "|" + roomNm.InnerText;
                            }
                            else
                            {
                                selectedloc.Value += "," + roomID.InnerText;
                                locstrname.Value += "++" + roomID.InnerText + "|" + roomNm.InnerText;
                            }
                        }
                    }
                    BindRoomToList();
                    hdnLocation.Value = selectedloc.Value;
                    if (!string.IsNullOrEmpty(hdnLocation.Value))
                        ChkDialoutLoc.Checked = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadRoomProfile: " + ex.StackTrace);
            }
        }
        #endregion

        #region SetRoomProfile
        /// <summary>
        /// SetRoomProfile
        /// </summary>
        /// <returns></returns>
        protected void SetRoomProfile(Object sender,EventArgs e)
        {
            XmlDocument createRooms = null;
            XmlDocument loadRooms = null;
            String roomxmlPath = "";//Room search
            XmlNodeList roomList = null;
            string roomId = "", RoomAdminEmail = "";//ZD 103263 
            XmlNode ndeRoomID = null;
            StringBuilder inXML = new StringBuilder();
            if (Session["VMRPW"] == null)//ZD 100753
                Session.Add("VMRPW", "");
            try
            {
				//ZD 100522 Starts
                if (chkCreateMCU.Checked || ChkDialoutLoc.Checked)
                {
                    if (lstMCU.SelectedIndex <= 0)
                    {
                        errLabel.Text = obj.GetTranslatedText("Please select MCU.");
                        errLabel.Visible = true;
                        return;
                    }

                    if (ChkDialoutLoc.Checked)
                    {
                        if (string.IsNullOrWhiteSpace(selectedloc.Value))
                        {
                            errLabel.Text = obj.GetTranslatedText("Please select Dial-out to Locations.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                }
				//ZD 100522 End
                //ZD 103263 Start
                if (chkEnableBJN.Checked)
                {
                    if (hdnPasschange.Value != "" && hdnPasschange.Value == "true")
                    {
                        if (string.IsNullOrEmpty(txtPassword1.Text))
                        {
                            errLabel.Text = obj.GetTranslatedText("Please enter Virtual Meeting Room password");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Session["VMRPW"].ToString()))
                        {
                            errLabel.Text = obj.GetTranslatedText("Please enter Virtual Meeting Room password");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                }
                //ZD 103263 End
                inXML.Append("<SetRoomProfile>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<RoomID>" + hdnRoomID.Value + "</RoomID>");
                inXML.Append("<RoomName>" + txtRoomName.Text + "</RoomName>");
                inXML.Append("<RoomPhoneNumber></RoomPhoneNumber>");
                inXML.Append("<MaximumCapacity>0</MaximumCapacity>");
                inXML.Append("<MaximumConcurrentPhoneCalls>0</MaximumConcurrentPhoneCalls>");
                //inXML.Append("<SetupTime>0</SetupTime>");//ZD 101563
                //inXML.Append("<TeardownTime>0</TeardownTime>");
                //ALLDEV-807 Starts
                //inXML.Append("<AssistantInchargeID>" + AssistantID.Value + "</AssistantInchargeID>");
                //inXML.Append("<AssistantInchargeEmail>" + AssistantEmail.Value + "</AssistantInchargeEmail>");//ZD 103263
                inXML.Append("<Assistants>");
                inXML.Append("<Assistant>");
                inXML.Append("<AssistantInchargeID>" + Approver3ID.Value + "</AssistantInchargeID>");
                inXML.Append("<AssistantInchargeEmail></AssistantInchargeEmail>");
                inXML.Append("<AssistantInchargeName>" + Approver3.Text + "</AssistantInchargeName>");
                inXML.Append("</Assistant><Assistant>");
                inXML.Append("<AssistantInchargeID>" + Approver4ID.Value + "</AssistantInchargeID>");
                inXML.Append("<AssistantInchargeEmail></AssistantInchargeEmail>");
                inXML.Append("<AssistantInchargeName>" + Approver4.Text + "</AssistantInchargeName>");
                inXML.Append("</Assistant><Assistant>");
                inXML.Append("<AssistantInchargeID>" + Approver5ID.Value + "</AssistantInchargeID>");
                inXML.Append("<AssistantInchargeEmail></AssistantInchargeEmail>");
                inXML.Append("<AssistantInchargeName>" + Approver5.Text + "</AssistantInchargeName>");
                inXML.Append("</Assistant></Assistants>"); 

                //ALLDEV-807 Ends
                inXML.Append("<AssistantInchargeName></AssistantInchargeName>");
                inXML.Append("<MultipleAssistantEmails></MultipleAssistantEmails>");
                //inXML.Append("<Tier1ID>" + Session["OnflyTopTierID"].ToString() + "</Tier1ID>"); //ZD 100068
                //inXML.Append("<Tier2ID>" + Session["OnflyMiddleTierID"].ToString() + "</Tier2ID>");//ZD 100068
                if (Session["VMRTopTierID"] != null)
                    inXML.Append("<Tier1ID>" + Session["VMRTopTierID"].ToString() + "</Tier1ID>");//ZD 100068
                if (Session["VMRMiddleTierID"] != null)
                    inXML.Append("<Tier2ID>" + Session["VMRMiddleTierID"].ToString() + "</Tier2ID>");//ZD 100068
                inXML.Append("<Floor></Floor>");
                inXML.Append("<RoomNumber></RoomNumber>");
                inXML.Append("<StreetAddress1></StreetAddress1>");
                inXML.Append("<StreetAddress2></StreetAddress2>");
                inXML.Append("<City></City>");
                inXML.Append("<State></State>");
                inXML.Append("<ZipCode></ZipCode>");
                inXML.Append("<Country></Country>");
                inXML.Append("<Handicappedaccess>0</Handicappedaccess>");
                inXML.Append("<isVIP>0</isVIP>");
                inXML.Append("<isTelepresence>0</isTelepresence>");
                inXML.Append("<RoomQueue></RoomQueue>");
                inXML.Append("<MapLink></MapLink>");
                inXML.Append("<ParkingDirections></ParkingDirections>");
                inXML.Append("<AdditionalComments></AdditionalComments>");
                inXML.Append("<TimezoneID>26</TimezoneID>");
                inXML.Append("<Longitude></Longitude>");
                inXML.Append("<Latitude></Latitude>");
                inXML.Append("<Approvers>");
                inXML.Append("  <Approver1ID></Approver1ID>");
                inXML.Append("  <Approver1Name></Approver1Name>");
                inXML.Append("  <Approver2ID></Approver2ID>");
                inXML.Append("  <Approver2Name></Approver2Name>");
                inXML.Append("  <Approver3ID></Approver3ID>");
                inXML.Append("  <Approver3Name></Approver3Name>");
                inXML.Append("</Approvers>");
                //ZD 103263 Start
                if(hdnEPID.Value != "0")
                    inXML.Append("<EndpointID>" + hdnEPID.Value + "</EndpointID>");
                else
                    inXML.Append("<EndpointID></EndpointID>");
                //ZD 103263 end
                inXML.Append("<Custom1></Custom1>");
                inXML.Append("<Custom2></Custom2>");
                inXML.Append("<Custom3></Custom3>");
                inXML.Append("<Custom4></Custom4>");
                inXML.Append("<Custom5></Custom5>");
                inXML.Append("<Custom6></Custom6>");
                inXML.Append("<Custom7></Custom7>");
                inXML.Append("<Custom8></Custom8>");
                inXML.Append("<Custom9></Custom9>");
                inXML.Append("<Custom10></Custom10>");
                inXML.Append("<Projector>0</Projector>");
                inXML.Append("<RoomCategory>" + (int)myVRMNet.NETFunctions.RoomCategory.VMRRoom + "</RoomCategory>"); //FB 2694
                inXML.Append("<Video>2</Video>"); //Media type - video rooms //FB 2488
                inXML.Append("<DynamicRoomLayout>1</DynamicRoomLayout>");
                inXML.Append("<CatererFacility>0</CatererFacility>");
                inXML.Append("<ServiceType>-1</ServiceType>");
                inXML.Append("<DedicatedVideo>0</DedicatedVideo>");
                inXML.Append("<DedicatedCodec>0</DedicatedCodec>");
                inXML.Append("<IsVMR>1</IsVMR>");
                inXML.Append("<iControlRoom>0</iControlRoom>");//ZD 101098
                inXML.Append("<InternalNumber>" + txtInternalnum.Text + "</InternalNumber>");
                inXML.Append("<ExternalNumber>" + txtExternalnum.Text + "</ExternalNumber>");
                inXML.Append("<VMRLink>" + txtVMRLink.Text.Replace("&","&amp;").Trim() + "</VMRLink>");//FB 2727
                //ZD 100753 Starts //ZD 100522 //ZD 101371 - Start
                if (hdnPasschange.Value != "" && hdnPasschange.Value == "true")
                {
                    if (!string.IsNullOrEmpty(txtPassword1.Text))
                    {
                        inXML.Append("<password>" + Password(txtPassword1.Text) + "</password>");
                        inXML.Append("<isPwdChanged>1</isPwdChanged>"); //ZD 100781
                    }
                    else
                    {
                        inXML.Append("<Password></Password>");
                    }
                }
                else
                {
                    inXML.Append("<password>" + Password(Session["VMRPW"].ToString()) + "</password>");
                }
                //ZD 101371 - End
                //ZD 100753 Ends
                inXML.Append("<AVOnsiteSupportEmail></AVOnsiteSupportEmail>");

                //ZD 100522 Start
                inXML.Append("<BridgeId>" + lstMCU.SelectedValue + "</BridgeId>");

                if(chkCreateMCU.Checked)
                    inXML.Append("<IsCreateOnMCU>1</IsCreateOnMCU>");
                else
                    inXML.Append("<IsCreateOnMCU>0</IsCreateOnMCU>");

                if (chkCreateTemplate.Checked)
                    inXML.Append("<IsCreateTemplate>1</IsCreateTemplate>");
                else
                    inXML.Append("<IsCreateTemplate>0</IsCreateTemplate>");

                if (ChkDialoutLoc.Checked)
                    inXML.Append("<IsDialOutLoc>1</IsDialOutLoc>");
                else
                    inXML.Append("<IsDialOutLoc>0</IsDialOutLoc>");
                //ZD 100522 End
                //ZD 103263 Start
                if (chkEnableBJN.Checked)
                    inXML.Append("<EnableBJN>1</EnableBJN>");
                else
                    inXML.Append("<EnableBJN>0</EnableBJN>");
                //ZD 103263 End
                inXML.Append("<Departments>");
                
                foreach (ListItem item in DepartmentList.Items)
                {
                    if (item.Selected)
                        inXML.Append("<Department><ID>" + item.Value + "</ID><Name>" + item.Text + "</Name><SecurityKey></SecurityKey></Department>");
                }

                inXML.Append("</Departments>");

                //FB 2994 start...
                rmImg = "";
                inXML.Append("<RoomImages>"); 
                foreach (DataGridItem dgi in dgItems.Items)
                {
                    if (rmImg == "")
                        rmImg = dgi.Cells[0].Text.Trim();
                    else
                        rmImg += "," + dgi.Cells[0].Text.Trim();

                    inXML.Append("<Image>");
                    inXML.Append("<ActualImage>" + dgi.Cells[1].Text.Trim() + "</ActualImage>");
                    inXML.Append("</Image>");
                }
                inXML.Append("</RoomImages>");
                inXML.Append("<RoomImageName>" + rmImg + "</RoomImageName>");
                //FB 2994 End ...

                inXML.Append("<Images>");
                inXML.Append("  <Map1></Map1>");
                inXML.Append("  <Map1Image></Map1Image>");
                inXML.Append("  <Map2></Map2>");
                inXML.Append("  <Map2Image></Map2Image>");
                inXML.Append("  <Security1></Security1>");
                inXML.Append("  <Security1ImageId></Security1ImageId>");
                inXML.Append("  <Misc1></Misc1>");
                inXML.Append("  <Misc1Image></Misc1Image>");
                inXML.Append("  <Misc2></Misc2>");
                inXML.Append("  <Misc2Image></Misc2Image>");
                inXML.Append("</Images>");

                //ZD 100522 Start
                inXML.Append("<DialOutLocationIds>" + selectedloc.Value + "</DialOutLocationIds>");
                inXML.Append("<VMRConfUID></VMRConfUID>");
                inXML.Append("<PermanentConfUID></PermanentConfUID>");
                inXML.Append("<PermanentconfName></PermanentconfName>");
                inXML.Append("<RoomVMRID>" + txtVMRID.Text + "</RoomVMRID>");
                //ZD 100522 End
                
                inXML.Append("</SetRoomProfile>");
                string outXML = obj.CallMyVRMServer("SetRoomProfile", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                        errLabel.Text = obj.ShowErrorMessage(outXML);

                    errLabel.Visible = true;
                    return;
                }
                loadRooms = new XmlDocument(); //ZD 100522_P1
                loadRooms.LoadXml(outXML);
                ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room/RoomID");
                if (ndeRoomID != null)
                    roomId = ndeRoomID.InnerText;
                //ZD 103263 Start
                ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room/RoomAdminEmail");
                if (ndeRoomID != null)
                    RoomAdminEmail = ndeRoomID.InnerText;
                //ZD 103263 End
				//ZD 103496 Starts
                DataTable dtTable = null;
                XmlNode node = null;
                string disabled = "0";
                node = loadRooms.SelectSingleNode("Rooms/Room/Disabled");
                if (node != null)
                    disabled = node.InnerText.Trim();
				//ZD 103496 End
                //ZD 100522 Start
                string tempOutXML = "";
                ViewState["RoomID"] = roomId;
                errLabel.Text = "";
                if (chkCreateTemplate.Checked)
                {
                    if (!CreateConfTemplate(ref tempOutXML))
                        errLabel.Visible = true;
                }
                if (chkCreateMCU.Checked)
                {
                    if(!CreateConfOnMCU(ref tempOutXML))
                    {
                        errLabel.Text = obj.ShowErrorMessage(tempOutXML);
                        errLabel.Visible = true;
                    }
                }
                if (errLabel.Text != "")
                    return;
                //ZD 100522 End
                /* commented for ZD 104482
				//ZD 103496 Starts
                #region Memcache
                DataTable dtaddTable = null;
                DataRow[] RoomOldRows = null;
                DataSet dsCache = null;
                DataView dv = null;
                DataTable dtNewTable = null;
                bool bRet = false;
                if (memcacheEnabled == 1)
                {
                    using (memClient = new Enyim.Caching.MemcachedClient())
                    {
                        dtNewTable = new DataTable();
                        obj.GetTablefromCache(ref memClient, ref dsCache);

                        if (dsCache.Tables.Count > 0)
                            dtaddTable = dsCache.Tables[0];

                        bRet = obj.GetAllRoomsInfo(roomId, disabled, ref dtNewTable);

                        if (!bRet || dtNewTable.Rows.Count <= 0)
                        {
                            //dtTable = memClient.Get<DataTable>("myVRMRoomsDatatable");
                            obj.GetAllRoomsInfo("", "0", ref dtaddTable, ref dsCache);
                        }
                        RoomOldRows = dtaddTable.Select("Roomid = '" + roomId + "'");

                        if (RoomOldRows != null && RoomOldRows.Count() > 0)
                        {
                            dtaddTable.Rows.Remove(RoomOldRows[0]);
                            dtaddTable.ImportRow(dtNewTable.Rows[0]);
                        }
                        else
                            dtaddTable.ImportRow(dtNewTable.Rows[0]);


                        dv = new DataView(dtaddTable);
                        dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";
                        dtaddTable = dv.ToTable();

                        obj.AddRoomTabletoCache(ref memClient, ref dsCache);
                        //memClient.Store(Enyim.Caching.Memcached.StoreMode.Set, "myVRMRoomsDatatable", dtTable);

                    }
                }

                #endregion
				//ZD 103496 End
                */
                //ZD 103263 Start
                if (chkEnableBJN.Checked)
                {
                    string inXML1 = "<SetEndpoint>";
                    inXML1 += obj.OrgXMLElement();
                    inXML1 += "<EndpointID>" + hdnEPID.Value + "</EndpointID>";
                    inXML1 += "<EndpointName>" + txtRoomName.Text + "</EndpointName>";
                    inXML1 += "<EntityType>U</EntityType>";
                    inXML1 += "<UserID>" + Session["userID"] + "</UserID>";
                    inXML1 += "<IsBJNEpt>1</IsBJNEpt>"; // Diff from user Endpoint
                    inXML1 += "<RoomID>" + ViewState["RoomID"].ToString() + "</RoomID>"; // Diff from user Endpoint
                    inXML1 += "<Profiles>";
                    inXML1 += "<Profile>";
                    inXML1 += "<ProfileID>0</ProfileID>";
                    inXML1 += "<ProfileName>" + txtRoomName.Text + "</ProfileName>";
                    inXML1 += "<Default>0</Default>";
                    inXML1 += "<EncryptionPreferred>0</EncryptionPreferred>";
                    inXML1 += "<AddressType>" + ns_MyVRMNet.vrmAddressType.IP + "</AddressType>";
                    inXML1 += "<Password></Password>";
                    inXML1 += "<Address>199.48.152.152</Address>"; // Statis IP for BJN
                    inXML1 += "<URL></URL>";
                    inXML1 += "<IsOutside>0</IsOutside>";
                    inXML1 += "<ConnectionType>" + ns_MyVRMNet.vrmConnectionTypes.DialOut + "</ConnectionType>";
                    inXML1 += "<VideoEquipment>-1</VideoEquipment>";
                    inXML1 += "<LineRate>384</LineRate>";
                    inXML1 += "<Bridge>" + lstMCU.SelectedValue + "</Bridge>";
                    inXML1 += "<MCUAddress></MCUAddress>";
                    inXML1 += "<ApiPortno>23</ApiPortno>";
                    inXML1 += "<DefaultProtocol>1</DefaultProtocol>";
                    inXML1 += "<MCUAddressType>-1</MCUAddressType>";
                    inXML1 += "<Deleted>0</Deleted>";
                    inXML1 += "<ExchangeID></ExchangeID>";
                    inXML1 += "<TelnetAPI>0</TelnetAPI>";
                    inXML1 += "<SSHSupport>0</SSHSupport>";
                    inXML1 += "<conferenceCode></conferenceCode>";
                    inXML1 += "<leaderPin></leaderPin>";
                    inXML1 += "</Profile>";
                    inXML1 += "</Profiles>";
                    inXML1 += "</SetEndpoint>";

                    outXML = obj.CallMyVRMServer("SetEndpoint", inXML1, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                        return;
                    }
                    else
                    {
						// ZD 103550
                        inXML1 = "<GetRoomDetails><RoomID>" + ViewState["RoomID"].ToString() + "</RoomID>";
                        inXML1 += obj.OrgXMLElement();
                        inXML1 += "<RoomAdminEmail>" + RoomAdminEmail + "</RoomAdminEmail></GetRoomDetails>"; 
                        outXML = obj.CallCOM2("GetRoomBJNDetails", inXML1, HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                            return;
                        }
                    }
                }
                //ZD 103263 End
                Button btnCtrl = (System.Web.UI.WebControls.Button)sender;
                if (btnCtrl.ID.Trim().Equals("btnSubmitAddNew"))
                    Response.Redirect("ManageVirtualMeetingRoom.aspx?m=1");
                else
                    Response.Redirect("manageroom.aspx?hf=&m=1&pub=&d=&comp=&f=&frm=");

            }
            catch (Exception ex)
            {
                log.Trace("SetRoomProfile: "+ ex.StackTrace);
                return;
            }
        }
        #endregion

        #region ResetRoomProfile
        /// <summary>
        /// ResetRoomProfile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResetRoomProfile(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ManageVirtualMeetingRoom.aspx?rID=" + hdnRoomID.Value);
            }
            catch (Exception ex)
            {
                log.Trace("ResetRoomProfile" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        // FB 2994 starts...

        #region UploadRoomImage
        /// <summary>
        /// UploadRoomImage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UploadRoomImage(Object sender, EventArgs e)
        {
            try
            {
                sessionKey = "RoomImg" + Session.SessionID;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData = null;
                String errMsg = "";
                string imageString = "";
                //ZD 100263
                string filextn = "";
                List<string> strExtension = null;
                bool filecontains = false;

                if (roomImageDt == null)
                {
                    roomImageDt = new DataTable();
                    roomImageDt.Columns.Add(new DataColumn("ImageName"));
                    roomImageDt.Columns.Add(new DataColumn("Image"));
                    roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                    roomImageDt.Columns.Add(new DataColumn("ImagePath"));
                }
                FillRoomDataTable();

                if (!roomfileimage.Value.Equals(""))
                {
                    fName = Path.GetFileName(roomfileimage.Value);

                    //ZD 100263 Starts
                    //filextn = fName.Split('.')[fName.Split('.').Length - 1];
                    filextn = Path.GetExtension(fName);
                    strExtension = new List<string> { ".jpg", ".jpeg", ".png", ".bmp", ".gif" };
                    filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                    if (!filecontains)
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End

                    myFile = roomfileimage.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];

                    fileext = Path.GetExtension(roomfileimage.Value);

                    pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/" + fName;

                    if (File.Exists(pathName))
                        errMsg += "Room image already exists.";
                    else
                    {
                        myFile.InputStream.Read(myData, 0, nFileLen);
                        if (nFileLen <= 5000000)
                        {
                            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName, ref myData);

                            imageString = imageUtilObj.ConvertByteArrToBase64(myData);

                            DataRow dr = roomImageDt.NewRow();
                            dr["ImageName"] = fName;
                            dr["Imagetype"] = fileext;
                            dr["Image"] = imageString;
                            dr["ImagePath"] = pathName;

                            roomImageDt.Rows.Add(dr);

                            dgItems.DataSource = roomImageDt;
                            dgItems.DataBind();

                            dgItems.Visible = true;
                        }
                        else
                            errMsg += "Room image is greater than 5MB. File has not been uploaded.";
                    }
                }

                if (errMsg != "")
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region FillRoomDataTable

        private void FillRoomDataTable()
        {
            try
            {
                foreach (DataGridItem dgi in dgItems.Items)
                {
                    DataRow dr = roomImageDt.NewRow();

                    dr["ImageName"] = dgi.Cells[0].Text;
                    dr["Imagetype"] = dgi.Cells[2].Text;
                    dr["Image"] = dgi.Cells[1].Text;
                    dr["ImagePath"] = dgi.Cells[3].Text;

                    roomImageDt.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("FillRoomDatatable" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }

        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Item?") + "')"); //100288
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindRowsDeleteMessage" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region LoadRoomImageGrid - Added for Image Project Edit Mode

        private void LoadRoomImageGrid(XmlNodeList rmImgNodes)
        {
            try
            {
                roomImageDt = new DataTable();
                roomImageDt.Columns.Add(new DataColumn("ImageName"));
                roomImageDt.Columns.Add(new DataColumn("Image"));
                roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                roomImageDt.Columns.Add(new DataColumn("ImagePath"));

                string imgData = "";
                pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/";

                foreach (XmlNode xnode in rmImgNodes)
                {
                    fName = xnode.SelectSingleNode("ImageName").InnerText;
                    fileext = xnode.SelectSingleNode("Imagetype").InnerText;
                    imgData = xnode.SelectSingleNode("Image").InnerText;

                    if (!File.Exists(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName))
                    {
                        File.Delete((Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName));
                    }
                    imgArray = imageUtilObj.ConvertBase64ToByteArray(imgData);
                    WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName, ref imgArray);

                    DataRow dr = roomImageDt.NewRow();

                    dr["ImageName"] = fName;
                    dr["Imagetype"] = fileext;
                    dr["Image"] = imgData;
                    dr["ImagePath"] = pathName + fName;

                    roomImageDt.Rows.Add(dr);
                }
                if (roomImageDt.Rows.Count > 0)
                    dgItems.DataSource = roomImageDt;
                else
                    dgItems.DataSource = null;

                dgItems.DataBind();
                dgItems.Visible = true;

            }
            catch (Exception ex)
            {
                log.Trace("LoadRoomProfile: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region RemoveImage
        protected void RemoveImage(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string imgName = dgItems.Items[e.Item.ItemIndex].Cells[0].Text;

                roomImageDt = new DataTable();
                roomImageDt.Columns.Add(new DataColumn("ImageName"));
                roomImageDt.Columns.Add(new DataColumn("Image"));
                roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                roomImageDt.Columns.Add(new DataColumn("ImagePath"));

                foreach (DataGridItem dgi in dgItems.Items)
                {
                    if (!dgi.ItemIndex.Equals(e.Item.ItemIndex))
                    {
                        DataRow dr = roomImageDt.NewRow();

                        dr["ImageName"] = dgi.Cells[0].Text;
                        dr["Imagetype"] = dgi.Cells[2].Text;
                        dr["Image"] = dgi.Cells[1].Text;
                        dr["ImagePath"] = dgi.Cells[3].Text;

                        roomImageDt.Rows.Add(dr);
                    }
                }

                if (roomImageDt.Rows.Count > 0)
                    dgItems.DataSource = roomImageDt;
                else
                    dgItems.DataSource = null;

                dgItems.DataBind();
                dgItems.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("RemoveImage" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion    

        // FB 2994 ends...
        protected string Password(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace("UserPassword:" + ex.Message);//ZD 100263
            }
            return Encrypted;
        }


        //ZD 101026 Starts
        #region LoadHistory
        /// <summary>
        /// LoadHistory
        /// </summary>
        /// <param name="changedHistoryNodes"></param>
        /// <returns></returns>
        private bool LoadHistory(XmlNodeList changedHistoryNodes)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();

            try
            {
                DataTable dtable = new DataTable();
                if (!dtable.Columns.Contains("ModifiedUserId"))
                    dtable.Columns.Add("ModifiedUserId");
                if (!dtable.Columns.Contains("ModifiedDateTime"))
                    dtable.Columns.Add("ModifiedDateTime");
                if (!dtable.Columns.Contains("ModifiedUserName"))
                    dtable.Columns.Add("ModifiedUserName");
                if (!dtable.Columns.Contains("Description"))
                    dtable.Columns.Add("Description");

                if (changedHistoryNodes != null && changedHistoryNodes.Count > 0)
                {
                    dtable = obj.LoadDataTable(changedHistoryNodes, null);
                    DateTime ModifiedDateTime = DateTime.Now;

                    for (Int32 i = 0; i < dtable.Rows.Count; i++)
                    {
                        DateTime.TryParse(dtable.Rows[i]["ModifiedDateTime"].ToString(), out ModifiedDateTime);
                        dtable.Rows[i]["ModifiedDateTime"] = myVRMNet.NETFunctions.GetFormattedDate(ModifiedDateTime.Date.ToString()) + " " + ModifiedDateTime.ToString(tformat);

                        dtable.Rows[i]["Description"] = obj.GetTranslatedText(dtable.Rows[i]["Description"].ToString());
                    }
                }
                dgChangedHistory.DataSource = dtable;
                dgChangedHistory.DataBind();

            }
            catch (Exception ex)
            {
                log.Trace("LoadHistory" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion
        //ZD 101026 End

        //ZD 100522 Start
        //ZD 102195 Start
        #region CreateConfOnMCU
        /// <summary>
        /// CreateConfOnMCU
        /// </summary>
        /// <param name="outXML"></param>
        /// <returns></returns>
        private bool CreateConfOnMCU(ref string outXML)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();

            StringBuilder inxml = new StringBuilder();

            try
            {
                inxml.Append("<conference>");
                inxml.Append(obj.OrgXMLElement());
                inxml.Append("<requestorID>" + Session["userid"].ToString() + "</requestorID>");
                inxml.Append("<userID>" + Session["userid"].ToString() + "</userID>");
                inxml.Append("<confInfo>");

                if (hdnPermanetConfid.Value != "")
                    inxml.Append("<confID>" + hdnPermanetConfid.Value + "</confID>");
                else
                    inxml.Append("<confID>new</confID>");

                inxml.Append("<editFromWeb>1</editFromWeb>");
                inxml.Append("<isExpressConference>0</isExpressConference>");
                inxml.Append("<language>en</language>");
                inxml.Append("<confName>" + txtRoomName.Text + "</confName>");
                inxml.Append("<confHost>" + Session["userid"].ToString() + "</confHost>");
                inxml.Append("<confOrigin>0</confOrigin>");
                inxml.Append("<timeCheck></timeCheck>");
                inxml.Append("<confPassword>" + txtPassword1.Text + "</confPassword>");
                inxml.Append("<isVMR>0</isVMR>");
                inxml.Append("<EnableStaticID>0</EnableStaticID>");
                inxml.Append("<MeetingId>1</MeetingId>");
                inxml.Append("<StaticID>0</StaticID>");
                inxml.Append("<isVIP>0</isVIP>");
                inxml.Append("<isDedicatedEngineer>0</isDedicatedEngineer>");
                inxml.Append("<isLiveAssitant>0</isLiveAssitant>");
                inxml.Append("<isReminder>0</isReminder>");
                inxml.Append("<CloudConferencing>0</CloudConferencing>");
                inxml.Append("<isPCconference>0</isPCconference>");
                inxml.Append("<pcVendorId>0</pcVendorId>");
                inxml.Append("<StartMode>0</StartMode>");
                inxml.Append("<MeetandGreetBuffer>0</MeetandGreetBuffer>");
                inxml.Append("<Secured>0</Secured>");
                inxml.Append("<EnableNumericID>1</EnableNumericID>");
                inxml.Append("<CTNumericID>" + txtVMRID.Text + "</CTNumericID>");
                inxml.Append("<McuSetupTime>0</McuSetupTime>");
                inxml.Append("<MCUTeardonwnTime>0</MCUTeardonwnTime>");
                inxml.Append("<Permanent>1</Permanent>");
                inxml.Append("<immediate>1</immediate>");
                inxml.Append("<recurring>0</recurring>");
                inxml.Append("<recurringText></recurringText>");
                inxml.Append("<startDate></startDate>");
                inxml.Append("<startHour></startHour>");
                inxml.Append("<startMin></startMin>");
                inxml.Append("<startSet></startSet>");
                inxml.Append("<timeZone>26</timeZone>");
                inxml.Append("<setupDuration>0</setupDuration>");
                inxml.Append("<teardownDuration>0</teardownDuration>");
                inxml.Append("<setupDateTime></setupDateTime>");
                inxml.Append("<teardownDateTime></teardownDateTime>");
                inxml.Append("<createBy>2</createBy>");
                inxml.Append("<durationMin>0</durationMin>");
                inxml.Append("<description></description>");
                inxml.Append("<locationList></locationList>");
                inxml.Append("<ConfGuestRooms></ConfGuestRooms>");
                inxml.Append("<publicConf>0</publicConf>");
                inxml.Append("<dynamicInvite>0</dynamicInvite>");
                inxml.Append("<advAVParam> ");
                inxml.Append("<maxAudioPart></maxAudioPart>");
                inxml.Append("<maxVideoPart></maxVideoPart>");
                inxml.Append("<restrictProtocol>3</restrictProtocol>");
                inxml.Append("<restrictAV>2</restrictAV>");
                inxml.Append("<videoLayout>01</videoLayout>");
                inxml.Append("<FamilyLayout>0</FamilyLayout>");
                inxml.Append("<maxLineRateID>384</maxLineRateID>");
                inxml.Append("<audioCodec>0</audioCodec>");
                inxml.Append("<videoCodec>0</videoCodec>");
                inxml.Append("<dualStream>1</dualStream>");
                inxml.Append("<confOnPort>0</confOnPort>");
                inxml.Append("<encryption>0</encryption>");
                inxml.Append("<lectureMode>0</lectureMode>");
                inxml.Append("<VideoMode>3</VideoMode>");
                inxml.Append("<SingleDialin>0</SingleDialin>");
                inxml.Append("<internalBridge></internalBridge>");
                inxml.Append("<externalBridge></externalBridge>");
                inxml.Append("<FECCMode>1</FECCMode>");
                inxml.Append("<PolycomSendMail>0</PolycomSendMail>");
                inxml.Append("<PolycomTemplate></PolycomTemplate>");
                inxml.Append("</advAVParam>");
                inxml.Append("<ParticipantList></ParticipantList>");
                inxml.Append("<ModifyType>0</ModifyType>");
                inxml.Append("<fileUpload>");
                inxml.Append("<file></file>");
                inxml.Append("<file></file>");
                inxml.Append("<file></file>");
                inxml.Append("</fileUpload>");
                inxml.Append("<ConciergeSupport></ConciergeSupport>");
                inxml.Append("<CustomAttributesList></CustomAttributesList>");
                inxml.Append("<ICALAttachment></ICALAttachment>");
                inxml.Append("<isExchange>0</isExchange>");
                inxml.Append("<overBookConf>0</overBookConf>");
                inxml.Append("<MCUs></MCUs>");
                inxml.Append("<BridgeID>" + lstMCU.SelectedValue + "</BridgeID>");
                inxml.Append("<ConfMessageList></ConfMessageList>");
                inxml.Append("<FromVMRRoom>1</FromVMRRoom>");//ZD 102918
                inxml.Append("</confInfo>");
                inxml.Append("</conference>");

                outXML = obj.CallMyVRMServer("SetConferenceDetails", inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmlout = new XmlDocument();
                    xmlout.LoadXml(outXML);

                    string confID = "";
                    if (xmlout.SelectSingleNode("//conferences/conference/confID") != null)
                        confID = xmlout.SelectSingleNode("//conferences/conference/confID").InnerText;
                    ViewState["ConfID"] = confID;

                    //if (!SetEndpoints(ref outXML))
                    //    return false;
                    //else
                    //{
                    string inXML = "<Conference><confID>" + confID + "</confID><RoomID>" + ViewState["RoomID"].ToString() + "</RoomID></Conference>"; //ZD 100522_S1
                    outXML = obj.CallCOM2("SetConferenceOnMcu", inXML, HttpContext.Current.Application["RTC_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                        return false;
                    //}
                }
                else
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                log.Trace("CreateConfOnMCU" + ex.Message);
                return false;
            }
        }
        #endregion

        #region SetEndpoints
        /// <summary>
        /// SetEndpoints
        /// </summary>
        /// <param name="outXML"></param>
        /// <returns></returns>
        private bool SetEndpoints(ref string outXML)
        {
            StringBuilder inXML = new StringBuilder();
            try
            {

                inXML.Append("<SetAdvancedAVSettings><editFromWeb>1</editFromWeb><language>en</language><isVIP>0</isVIP><isReminder>0</isReminder>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<ConfID>" + ViewState["ConfID"].ToString() + "</ConfID>");
                inXML.Append("<BridgeID>" + lstMCU.SelectedValue + "</BridgeID>");
                inXML.Append("<AVParams><isDedicatedEngineer>0</isDedicatedEngineer><isLiveAssitant>0</isLiveAssitant><SingleDialin>0</SingleDialin>");
                inXML.Append("<CTNumericID>" + txtVMRID.Text + "</CTNumericID><EnableStaticID>0</EnableStaticID>");
                inXML.Append("<StaticID></StaticID></AVParams></SetAdvancedAVSettings>");
                outXML = obj.CallMyVRMServer("SetAdvancedAVSettings", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion
        //ZD 102195 End
        #region CreateConfTemplate
        /// <summary>
        /// CreateConfTemplate
        /// </summary>
        /// <param name="tempOutXML"></param>
        /// <returns></returns>
        private bool CreateConfTemplate(ref string tempOutXML)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<template>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userInfo>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("</userInfo>");
                inXML.Append("<VMRRoomID>" + ViewState["RoomID"].ToString() + "</VMRRoomID>");
                inXML.Append("<templateInfo>");
                if (ViewState["ConfTemplateId"] == null || (ViewState["ConfTemplateId"] != null && (string.IsNullOrWhiteSpace(ViewState["ConfTemplateId"].ToString()) || ViewState["ConfTemplateId"].ToString() == "0")))
                    ViewState["ConfTemplateId"] = "new";
                inXML.Append("<ID>" + ViewState["ConfTemplateId"].ToString() + "</ID>");
                inXML.Append("<name>" + txtRoomName.Text + "</name>");
                inXML.Append("<public>1</public>");
                inXML.Append("<setDefault></setDefault>");
                inXML.Append("<description></description>");
                inXML.Append("</templateInfo>");
                inXML.Append("<confInfo>");
                inXML.Append("<confName>" + txtRoomName.Text + "</confName>");
                inXML.Append("<confPassword>" + txtPassword1.Text + "</confPassword>");
                inXML.Append("<confType>2</confType>");
                inXML.Append("<durationMin>" + DefaultConfDuration + "</durationMin>");
                inXML.Append("<description></description>");
                inXML.Append("<locationList>");
                inXML.Append("<selected>");
                if (!string.IsNullOrWhiteSpace(selectedloc.Value))
                {
                    int tempLocId = 0;
                    for (int k = 0; k < selectedloc.Value.Split(',').Length; k++)
                    {
                        tempLocId = 0;
                        int.TryParse(selectedloc.Value.Split(',')[k], out tempLocId);
                        if (tempLocId > 0)
                            inXML.Append("<level1ID>" + tempLocId + " </level1ID>");
                    }
                }
                inXML.Append("</selected>");
                inXML.Append("</locationList>");
                inXML.Append("<publicConf>0</publicConf>");
                inXML.Append("<dynamicInvite>0</dynamicInvite>");
                inXML.Append("</confInfo>");
                inXML.Append("</template>");
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                tempOutXML = obj.CallMyVRMServer("SetTemplate", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (tempOutXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(tempOutXML);
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("CreateConfTemplate" + ex.Message);
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                return false;
            }
            return true;
        }
        #endregion

        #region Bind to Rooms

        private bool BindRoomToList()
        {
            string[] locsName = null;
            try
            {
                if (locstrname.Value != "")
                {
                    locsName = locstrname.Value.Split('+');
                    RoomList.Items.Clear();
                    foreach (string s in locsName)
                    {
                        if (s != "")
                        {
                            if (s.Split('|').Length > 1)
                                RoomList.Items.Add(new ListItem(s.Split('|')[1], s.Split('|')[0]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region DisplayBridgeDetails
        /// <summary>
        /// DisplayBridgeDetails
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DisplayBridgeDetails(Object sender, EventArgs e)
        {
            string inXML = "", outXML = "";
            int BridgeId = 0, BridgeTypeId = 0;
            XmlNode node = null;
            try
            {
                if (!hdnRoomID.Value.ToLower().Equals("new"))//ZD 101371
                {
                    if (txtPassword1.Text != "" || txtPassword2.Text != "")
                    {
                        txtPassword1.Style.Add("background-image", "");
                        txtPassword2.Style.Add("background-image", "");
                    }
                }
               
                if (lstMCU.SelectedIndex > 0)
                {
                    inXML = "<GetBridgeDetails>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><BridgeId>" + lstMCU.SelectedValue + "</BridgeId></GetBridgeDetails>";
                    outXML = obj.CallMyVRMServer("GetBridgeDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);

                        node = xmldoc.SelectSingleNode("//GetBridgeDetails/BridgeId");
                        if (node != null)
                            int.TryParse(node.InnerText.Trim(), out BridgeId);

                        node = xmldoc.SelectSingleNode("//GetBridgeDetails/BridgeTypeId");
                        if (node != null)
                            int.TryParse(node.InnerText.Trim(), out BridgeTypeId);

                        if (BridgeTypeId == 4 || BridgeTypeId == 5 || BridgeTypeId == 6 || BridgeTypeId == 12 || BridgeTypeId == 15)
                        {
                            tdVMRLink.Visible = false;
                            txtVMRLink.Visible = false;
                        }
                        else
                        {
                            tdVMRLink.Visible = true;
                            txtVMRLink.Visible = true;
                        }
                    }
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                }
                else
                {
                    tdVMRLink.Visible = true;
                    txtVMRLink.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("DisplayBridgeDetails:" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion

        //ZD 100522 End

    }
}
