﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace ns_MyVRM
{
    public partial class en_ParticipantAssignment : System.Web.UI.Page
    {

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region protected Members

        protected DevExpress.Web.ASPxGridView.ASPxGridView PartcipantGrid;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPartyInfo;
        string RoomID = "";

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();

            //obj.AccessandURLConformityCheck("PartcipantAssignment.aspx", Request.Url.AbsoluteUri.ToLower());
            //log = new ns_Logger.Logger();

            if (Request.QueryString["Rmid"] != null)
                RoomID = Request.QueryString["Rmid"].ToString().Trim();

            BindParticpants();
        }

        #region
        public void BindParticpants()
        {
            try
            {
                if (Session["PartyDatatable"] != null)
                {
                    DataTable dt = new DataTable();
                    DataTable FilterList = new DataTable();
                    dt = (DataTable)Session["PartyDatatable"];

                    if (dt.Rows.Count > 0)
                    {
                        DataRow[] RoomLst = dt.Select("RoomID ='" + RoomID + "' OR Assigned = 0");
                        if (RoomLst.Count() > 0)
                        {
                            FilterList = RoomLst.CopyToDataTable();

                            PartcipantGrid.DataSource = FilterList;
                            PartcipantGrid.DataBind();
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                log.Trace(ex.ToString());
            }
        }
        #endregion

        

        
        #region UpdatePartyList
        /// <summary>
        /// UpdatePartyList
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdatePartyList(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();                
                string[] delimiter = { "," };
                string[] partyInfo = null;
                DataRow[] partyID = null;
                DataRow[] partyList = null;
                if (Session["PartyDatatable"] != null)
                {
                    dt = (DataTable)Session["PartyDatatable"];

                    if (dt.Rows.Count > 0)
                    {                        
                        partyInfo = hdnPartyInfo.Value.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                        partyList = dt.Select("RoomID ='" + RoomID + "'");
                        if (partyList.Count() > 0)
                        {
                            for (int i = 0; i < partyList.Count(); i++)
                            {
                                partyList[i]["RoomID"] = "";//Removed RoomID all existing Assigned Rooms
                                partyList[i]["Assigned"] = "0";
                            }
                        }

                        for (int i = 0; i < partyInfo.Length; i++)
                        {
                            partyID = dt.Select("Email ='" + partyInfo[i] + "'");
                            if (partyID.Count() > 0)
                            {
                                partyID[0]["RoomID"] = RoomID;//Assigned RoomID selected party
                                partyID[0]["Assigned"] = "1";
                            }
                            
                        }
                    }
                    Session["PartyDatatable"] = dt;
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "closeMe", "closeMe();", true);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }
        #endregion


        #region ASPxGridView1_DataBound
        protected void ASPxGridView1_DataBound(object sender, EventArgs e)
        {
            try
            {   
                if (!IsPostBack)
                {
                    if (Session["PartyDatatable"] != null)
                    {
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["PartyDatatable"];

                        if (dt.Rows.Count > 0)
                        {
                            DataRow[] partyList = dt.Select("RoomID ='" + RoomID + "'");
                            if (partyList.Count() > 0)
                            {
                                for (int i = 0; i < partyList.Count(); i++)
                                {
                                    //int tmpPartyId = 0;
                                    //Int32.TryParse(partyList[i]["Email"].ToString(), out tmpPartyId);
                                    for (int j = 0; j < this.PartcipantGrid.VisibleRowCount; j++)
                                    {
                                        if (this.PartcipantGrid.GetRowValues(j, "Email") != null)
                                        {

                                            if(this.PartcipantGrid.GetRowValues(j, "Email").ToString() == partyList[i]["Email"].ToString())
                                                this.PartcipantGrid.Selection.SelectRow(j);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Trace(ex.ToString());
            }
        }
        #endregion
    }
}