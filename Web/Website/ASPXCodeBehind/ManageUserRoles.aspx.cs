/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class en_ManageUserRoles : System.Web.UI.Page
{
    #region Protected Members

    protected System.Web.UI.WebControls.Label MsgLbl;
    protected System.Web.UI.WebControls.ListBox UserRole;
    protected System.Web.UI.WebControls.Button ManageuserrolesSubmit;
    protected System.Web.UI.HtmlControls.HtmlInputHidden userroles;
    protected System.Web.UI.HtmlControls.HtmlInputHidden submit;
    protected System.Web.UI.HtmlControls.HtmlTableCell RNtxt;
    protected System.Web.UI.HtmlControls.HtmlInputText RoleName;
    protected System.Web.UI.HtmlControls.HtmlInputButton ManageuserrolesDelete;
    protected System.Web.UI.HtmlControls.HtmlInputButton ManageuserrolesCreate;

    #endregion

    #region Private Members

    private string outXML = "";
    private String inXML = "";
    private XmlDocument xmlDoc = null;
    private myVRMNet.NETFunctions obj = null;
    ns_Logger.Logger log;//ZD 100263
    #endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("ManageUserRoles.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            obj = new myVRMNet.NETFunctions();

            ManageuserrolesSubmit.Attributes.Add("onclick", "javascript:return frmManageuserroles_Validator ()");

            if(!IsPostBack)
                BindDataToRolesListBox();
            //FB 2664
            ManageuserrolesDelete.Disabled = false;
            ManageuserrolesDelete.Attributes.Add("Class", "altMedium0BlueButtonFormat");

        }
        catch (Exception ex)
        {
            MsgLbl.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace("Page_Load" + ex.Message);//ZD 100263
            MsgLbl.Visible = true;
        }
    }

    #endregion

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);

    }
    #endregion

    #region GetData

    private void GetData()
    {
        try 
        {
            inXML = "<login>"+ obj.OrgXMLElement() +
                    "  <userID>" + Session["userID"].ToString() + "</userID>" +
                    "  <foodModuleEnable>" + Session["foodModule"].ToString() + "  </foodModuleEnable>" +
                    "  <roomModuleEnable>" + Session["roomModule"].ToString() + "  </roomModuleEnable>" +
                    "  <hkModuleEnable>" + Session["hkModule"].ToString()+ "  </hkModuleEnable>" +
                    "</login>"; //FB 2027

            outXML = obj.CallMyVRMServer("GetUserRoles", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027

            if (Session["errMsg"] != null) //Login Management
                if (Session["errMsg"].ToString() != "")
                {
                    if (outXML.IndexOf("<error>") > 0)
                        throw new Exception(outXML);
                }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

#endregion

    #region Bind User Roles

    private void BindDataToRolesListBox()
    {
        String displayName = "";
        try 
        {

            GetData();

            if (outXML != "")
            {
                UserRole.Items.Clear();
		        userroles.Value = "";
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(outXML);

                XmlNodeList userroleNodes = xmlDoc.SelectNodes("userRole");

                if (userroleNodes != null)
                {
                    if (userroleNodes.Count > 0)
                    {

                        XmlNodeList roles = userroleNodes[0].SelectNodes("roles");

                        if (roles.Count > 0)
                        {

                            XmlNodeList role = roles[0].SelectNodes("role");


                            foreach (XmlNode node in role)
                            {
                                if (node.SelectSingleNode("createType").InnerText == "1")//FB 1939
                                    displayName = obj.GetTranslatedText(node.SelectSingleNode("name").InnerText) + obj.GetTranslatedText("(System)");
                                else
                                    displayName = node.SelectSingleNode("name").InnerText + obj.GetTranslatedText("(Custom)");
                                /* *** Code added for FB 1425 QA Bug -Start *** */

                                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                                {
                                    if ((displayName.ToString().Contains("Catering")) || (displayName.ToString().Contains("Facility")) || (displayName.ToString().Contains("AV"))) //FB 2570
                                    {
                                        UserRole.Items.Remove(new ListItem(displayName, node.SelectSingleNode("ID").InnerText));

                                    }
                                    else
                                    {
                                        if (displayName.ToString().Contains("Conferencing"))
                                        {
                                            node.SelectSingleNode("name").InnerText = displayName.ToString().Replace("Conferencing", "Hearing");
                                            displayName = node.SelectSingleNode("name").InnerText;
                                        }

                                        UserRole.Items.Add(new ListItem(displayName, node.SelectSingleNode("ID").InnerText));
                                        displayName = "";
                                        userroles.Value += node.SelectSingleNode("ID").InnerText + "@@" + node.SelectSingleNode("name").InnerText + "@@" + node.SelectSingleNode("menuMask").InnerText + "@@" + node.SelectSingleNode("active").InnerText + "@@" + node.SelectSingleNode("locked").InnerText + "@@" + node.SelectSingleNode("createType").InnerText + "##"; // FB 1968
                                    }

                                }
                                else
                                {
                                    /* *** Code added for FB 1425 QA Bug -End *** */
                                    UserRole.Items.Add(new ListItem(displayName, node.SelectSingleNode("ID").InnerText));
                                    displayName = "";
                                    userroles.Value += node.SelectSingleNode("ID").InnerText + "@@" + obj.GetTranslatedText(node.SelectSingleNode("name").InnerText) + "@@" + node.SelectSingleNode("menuMask").InnerText + "@@" + node.SelectSingleNode("active").InnerText + "@@" + node.SelectSingleNode("locked").InnerText + "@@" + node.SelectSingleNode("createType").InnerText + "##"; // FB 1968 ZD 100288
                                }//Code added for FB 1425 QA Bug


                            }

                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Submit Click

    protected void SubmitClick(object sender, EventArgs e)
    {
        string[] arr = null;
        string[] innarr = null;
        int others = 0; //ZD 101388
        string custAdminRights = "";
        try
        {
            if (userroles.Value != "")
            {
                arr = userroles.Value.Split('#');

                if (arr.Length > 0)
                {
                    inXML = "";

                    inXML = "<userRole><roles>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes

                    foreach (string s in arr)
                    {
                        if (s != "")
                        {
                            inXML += "<role>";
                            innarr = s.Split('@');
							//ZD 101388 Starts
                            if (innarr.Length > 0)
                            {
                                custAdminRights = "0"; // ZD 101233
                                int.TryParse(innarr[4].Split('-')[2].Split('*')[1], out others);
                                if (Convert.ToBoolean(others & 1))
                                    custAdminRights = "2";
                                inXML += "<ID>" + innarr[0] + "</ID><name>" + innarr[2] + "</name><menuMask>" + innarr[4] + "</menuMask><custAdminRights>" + custAdminRights + "</custAdminRights></role>";
                            }
							//ZD 101388 End
                            
                        }
                    }

                    inXML += "</roles></userRole>";

                    arr = null;
                    innarr = null;
                    
                    //outXML = obj.CallCOM("SetUserRoles", inXML, Application["COM_ConfigPath"].ToString());
                    outXML = obj.CallMyVRMServer("SetUserRoles", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                    if (Session["errMsg"] != null) //Login Management
                        if (Session["errMsg"].ToString() != "")
                        {
                            if (outXML.IndexOf("<error>") > 0)
                                throw new Exception(outXML);
                        }

                    MsgLbl.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    userroles.Value = "";
                    RoleName.Value = "";
                    BindDataToRolesListBox();

                }
            }

        }
        
        catch (Exception ex)
        {
            MsgLbl.Text = obj.ShowSystemMessage();//ZD 100263
            MsgLbl.Visible = true;
        }

    }
    #endregion
}
