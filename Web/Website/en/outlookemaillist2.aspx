﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_outlookemaillist2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html> 
<head>
  <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
   <script type="text/javascript"> // FB 2815
       var path = '<%=Session["OrgCSSPath"]%>';
       path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
       document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>
  <script type="text/javascript" src="script/errorList.js"></script>

  <script language="javascript">
  <!--
	
	function errorHandler( e, f, l ){
		alert("An error has ocurred in the JavaScript on this page.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
		return true;
	}
	
  //-->
  </script>

</head>


<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">


<!--------------------------------- CONTENT START HERE --------------->


<link rel="stylesheet" type="text/css" href="css/fixedHeader.css" />
<script language="javascript1.1" src="extract.js"></script>
<script language="JavaScript" src="sorttable.js"></script> 

<script type="text/javascript" language="JavaScript">
<!--  

var isCreate, isFu, isRm, isFuCreate, isRmCreate;
if (parent.opener.document.frmSettings2) {
	isCreate = ( (parent.opener.document.frmSettings2.ConfID.value == "new") ? true : false ) ;
	isFu = ( (parent.opener.document.frmSettings2.CreateBy.value == "1") ? true : false ) ;
	isRm = ( ((parent.opener.document.frmSettings2.CreateBy.value == "2") || (parent.opener.document.frmSettings2.CreateBy.value == "7")) ? true : false ) ;

	isFuCreate = (isFu && isCreate)
	isRmCreate = (isRm && isCreate)
}

// all settings2 pages
function partyChg (pinfo)
{
//alert("pinfo=" + pinfo)
	pary = pinfo.split(",");
	pid = pary[0]; pfn = pary[1]; pln = pary[2]; pemail = pary[3]; ischk = pary[4]; 
	pno = 2 + parseInt(pary[5], 10) * ((isFuCreate || isRm) ? 6 : 7);
		
	partysinfo = parent.opener.document.frmSettings2.PartysInfo.value;
	if (document.getElementById("0" + pemail).checked) {
	
		mp = pid + "," + pfn + "," + pln + "," + pemail;
			
//		mp += (document.frmOutlookemaillist2.elements[pno+i].checked) ? ",1" : ",0";
//		mp += (document.frmOutlookemaillist2.elements[pno+i].checked) ? ",1" : ",0";
		mp += ( isFuCreate ? (document.frmOutlookemaillist2.elements[pno+1].checked ? ",1" : ",0") : (isRm ? ",0" : (document.frmOutlookemaillist2.elements[pno+1].checked ? ",1" : ",0") ) );
		mp += ( isFuCreate ? ",0" : (isRm ? (document.frmOutlookemaillist2.elements[pno+1].checked ? ",1" : ",0") : (document.frmOutlookemaillist2.elements[pno+2].checked ? ",1" : ",0") ) );

		frm = ((isFuCreate || isRm) ? 2 : 3);
		for (var i=frm; i<(frm + 4); i++) {
			mp += (document.frmOutlookemaillist2.elements[pno+i].checked) ? ",1" : ",0";
		}
			
		mp += ",,,-7,,,;";
		
	
		if (partysinfo.indexOf("," +pemail + ",") == -1) {
			partysinfo += mp;			
		} else {
			lp = partysinfo.lastIndexOf(";",partysinfo.indexOf("," + pemail + ",")) + 1;
			rp = partysinfo.indexOf(";",partysinfo.indexOf("," + pemail + ",")) + 1;

			newpartysinfo = partysinfo.substring(0, (lp=="-1") ? 0 : lp);
			newpartysinfo += mp;
			newpartysinfo += partysinfo.substring(rp, partysinfo.length);
			partysinfo = newpartysinfo;
		}
	} else {
		if (ischk=="1") {
			if (partysinfo.indexOf("," + pemail + ",") != -1) {
				lp = partysinfo.lastIndexOf(";",partysinfo.indexOf("," + pemail + ",")) + 1;
				rp = partysinfo.indexOf(";",partysinfo.indexOf("," + pemail + ",")) + 1;
				newpartysinfo = partysinfo.substring(0, (lp=="-1") ? 0 : lp);
				newpartysinfo += partysinfo.substring(rp, partysinfo.length);
				partysinfo = newpartysinfo;
			}
		}
	}
	

	parent.opener.document.frmSettings2.PartysInfo.value = partysinfo;
	parent.opener.ifrmPartylist.location.reload(); 

}

function partyChgNET (pinfo)
{
	pary = pinfo.split(",");
	pid = pary[0]; pfn = pary[1]; pln = pary[2]; pemail = pary[3]; ischk = pary[4]; 
	pno = 2 + parseInt(pary[5], 10) * ((isFuCreate || isRm) ? 6 : 7);
		
    //Code Modified For Outlook LookUP- On Selection of checkbox to display value in calling page grid - FB 412 - Start 
	//partysinfo = parent.opener.document.getElementById("Wizard1_txtPartysInfo").value;
	if( parent.opener.document.getElementById("txtPartysInfo") != null)
	    partysinfo = parent.opener.document.getElementById("txtPartysInfo").value;
	else
	    partysinfo = parent.opener.document.frmSettings2.PartysInfo.value;
    //Code Modified For Outlook LookUP- On Selection of checkbox to display value in calling page grid - FB 412 - End 
	    
	    
	if (document.getElementById("0" + pemail).checked) {
		//mp = pid + "," + pfn + "," + pln + "," + pemail + ",1,0,0,1,1,0,,,-7,,,;";
		 /* Code Modified For FB 1456 - Start */
	    if(isRm)
	    {
		    mp = pid + "," + pfn + "," + pln + "," + pemail + ",1,1,0,1,1,0,,,-7,,,;";
		}
		else
		{
		    mp = pid + "," + pfn + "," + pln + "," + pemail + ",1,0,0,1,1,0,,,-7,,,;";	
		}	    
	    /* Code Modified For FB 1456 - End */
		
		if (partysinfo.indexOf("," +pemail + ",") == -1) {
			partysinfo += mp;			
		} else {
			lp = partysinfo.lastIndexOf(";",partysinfo.indexOf("," + pemail + ",")) + 1;
			rp = partysinfo.indexOf(";",partysinfo.indexOf("," + pemail + ",")) + 1;

			newpartysinfo = partysinfo.substring(0, (lp=="-1") ? 0 : lp);
			newpartysinfo += mp;
			newpartysinfo += partysinfo.substring(rp, partysinfo.length);
			partysinfo = newpartysinfo;
		}
	} else {
		if (ischk=="1") {
			if (partysinfo.indexOf("," + pemail + ",") != -1) {
				lp = partysinfo.lastIndexOf(";",partysinfo.indexOf("," + pemail + ",")) + 1;
				rp = partysinfo.indexOf(";",partysinfo.indexOf("," + pemail + ",")) + 1;
				newpartysinfo = partysinfo.substring(0, (lp=="-1") ? 0 : lp);
				newpartysinfo += partysinfo.substring(rp, partysinfo.length);
				partysinfo = newpartysinfo;
			}
		}
	}
	
    //Code Modified For Outlook LookUP- On Selection of checkbox to display value in calling page grid - FB 412 - Start 	
	//parent.opener.document.getElementById("Wizard1_txtPartysInfo").value = partysinfo;
	if( parent.opener.document.getElementById("txtPartysInfo") != null)
	    parent.opener.document.getElementById("txtPartysInfo").value = partysinfo;	
	else
	    parent.opener.document.frmSettings2.PartysInfo.value = partysinfo;
    //Code Modified For Outlook LookUP- On Selection of checkbox to display value in calling page grid - FB 412 - End 
	    
	    
	parent.opener.ifrmPartylist.location.reload(); 

}
// managegoup2
function memberChg (pinfo, pemail)
{
//alert("pinfo=" + pinfo + "***pemail=" + pemail)
//alert("in");
	if (!parent.opener.document.frmManagegroup2) {
		alert(EN_106);
		
		ary = (document.frmOutlookemaillist2.ptyinfos.value).split(";");
		start_no = 2;
		if (queryField("srch") == "y")
			start_no += 3; 
		start_no -= 1;
		
		found = false;
		for (k = 0; k < ary.length-1; k++) {
			start_no += 1;
			if ( document.frmOutlookemaillist2.elements[start_no].name == ("0" + pemail) ) {
				found = true;
				break;
			}
		}
		if (found) {
			cb1 = document.frmOutlookemaillist2.elements[start_no];
			if (cb1.checked)
				cb1.checked = false;
		}

		return false;
	}
	
	gu = parent.opener.document.frmManagegroup2.PartysInfo.value
	
	if (document.getElementById("0" + pemail).checked) {
		if ( gu.indexOf("," + pemail + ",") == -1) {
			parent.opener.document.frmManagegroup2.PartysInfo.value += pinfo;
		}
	} else {
		if ( gu.indexOf("," + pemail + ",") != -1) {
			lp = gu.lastIndexOf( ";", gu.indexOf("," + pemail + ",") ) + 1;
			rp = gu.indexOf( ";", gu.indexOf("," + pemail + ",") ) + 1;
			newgu = gu.substring(0, (lp=="-1") ? 0 : lp);
			newgu += gu.substring(rp, gu.length);
			parent.opener.document.frmManagegroup2.PartysInfo.value = newgu;
		}
	}

	parent.opener.ifrmMemberlist.location.reload(); 
}


// replaceuser
function userChg (pid, pfname, plname, pemail)
{
//alert("pid=" + pid + "***pfname=" + pfname + "***plname=" + plname + "***pemail=" + pemail)
	parent.opener.document.frmReplaceuser.NewUserID.value = pid;
	parent.opener.document.frmReplaceuser.NewUserFirstName.value = pfname;
	parent.opener.document.frmReplaceuser.NewUserLastName.value = plname;
	parent.opener.document.frmReplaceuser.NewUserEmail.value = pemail;
}


function sortlist(id)
{
	SortRows(party_email, id);
}


function Reset ()
{
	if (queryField("frm") == "users") {
		self.window.history.back ();
		
		parent.opener.document.frmReplaceuser.NewUserID.value = "";
		parent.opener.document.frmReplaceuser.NewUserFirstName.value = "";
		parent.opener.document.frmReplaceuser.NewUserLastName.value = "";
		parent.opener.document.frmReplaceuser.NewUserEmail.value = "";
		
		return true;
	}


	ary = (document.frmOutlookemaillist2.ptyinfos.value).split(";");

	switch (queryField("frm")) {
		case "party2":
			unit = ( (isFuCreate || isRm) ? 6 : 7);		
			break;
		case "users":
			break;
		default:
			unit = 1;			
			break;
	}
					
	for (i = 0; i < ary.length-1; i++) {
		ary[i] = ary[i].split(",");
		
		start_no = 2;
		start_no -= unit;
	
		found = false;
		for (k = 0; k < ary.length-1; k++) {
			start_no += unit;
			if ( document.frmOutlookemaillist2.elements[start_no].name == ("0" + ary[i][3]) ) {
				found = true;
				break;
			}
		}
		
			
		if (found) {
			cb1 = document.frmOutlookemaillist2.elements[start_no];
			if (cb1.checked)	{
				cb1.checked = false;
				switch (queryField("frm")) {
					case "party2":
						pi = "";
						for (j = 0; j < ary[i].length; j++) {
							pi += ary[i][j] + ","
						}
						pi = pi.substr(0, pi.length-1)
						partyChg (pi);
						break;
					case "users":
						break;
					default:
						pi = "";
						for (j = 0; j < ary[i].length; j++) {
							pi += ary[i][j] + ";;"
						}
						pi = pi.substr(0, pi.length-2) + "||"
						memberChg (pi, ary[i][3]);
						break;
				}
			}
		}
		
	}
	
	
	if (queryField("frm") == "party2") {
		start_no = 2;
		start_no -= ( (isFuCreate || isRm) ? 6 : 7);
	
		for (i = 0; i < ary.length-1; i++) {
			start_no += ( (isFuCreate || isRm) ? 6 : 7);

			document.frmOutlookemaillist2.elements[(start_no+1)].checked = true;
			document.frmOutlookemaillist2.elements[(isFuCreate || isRm) ? (start_no+3) : (start_no+4)].checked = false;
			document.frmOutlookemaillist2.elements[(isFuCreate || isRm) ? (start_no+5) : (start_no+6)].checked = true;

		}
	}
}
//-->
</script>
 <%
     if (Request.QueryString["frm"] == "party2")
     {
%>
	<form name="frmOutlookemaillist2"  method="POST" action="">
<%
     }
     else
     {
%>
	<form name="frmOutlookemaillist2"  method="POST" action="dispatcher/userdispatcher.asp">
<%
     }
%>
    <input type="hidden" name="outlookEmails" value="" />    
	<input type="hidden" name="emailListPage" value="<% =Convert.ToString( Convert.ToInt32(Request.QueryString["page"])+1 ) %>" />
    <input type="hidden" name="xmljs" id="xmljs" runat="server" />
 <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>


<script language="JavaScript">
<!-- 

	if (parent.document.frmOutlookemaillist2main) {
	    xmlstr = document.frmOutlookemaillist2.xmljs.value;   
	} else {
		xmlstr = ""
		setTimeout('window.location.reload();',500);
	}

	if (xmlstr == "") {
		msg = "<center><table width=90%>"
		msg += "<tr height=2><td height=10></td></tr>"
		msg += "<tr height=2><td align=center><font color=#FF66FF><b>No users were found.</b></font></td></tr>"
		msg += "<tr  height=2><td height=5></td></tr>"
		msg += "<tr  height=2><td><font size=2><b>Possible reasons for no users being listed in the Lookup:</b></font></td></tr>"
		msg += "<tr  height=2><td><font color=#0000FF>1. Your web browser settings are incorrect.  Please edit the settings by following the solution below or contact myVRM Administrator for more details.</font></td></tr>"
		msg += "<tr  height=2><td height=2></td></tr>"
		msg += "<tr  height=2><td><font color=#0000FF>2. You do not have any contacts in your MS Outlook Address Book (Win2000) or Contacts Folder (WinXP).</font></td></tr>"
		msg += "<tr  height=2><td height=10></td></tr>"
		msg += "<tr  height=2><td><font size=2><b>Solution</b></font></td></tr>"
		msg += "<tr  height=2><td><font color=#0000FF>1. This website must be made a trusted site in your Internet Explorer browser. This is a prerequisite for myVRM to interact with Microsoft Outlook. Use the following steps to select this website as a trusted site: "
        msg += "In Internet Explorer, go to ‘Tools?menu. "
		msg += "Select 'Internet Options...'. "
		msg += "Click 'Security' tab. "
		msg += "Select 'Trusted Sites' icon. "
		msg += "Click 'Sites..?button. "
		msg += "Now, add <u>" + (window.location.href).substr(0, (window.location.href).indexOf("/en/")) + "</u> into the trusted web sites. Do not select 'Require server verification (https:) for all sites in this zone'.</font></td></tr>"
		msg += "<tr  height=2><td height=2></td></tr>"
		msg += "<tr  height=2><td><font color=#0000FF>2. Please make sure Microsoft Outlook software is correctly installed and opened. Click OK to continue.  If  prompted to select ActiveX interaction, click 'Yes'. If nothing happens after clicking 'Yes', please check Internet Explorer settings or contact your myVRM Administrator.</font></td></tr>"
		msg += "</table></center>"
		
		document.write (msg);
	}
	
	var party_email = new SortTable("party_email");
	var pinfos = "";	
	switch (queryField("frm")) {
		case "party2":
		case "party2NET":
			if (isFuCreate || isRm) 
				wid = 22;
			else
				wid = 18;		
			party_email.AddColumn("Name","width=" + wid + "%","center","email");
			party_email.AddColumn("Email","width=" + (wid+6) + "%","center","email"); // style='font-size:-1; text-overflow : ellipsis; overflow : visible'"
			party_email.AddColumn("Selected","width=12%","center","form");			
			//Commented by Mary on 3rdApril 2009 start	
			//if (isFuCreate || isRm) {
				//party_email.AddColumn("Invite","width=11%","center","form");
			//} else {
				//party_email.AddColumn("Invited","width=11%","center","form");
				//party_email.AddColumn("Invitee","width=10%","center","form");
			//}
			//party_email.AddColumn("CC","width=4%","center","form");
			//party_email.AddColumn("Notify","width=8%","center","form");
			//party_email.AddColumn("Audio","width=8%","center","form");
			//party_email.AddColumn("Video","width=10%","center","form");
			//Commented by Mary on 3rdApril 2009 End
			break;
		default:		
			party_email.AddColumn("Name","width='35%'","center","email");
			party_email.AddColumn("Email","width='58%'","center","email");
			party_email.AddColumn("Selected","width='7%'","center","form");
			break;
	}
	usersary = xmlstr.split(";;")	
	j = -1;
	for (i = 0; i < usersary.length-1; i++) {
		usersary[i] = usersary[i].split("%%")

		comp = "";
		switch ( queryField("st") ) {
			case "0":
				comp = ( (usersary[i][1]).substr(0,1) ).toUpperCase()
				break;
			case "1":
				comp = ( (usersary[i][0]).substr(0,1) ).toUpperCase()
				break;
			case "2":
				comp = ( (usersary[i][2]).substr(0,1) ).toUpperCase()
				break;
		}

		if ( (queryField("sn") == "0") || (queryField("sn") == comp) ) {
			j++;

			switch (queryField("frm")) {
				case "party2":
				//Commented on 3rd Apr09 start
					//if (usersary[i][0].length > 20)
						//usersary[i][0] = usersary[i][0].substr(0,20) + "..."
					//if (usersary[i][1].length > 20)
						//usersary[i][1] = usersary[i][1].substr(0,20) + "..."
					//if (usersary[i][2].length > 20)
						//usersary[i][2] = usersary[i][2].substr(0,20) + "..."
				//Commented on 3rd Apr09 End
                    pinfostr = "new," + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + ",1," + j;
				
					selectstr = "<input type='checkbox' name='0" + usersary[i][2] + "' value='1' " + 
								"onclick='JavaScript: partyChg(\"" + pinfostr + "\");'>";

					tmpstr1 = "onclick='JavaScript:partyChg(\"new," + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + ",0," + j + "\");'";
					tmpstr2 = ""
					tmpstr = " " + tmpstr1 + " " + tmpstr2 + ">"

					invitedstr = "<input type='radio' name='1" + usersary[i][2] + "' value='1' checked" + tmpstr
					inviteestr = "<input type='radio' name='1" + usersary[i][2] + "' value='2'" + tmpstr
					
					ccstr = "<input type='radio' name='1" + usersary[i][2] + "' value='0'" + tmpstr
					notifystr = "<input type='checkbox' name='2" + usersary[i][2] + "' value='1' checked" + tmpstr
					audiostr = "<input type='radio' name='3" + usersary[i][2] + "' value='0'" + tmpstr
					videostr = "<input type='radio' name='3" + usersary[i][2] + "' value='1' checked" + tmpstr
					break;
				case "party2NET":
				//Commented on 3rd Apr09 start
					//if (usersary[i][0].length > 20)
						//usersary[i][0] = usersary[i][0].substr(0,20) + "..."
					//if (usersary[i][1].length > 20)
						//usersary[i][1] = usersary[i][1].substr(0,20) + "..."
					//if (usersary[i][2].length > 20)
						//usersary[i][2] = usersary[i][2].substr(0,20) + "..."
				//Commented on 3rd Apr09 End

					pinfostr = "new," + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2].toLowerCase() + ",1," + j;
				
					selectstr = "<input type='checkbox' name='0" + usersary[i][2] + "' value='1' " + 
								"onclick='JavaScript: partyChgNET(\"" + pinfostr + "\");'>";

					tmpstr1 = "onclick='JavaScript:partyChgNET(\"new," + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2].toLowerCase() + ",0," + j + "\");'";
					tmpstr2 = ""
					tmpstr = " " + tmpstr1 + " " + tmpstr2 + ">"

					invitedstr = "<input type='hidden' name='1" + usersary[i][2] + "' value='1' checked" + tmpstr
					inviteestr = "<input type='hidden' name='1" + usersary[i][2] + "' value='2'" + tmpstr
					
					ccstr = "<input type='hidden' name='1" + usersary[i][2] + "' value='0'" + tmpstr
					notifystr = "<input type='hidden' name='2" + usersary[i][2] + "' value='1' checked" + tmpstr
					audiostr = "<input type='hidden' name='3" + usersary[i][2] + "' value='0'" + tmpstr
					videostr = "<input type='hidden' name='3" + usersary[i][2] + "' value='1' checked" + tmpstr
					break;
				case "users":	// rplc us
					pinfostr = "new," + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2]

					selectstr = "<input type='radio' name='newuser' value='1' " + 
						"onclick='JavaScript: userChg(\"new\", \"" + usersary[i][0] + "\", \"" + usersary[i][1] + "\", \"" + usersary[i][2] + "\");'>"
					break;
				default:		// gp
					pinfostr = "new," + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2]

					selectstr = "<input type='checkbox' name='0" + usersary[i][2] + "' value='1' " + 
								"onclick='JavaScript: memberChg(\"new," + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + ",-7;\", \"" + usersary[i][2] + "\");'>"
					break;
			}
							
			namestr = usersary[i][0] + " " + usersary[i][1]		
			emailstr = usersary[i][2]

			switch (queryField("frm")) {
				case "party2":
				case "party2NET":
					// selectstr,invitedstr,inviteestr,ccstr,notifystr,audiostr,videoeestr,namestr,emailstr
					//Changed by Mary on 3rdApril 2009 start
					//if (isFuCreate || isRm)
						//party_email.AddLine(namestr, emailstr, selectstr, invitedstr, ccstr, notifystr, audiostr, videostr);
					//else
						//party_email.AddLine(namestr, emailstr, selectstr, invitedstr, inviteestr, ccstr, notifystr, audiostr, videostr);
						party_email.AddLine(namestr, emailstr, selectstr);
				   //Changed by Mary on 3rdApril 2009 End		
					break;
				case "users":
					party_email.AddLine(namestr, emailstr, selectstr);
					break;
				default:
					party_email.AddLine(namestr, emailstr, selectstr);
					break;
			}

			pinfos += pinfostr + ";"
		}
	}

//-->
</script>
<div id="container">
    <table>
	    <script type="text/javascript">
		document.write(parent.document.frmOutlookemaillist2main.mt.value);
//Window Dressing
	    if (party_email.Lines.length == 0) {
		    document.write ("<tr height=4><td colspan='10' class='tableBody'><font class='lblError'>No users.</font></td></tr>")
	    }
	    else
		    party_email.WriteRows()
	    </script>
    </table>
</div>
  </center>

  <input type="hidden" name="ptyinfos" value="" />
</form>
<script type="text/javascript" language="JavaScript">
<!--  

	document.frmOutlookemaillist2.ptyinfos.value = pinfos;
	
//-->
</script>
	 

	 
</body>

</html>

