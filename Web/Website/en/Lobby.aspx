﻿<!--ZD 100147 Start-->
<!--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 ZD 100866 End-->
<%@ Page Language="C#" AutoEventWireup="true" Inherits="MyVRMNet.Lobby" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--FB 2779--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Lobby</title>
    <%--<script type="text/javascript" src="script/wincheck.js"></script>--%> <%--FB 3055-URL--%><%--ZD 100200--%>
    <script type="text/javascript">            // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
    	path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    	document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>

    
<style type="text/css">
img
{
	border:none;
}



.big
{
		position:relative;
		/*background-color:lightyellow;*/
		cursor: default;
		z-index:10;
		width:80px;
	    height:125px;

}

.small
{
		position:relative;
		/*background-color:Green;*/
		cursor: default;
		z-index:10;
	    width:100%;
	    height:100%;

}

.big span
{
	color:#555555; /* FB 2785 */
	font-weight:bold;
	font-size:12px;
	cursor:default;
	font-family:Arial;
	cursor:pointer;
}

.small span
{
	color:Gray;
	font-weight:normal;
	font-size:10px;
	cursor:default;
	font-family:Arial;
}

.big a img
{
	border:none;
	width:40px; /*50*/
	height:40px; /*50*/
	filter:alpha(opacity=100);
	opacity:1;
}

.big a img:hover
{
	border:none;
	width:50px;
	height:50px;
	filter:alpha(opacity=100);
	opacity:1;

}
/*style="opacity:0.4;filter:alpha(opacity=50)"

onmouseover="this.style.opacity=1;this.filters.alpha.opacity=100"

onmouseout="this.style.opacity=0.4;this.filters.alpha.opacity=50"*/
.small a img
{
	border:none;
	width:40px;
	height:40px;
	filter:alpha(opacity=50);
    /*-moz-opacity:0.5;*/
    opacity:0.5;
	
}

/*
.small img:hover
{
	border:none;
	width:40px;
	height:40px;
	filter:alpha(opacity=100);
    -moz-opacity:1;
    opacity:1;
}
*/

.tdx
{
	/*border:solid 1px lightgray;*/
	width:11%;
	vertical-align:middle;
	
}




</style>

<script type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //ZD 100429
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    
function noError(){return true;}
window.onerror = noError;
var actualText = null; // FB 2050

function fnZoomOut(z)
{
    var zz = document.getElementById(z);
    if(zz.childNodes[0].childNodes.length > 0)
    {
    zz.childNodes[0].childNodes[0].style.width='50px';
    zz.childNodes[0].childNodes[0].style.height='50px';
    actualText = zz.childNodes[0].childNodes[2].innerHTML; // FB 2050
    
    zz.childNodes[0].childNodes[2].appendChild(document.createElement("br")); // FB 2050
    zz.childNodes[0].childNodes[2].innerHTML = actualText; // FB 2050

    }
}

function fnZoomIn(z)
{
var zz = document.getElementById(z);
    if(zz.childNodes[0].childNodes.length > 0)
    {
    zz.childNodes[0].childNodes[0].style.width='40px';
    zz.childNodes[0].childNodes[0].style.height='40px';
    zz.childNodes[0].childNodes[2].innerHTML = actualText; // FB 2050
    }
}

    
</script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    <%-- ZD 100429 --%>
    <table width="100%">
    <tr>
        <td align="center">
            <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                <img border='0' src='image/wait1.gif' alt='Loading..' />
            </div><%--ZD 100678--%>
       </td>
    </tr></table>
    <table id="main" cellpadding="1" cellspacing="1" width="100%" border="0">
        
        <tr>
            <td align="center" width="11%" id="tdCalender" runat="server" class="tdx" ><div id="pane11" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);" >
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="gd" runat="server" class="tdx" ><div id="pane12" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdNewConf" runat="server" class="tdx" ><div id="pane13" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdMypreferences" runat="server" class="tdx" ><div id="pane14" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdReports" runat="server" class="tdx" ><div id="pane15" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
        </tr>
        <tr>
             <td align="center" width="11%" id="td1" runat="server" class="tdx" ><div id="pane16" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" class="tdx" ><div id="pane17" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
            <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" class="tdx" ><div id="pane18" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
            <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdOrgoptions" runat="server" class="tdx" ><div id="pane19" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdUserEmailQueue" runat="server" class="tdx" ><div id="pane20" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
        </tr>
        <tr>
            <td align="center" width="11%" id="tdOrgSettings" runat="server" class="tdx" ><div id="pane21" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdSysDiagnostic" runat="server" class="tdx" ><div id="pane22" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdSiteSettings" runat="server" class="tdx" ><div id="pane23" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdOrgUI" runat="server" class="tdx" ><div id="pane24" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdSysAudit" runat="server" class="tdx" ><div id="pane25" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
        </tr>
        <tr>
            <td align="center" width="11%" class="tdx" ><div id="pane26" class="big"  onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
            <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdRooms" runat="server" class="tdx" ><div id="pane27" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdGroups" runat="server" class="tdx" ><div id="pane28" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdTemp" runat="server" class="tdx" ><div id="pane29" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" class="tdx" ><div id="pane30" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
            <br /><br /><br /><br /><br /></div>
            </td>
        </tr>
        <tr>
            <td align="center" width="11%" id="tdInv" runat="server" class="tdx" ><div id="pane31" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdHsKep" runat="server" class="tdx" ><div id="pane32" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdCtr" runat="server" class="tdx" ><div id="pane33" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" class="tdx" ><div id="pane34" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
            <br /><br /><br /><br /><br /></div>
            </td>
            <td align="center" width="11%" id="tdtiers" runat="server" class="tdx" ><div id="pane35" class="big" onmouseover="fnZoomOut(this.id);" onmouseout="fnZoomIn(this.id);">
                <br /><br /><br /><br /><br /></div>
            </td>
        </tr>




        </table>
        </div>
        
        
    
    </form>
    </body>
</html>
<script language="javascript" type="text/javascript">
function openFeedback()
	{
		window.open ('feedback.aspx?wintype=pop', 'Feedback', 'width=450,height=410,resizable=no,scrollbars=no,status=no,left=' + (screen.availWidth-478) + ',top=198'); //Login Management
	}
	
// FB 2050 Start

function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

//window.onload = refreshStyle; // Commented for Refresh Issue

//FB 2050 End

</script>
