<!--ZD 100147 Start-->
<!--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 ZD 100886 End-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%--FB 1861--%>
<!--  #INCLUDE FILE="../inc/Holiday.aspx"  -->
<script Language="JavaScript">

<!--
	showMenu(1, "<%=Session["contactName"]%>", "<%=Session["contactEmail"]%>", "<%=Session["contactPhone"]%>", "<%=Session["contactAddInfo"]%>");

//-->
</script>
<table width="100%" border="0" cellpadding="0" cellspacing="0">

<% 
if (Session["tickerStatus"].Equals("0") && Session["tickerPosition"].Equals("1") ) { 
%>
<tr>
<td bgcolor="#848484" style="font-weight:bold;" nowrap><div id="marticker2Div" style="width:999px; overflow:hidden"> <%-- FB 2050 --%>
<% 
if(Session["tickerDisplay"].Equals("0")) { 
%>
<% 
Response.Write("My Conferences");
%> <%} %>
<% 
if(Session["tickerDisplay"].Equals("1")) { 
%>
<% 
Response.Write(Session["RSSTitle"]);
%> <%} %>
<%--Edited for FF--%><marquee id="marticker2" onmouseover="this.stop()" onmouseout="this.start()" 
bgcolor="<%=Session["tickerBackground"]%>" SCROLLAMOUNT="<%=Session["tickerSpeed"]%>"></> <%-- FB 2050 --%>
<%
       string dtCurrent;
		string frmtDT;
		DateTime dt;
		char[] splitter  = {'/'};

        myVRMNet.NETFunctions obj; 
        obj = new myVRMNet.NETFunctions();
        obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());
		
		dt = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());
		
		frmtDT = dt.ToString("dddd, MMMM dd yyyy");
		
		if(Session["FormatDateType"] != null)
		{
		
		    if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
		        frmtDT = dt.ToString("dddd, dd MMMM yyyy");
		
		}
		    
		    
		dtCurrent= frmtDT + ", " + " Current time is " + Session["systemTime"] ;
		
		if(Session["timeFormat"] != null)
		{
		    if(Session["timeFormat"].ToString() == "0")
		    {
		        dtCurrent= frmtDT + ", " + " Current time is " + dt.ToString("HH:mm") +" " ;						        					        
		    }
		    
		}
		
		if(Session["timeZoneDisplay"] != null)
		{
		    if(Session["timeZoneDisplay"].ToString() == "1")
		    {
		        dtCurrent +=  " " + Session["systemTimezone"]  +" " ;					        					        
		    }
		}    
        Response.Write("Welcome " + Session["userName"] + ". "+ dtCurrent + "  "+ Session["ticker"] + " ");
    %>
</marquee></div> <%-- FB 2050 --%>
</td>
</tr>
<% 
}
%>
<% 
if (Session["tickerStatus1"].Equals("0") && Session["tickerPosition1"].Equals("1") ) { 
%>
<tr style="height:15px">
<td bgcolor="#BDBDBD" style="font-weight:bold;" nowrap><div id="martic2Div" style="width:999px; overflow:hidden"> <%-- FB 2050 --%>
<% 
if(Session["tickerDisplay1"].Equals("0")) { 
%>
<% 
Response.Write("My Conferences");
%> <%} %>
<% 
if(Session["tickerDisplay1"].Equals("1")) { 
%>
<% 
Response.Write(Session["RSSTitle1"]);
%> <%} %>
<%--Edited for FF--%><marquee id="martic2" onmouseover="this.stop()" onmouseout="this.start()" 
bgcolor="<%=Session["tickerBackground1"]%>" SCROLLAMOUNT="<%=Session["tickerSpeed1"]%>"></> <%-- FB 2050 --%>
<%
       string dtCurrent1;
		string frmtDT1;
		DateTime dt1;
		char[] splitter1  = {'/'};

        myVRMNet.NETFunctions obj; 
        obj = new myVRMNet.NETFunctions();
        obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());
		
		dt1 = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());
		
		frmtDT1 = dt1.ToString("dddd, MMMM dd yyyy");
		
		if(Session["FormatDateType"] != null)
		{
		
		    if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
		        frmtDT1 = dt1.ToString("dddd, dd MMMM yyyy");
		
		}
		    
		    
		dtCurrent1 = frmtDT1 + ", " + " Current time is " + Session["systemTime"] ;
		
		if(Session["timeFormat"] != null)
		{
		    if(Session["timeFormat"].ToString() == "0")
		    {
		        dtCurrent1 = frmtDT1 + ", " + " Current time is " + dt1.ToString("HH:mm") +" " ;					        					        
		    }
		    
		}
		
		if(Session["timeZoneDisplay"] != null)
		{
		    if(Session["timeZoneDisplay"].ToString() == "1")
		    {
		        dtCurrent1 +=  " " + Session["systemTimezone"]  +" " ;					        					        
		    }
		} 
		
       Response.Write("Welcome " + Session["userName"] + ". "+ dtCurrent1 + "  "+ Session["ticker1"] + " ");
    %>
 
</marquee></div> <%-- FB 2050 --%>
</td>
</tr>

<%} %>

</table>
 </td>
    </tr>
  </table>	

</body>

</html>

<!-- FB 2299 Start -->
<script type="text/javascript">

function refreshImage()
{
  //changeFocus();
  var obj = document.getElementById("mainTop");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();

      if(obj.width > 804) // FB 2050
      obj.setAttribute('width','804'); // FB 2050
  }
  setMarqueeWidth(); // FB 2050
  //refreshStyle(); // FB 2050
  //setTimeout("refreshStyle()",500); // Commented for Refresh Issue
  return false;
}

// FB 2050 Starts
function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

function setMarqueeWidth()
{
    var screenWidth = screen.width - 25;
    if(document.getElementById('martickerDiv')!=null)
        document.getElementById('martickerDiv').style.width = screenWidth + 'px';
        
    if(document.getElementById('marticDiv')!=null)
        document.getElementById('marticDiv').style.width = screenWidth + 'px';
    
    if(document.getElementById('marticker2Div')!=null)
        document.getElementById('marticker2Div').style.width = (screenWidth-10) + 'px';
    
    if(document.getElementById('martic2Div')!=null)
        document.getElementById('martic2Div').style.width = (screenWidth-10) + 'px';
}

// FB 2050 Ends

window.onload = refreshImage;

function noError(){return true;} // FB 2050
window.onerror = noError;

//FB 2487 - Start
if (document.getElementById("errLabel") != null) 
    var obj = document.getElementById("errLabel");
else if(document.getElementById("lblError") != null)
    var obj = document.getElementById("lblError");
else if(document.getElementById("showConfMsg") != null)
    var obj = document.getElementById("showConfMsg");
else if(document.getElementById("LblMessage") != null)
    var obj = document.getElementById("LblMessage");  
else if(document.getElementById("MsgLbl") != null)
    var obj = document.getElementById("MsgLbl");
else if (document.getElementById("LblError") != null)
    var obj = document.getElementById("LblError");
if (obj != null) {

    var strInput = obj.innerHTML.toUpperCase();

    if (
          ((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
          || ((strInput.indexOf("EXITOSA") > -1) && !(strInput.indexOf("FALLIDA") > -1) && !(strInput.indexOf("ERROR") > -1)) //Spanish
          || ((strInput.indexOf("SUCC�S") > -1 || strInput.indexOf("�TABLIE") > -1) && !(strInput.indexOf("�CHEC") > -1) && !(strInput.indexOf("ERREUR") > -1)) //French            
          
        ) {
            obj.setAttribute("class", "lblMessage");
        obj.setAttribute("className", "lblMessage");
    }
    else {
        obj.setAttribute("class", "lblError");
        obj.setAttribute("className", "lblError");
    }
}
//FB 2487 - End

//FB 2738 Starts
scrollTo(0, 0); 
document.getElementById("topDiv").style.display = 'none';
//FB 2738 Ends
// FB 2719 Starts
/*
if (screen.width < 1300) {
    var menuId, menuLeft;
    for (var i = 1; i < 11; i++) {
        menuId = "menu" + i;
        if (document.getElementById(menuId) != null) {
            menuVal = document.getElementById(menuId).style.left.toUpperCase(); ;
            menuVal = parseInt(menuVal.replace("PX", ""), 10) - 50; // FB 2779
            document.getElementById(menuId).style.left = menuVal + "px";
        }
    }
}
*/
// FB 2719 Ends
//FB 2909 start
function getfilename(obj) {
    var fileInputVal = obj.value;
    fileInputVal = fileInputVal.replace("C:\\fakepath\\", "");
    if (navigator.userAgent.indexOf("MSIE") > -1)
        obj.parentNode.parentNode.childNodes[0].value = fileInputVal;
    else
        obj.parentNode.parentNode.childNodes[1].value = fileInputVal;
}
// FB 2909 End
//ZD 100381 - Start
if (document.getElementById('menu1').style.visibility == 'hidden') {
    window.location.href = window.location.href;
}

//ZD 101500
if (document.getElementById("topExpDiv1") != null && window.location.href.toLowerCase().indexOf('expressconference.aspx') == -1)
    document.getElementById("topExpDiv1").style.display = '<%= Session["hdModalDiv"]%>';

</script>
<!-- FB 2299 End -->