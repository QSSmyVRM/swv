<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="RoomCalendar" ValidateRequest="false" %> <%--ZD 100170--%>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc"%>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" tagprefix="dxw"%>
<%@ Register assembly="DevExpress.Web.ASPxTreeList.v10.2" namespace="DevExpress.Web.ASPxTreeList" tagprefix="dx" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --><!-- FB 2804 -->
<script language="javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>
<style type="text/css">
    #ModalDiv
    {
        opacity:0.5;
        filter: alpha(opacity=50);
    }
    .dxtlNode
    {
    	background-color:transparent;
    	background:transparent;
    }
</style>
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }
    else
    {
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
    }
%>

<% 
Boolean showCompanyLogo = false;
if(Request.QueryString["hf"] != null)
{
    if(Request.QueryString["hf"].ToString() == "1")
    {
	    showCompanyLogo = false;
%>
    <%--<!-- #INCLUDE FILE="inc/maintop2.aspx" --> --%> <%--ZD 101714--%>

<%
    }
    else if(Request.QueryString["gen"] != null && Request.QueryString["gen"].ToString() == "1")
    {

	 showCompanyLogo = false;
    %>
    <%--<!-- #INCLUDE FILE="inc/maintop2.aspx" --> --%>

    <%
     }
    else 
    {
%>

<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->--%>

<% }

}%>


<%--FB 3055-URL Starts--%>
    <script type="text/javascript">
        var ar = window.location.href.split("."); // ZD 100170
    ar = ar[ar.length - 1];
    var pg = "";
    if (ar.indexOf("?") > -1)
        pg = ar.split("?")[1];
    else
        pg = ar;

    var page = pg.toLowerCase();
    //alert(page);
    var cnt = 0;
    
    if (page.indexOf("v=1&r=1&hf=&d=&pub=&m=&comp=") > -1)
        cnt++;
    if (page.indexOf("f=v&hf=1&m=") > -1)
        cnt++;
     //ALLDEV-644 start
    if (page.toLowerCase().indexOf("v=1&r=1&hf=1&d=&pub=&m=&comp=&gen=1") > -1)
        cnt++;//ALLDEV-644 End

    if (cnt == 0) 
        window.location.href = "thankyou.aspx";
    

    //ALLDEV-644 start
    function GetbrowserLang() 
    {
        var browserlang = navigator.userLanguage || navigator.language;

        if (browserlang != "" && browserlang != null)
            document.getElementById("hdnBrowserLang").value = browserlang;
        else
            document.getElementById("hdnBrowserLang").value = "en-US";
                   
        //return false;
    } 
    //ALLDEV-644 End
    </script>  
<%--FB 3055-URL End--%>

<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src='../<%=Session["language"]%>/lang/calendar-en.js' ></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    if (path == "")//ALLDEV-644
        path = "Organizations/Org_11/CSS/Mirror/Styles/main.css"; //ALLDEV-644 
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>

<script type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    // ZD 100335 start
    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }
    setCookie("hdnscreenres", screen.width, 365);
    //ZD 100335 End

function noError(){return true;} // FB 2050
window.onerror = noError;

  var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
  parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
  
  
</script>
<html id="Html1" runat="server"  xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <script type="text/javascript" src="script/errorList.js"></script>
    <script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>
    <script type="text/javascript" src="extract.js"></script>
    <script type="text/javascript" src="script/mousepos.js"></script> <%-- ZD 102723 --%>
    <script type="text/javascript" src="script/showmsg.js"></script> <%-- ZD 102723 --%>
    <script type="text/javascript" src="script/roomsearch.js"></script> <%-- ZD 102723 --%>
    <script type="text/javascript" src="script/mytreeNET.js"></script>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <%--<link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />--%>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>
     <link rel="stylesheet" type="text/css" media="all" href="css/myprompt.css" /> <%--ALLDEV-644--%>
    <title>Room Calendar</title>
</head>
<body>
  <form id="frmCalendarRoom" runat="server" >
  <div id="ModalDiv" style="z-index:1100; position:fixed; left:0px; top:0px; width:1366px; height:768px; display:none; background-color:White" ></div>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" AsyncPostBackTimeout="2500" ><%--ZD 102358--%>
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" runat="server" id="hdnSelect" name="hdnSelect"  /><%--ZD 102358--%>
    <input type="hidden" id="cmd" value="GetSettingsSelect" />
    <input type="hidden" id="settings2locstr" name="settings2locstr" value="" runat="server" />
    <input type="hidden" id="helpPage" value="84" />
    <input type="hidden" id="hdnRoomIDs" runat="server" />
    <input type="hidden" id="MonthNum"  />
    <input type="hidden" id="isViewChanged" runat="server" />
    <input type="hidden" id="IsWeekOverLap" runat="server" />
    <input type="hidden" id="YrNum"  />
    <input type="hidden" id="IsWeekChanged" runat="server" />     
    <input type="hidden" id="Weeknum"  />
    <input type="hidden" id="selStatus" runat="server" /><%--FB 2804--%>
    <input type="hidden" id="selectedRooms"/><%--FB 2804--%>
    <input type="hidden" id="selectedListViewRooms"/><%--FB 2804--%>
    <input type="hidden" id="hdnRoomType"/><%--FB 2804--%>
    <input type="hidden" id="hdnRoomView"/>   
    <input type="hidden" id="hdnlstStartHrs"/>   
    <input type="hidden" id="hdnlstEndHrs"/>
    <input type="hidden" runat="server" id="hdnConfTime" name="hdnConfTime"  /><%--ZD 102008--%>   
    <input type="hidden" runat="server" id="hdnConfID" name="hdnConfID"  /><%--ZD 101942--%>
    <input type="hidden" id="hdnBrowserLang" runat="server" /> <%--ALLDEV-644--%>
    
  <%--FB 1630 - Changes--%>
  <table align="center"> <%--Added for FF--%>
       <tr>
        <td>
  <div id="footer" style="width:100%;">
    <table border="0"  width="100%" align="left"  style="vertical-align:bottom;">
        <tr>
            <td colspan="12">
              <%--<asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>--%> <%--ZD 100157--%>
               
                 <% if (Request.QueryString["gen"] != null) { 
                         if(Request.QueryString["gen"].ToString() == "1" )
  
                                   {%>
                       <h3><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, RoomCalendar_PublicRoomCal%>" runat="server"></asp:Literal></h3>
                       <asp:Label ID="lblTimezone" runat="server" Visible="false" CssClass="subtitleblueblodtext"></asp:Label> <%--ALLDEV-830--%>
                 <%}}
                 else
                 {%>
                      <%--<h3><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_RoomCalendar%>" runat="server"></asp:Literal></h3>--%>
                      <h3><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_RoomCalendar%>" runat="server"></asp:Literal><%--Edited For FF--%>     <%--<input type="button" name="PrsCal"  onfocus="this.blur()" value="Switch to Personal Calendar" class="altShortBlueButtonFormat" style="width:195px" onclick="javascript:viewrmdy(1)" /> </h3>--%>
            <%--Edited For FF--%>     <%--<input type="button" name="PrsCal"  onfocus="this.blur()" value="Switch to Personal Calendar" class="altShortBlueButtonFormat" style="width:195px" onclick="javascript:viewrmdy(1)" /> </h3>--%>
                 <asp:DropDownList runat="server" ID="lstCalendar" class="altText" tabindex="0" ToolTip="View" onChange="goToCal();javascript:DataLoading1('1');">
                        <asp:ListItem Value="3" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal3%>"></asp:ListItem>
                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal1%>"></asp:ListItem>
                        <%--<asp:ListItem Value="2" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal2%>"></asp:ListItem>--%> <%--ZD 101388 Commeted--%>
                </asp:DropDownList>
                   <%}
            %>
            </td> <%--ALLDEV-644 End--%>   
        </tr>
        <%--ALLOPS-58 - Start--%>
        <tr>            
            <td colspan="3"><asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label></td> <%--ZD 100157--%>                        
        </tr>
        <tr>
            <td></td>
            <td id="tdSelectRooms" runat="server" style="text-align:center"><%--FB 2804--%>
                <span class="lblError" style="margin-left: 100px;"><asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, RoomCalendar_PleaseSelectR%>" runat="server"></asp:Literal></span>               
            </td>
            <td id="tdClosed" align="center" runat="server" style="display: none;">
                <span class="lblError" style="margin-left: 100px;"><asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, RoomCalendar_RoomsClosedto%>" runat="server"></asp:Literal></span>
            </td>
        </tr>
        <%--ALLOPS-58 - End--%>
    </table>
  </div>
</td> <%--Added for FF--%>
      </tr> <%--ALLDEV-644 Start--%>
     </table> 
     <table border="0"  width="100%" align="left"  style="vertical-align:bottom;">
        <tr id="OrgRow" runat="server" align="left" style="display:none;">
                <td> 
                    <table width="95%" align="left" border="0">
                        <tr align="left"> <!-- FB 2050 -->
                            <td align="left" class="subtitleblueblodtext" width="10%"><asp:Label id="lblOrgNames" runat="server" text="<%$ Resources:WebResources, ConferenceList_lblOrgNames%>"></asp:Label> <!-- FB 2050 -->
                            </td>
                            <td valign="bottom" align="left"> <!-- FB 2050 -->
                                 <asp:DropDownList CssClass="altSelectFormat" ID="drpOrgs" runat="server" DataTextField="OrganizationName" OnSelectedIndexChanged="OrgIndexChanged" DataValueField="OrgID" AutoPostBack="true"></asp:DropDownList> <%--Edited for FF--%>
                            </td>
                        </tr>
                    </table>
                </td>
        </tr> <%--ALLDEV-644 End--%>
     </table>
  
  <%
if (Request.QueryString["hf"] != null)
{
    if (Request.QueryString["hf"].ToString() == "1")
    {
    %>
    <table width="100%" border="0">
        <tr><td align="center"><%--Edited For FF--%>
	        <input type="button" name="close" runat ="server" id="close" value="<%$ Resources:WebResources, Close%>" class="altMedium0BlueButtonFormat" onclick="javascript:window.close();">
<%     if (Request.QueryString["pub"] != null)
       {if (Request.QueryString["pub"].ToString() == "1")
        {%>
            <input type="button" name="login"  runat ="server" id="login" value="<%$ Resources:WebResources, Login%>" class="altShortBlueButtonFormat" onclick="javascript:window.location.href='~/en/genlogin.aspx'"> <%--FB 1830--%>
        </td></tr></table>
      <%}
        else
        {%> 
         </td></tr></table>
      <%}
      }
      else
      {%> 
        </td></tr></table>
    <%}
    }
  }%>
  <table width="100%" border="0" style="vertical-align:super;">
    <tr valign="top">
        <td id="ActionsTD" valign="top" align="Left" style="width:15%" runat="server">
            <div id="transDiv" align="right" style="display:none; z-index:10000; position: absolute; top:100; left:10; width:15%; height:500px;filter: alpha(opacity=10) !important; opacity:0.1; background-color:LightGrey" ></div><%--ZD 104491--%>
            <div align="left" id="othview" style="width:15%" >
                <table border="0" width="100%"   class="treeSelectedNode"> 
                 <% if (Request.QueryString["gen"] == null || Request.QueryString["gen"].ToString() != "1" ) 
                    {%>
                    <tr id="trofficehrDaily" style="display:block">
                        <td align="left">
                         <table border="0" style="border-collapse:collapse; text-align:left" >
                            <tr><%--ZD 100284--%>
                                <td nowrap="nowrap" valign='top'>
                                    <asp:CheckBox id="officehrDaily"  onclick="javascript:chnghrs();ShowHrs();fnStartDetectChange();"  AutoPostBack="false" runat="server" tooltip="show office hour only" />
                                    <label class="blackblodtext"  id="lblhour"><asp:Literal Text="<%$ Resources:WebResources, PersonalCalendar_lbl2%>" runat="server"></asp:Literal></label> &nbsp;    
                                    <label class="rmnamediv" for="officehr" id="lblOfficehrs" title="show office hours only" style="display:none;">
                                    <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ShowOfficeHoursOnly%>" runat="server"></asp:Literal></label>
                                    <%--ZD 100157 Starts--%>
                                </td>
                                <td align="left" nowrap="nowrap">
                                    <mbcbb:combobox id="lstStartHrs" cssclass="altText" causesvalidation="true"
                                    runat="server" enabled="true" rows="15" >
                                    </mbcbb:combobox>   
                                    <br />
                                    <asp:RequiredFieldValidator ID="reqlstStartHrs" runat="server" ControlToValidate="lstStartHrs"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="reglstStartHrs" runat="server" ControlToValidate="lstStartHrs" Style="white-space: pre-wrap;"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--ZD 100284--%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td></td><%--ZD 100284--%>
                                <td align="left" nowrap="nowrap">
                                    <mbcbb:combobox id="lstEndHrs" cssclass="altText" causesvalidation="true"
                                    runat="server" enabled="true" rows="15" >
                                    </mbcbb:combobox> 
                                    <asp:RequiredFieldValidator ID="reqlstEndHrs" runat="server" ControlToValidate="lstEndHrs"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="reglstEndHrs" runat="server" ControlToValidate="lstEndHrs" Style="white-space: pre-wrap;"
                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime1%>"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--ZD 100284--%>
                                    <%--ZD 100157 Ends--%>
                                </td>
                            </tr>
                         </table>
                        
                        </td>
                    </tr>
                    <tr id="trofficehrWeek" style="display:none">
                        <td align="left"  nowrap>
                            <asp:CheckBox id="officehrWeek" Checked="true" onclick="javascript:chnghrs();"   AutoPostBack="false" runat="server" tooltip="show office hour only" />
                            <asp:Label class="blackblodtext" runat="server"  id="lblweek" Text="<%$ Resources:WebResources, PersonalCalendar_lbl3%>"></asp:Label>
                            <label class="rmnamediv" for="officehr" id="Label1" title="show office hours only"  style="display:none;">Show Weekdays Only</label>
                        </td>
                    </tr>
                    <tr id="trofficehrMonth" style="display:none">
                        <td align="left"  nowrap>
                            <asp:CheckBox id="officehrMonth" Checked="true" onclick="javascript:chnghrs();"  AutoPostBack="false" runat="server" tooltip="show week days only" />
                            <asp:Label class="blackblodtext" runat="server"  id="lblmonth" Text="<%$ Resources:WebResources, PersonalCalendar_lbl3%>"></asp:Label>
                            <label class="rmnamediv" for="officehr" id="Label2" title="show office hours only" style="display:none;">Show Weekdays Only</label>
                        </td>
                    </tr>
                    <tr id="trDeleteConf" runat ="server" align="left" ><%--FB 1800--%><%--ZD 100963--%>
                      <td valign="top" align="left">
                        <asp:CheckBox id="showDeletedConf" runat="server" onclick="javascript:datechg();"></asp:CheckBox>
                        <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_DeletedConfere%>" runat="server"></asp:Literal></span>
                      </td>
                    </tr>
                     <%}
                    %>
                    <%--ZD 102358--%>
                    <tr style="display:none;">
                        <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Calendar%>" runat="server"></asp:Literal></td>
                    </tr>
                    <tr align="left"  valign="top">  
                        <td class="blackblodtext" > 
                        <hr />
                        <asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, SelectaDate%>" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd"
                        style="cursor: pointer; vertical-align: middle" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>"
                        onmouseover="return showCalendar('<%=txtSelectedDate.ClientID%>', 'cal_triggerd', 0, '<%=dtFormatType%>',3);"  <%--ALLDEV-644--%>
                        alt="Date selector" />
                        <a style="display:none;" href="#" class="name" onclick="return false;"><div id="flatCalendarDisplay" align="center" style="background-color:#D4D0C8;"></div></a> <%--ZD 100420--%><%--ZD 100639--%>
                        <div id="subtitlenamediv"></div>
                        </td> <%--ZD 100426--%>
                    </tr>
                    <tr align="center">
                        <td align="center"><hr style="height:2px;color:Black;" /></td>
                    </tr>
                    <tr>
                        <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Rooms%>" runat="server"></asp:Literal></td>
                    </tr>
                    <% if (Request.QueryString["gen"] == null || Request.QueryString["gen"].ToString() != "1" ) 
                    {%>
                    <tr >
                        <td align="left"><a id="RoomPoupup" runat="server" href="#" onclick="OpenRoomSearchCalen('frmCalendarRoom');"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_RoomPoupup%>" runat="server"></asp:Literal></a></td> <!-- FB 2050 -->
                    </tr>
                     <%}
                    %>
                    <tr>
                        <td  align="left"> <%--ALLDEV-644--%>
                            <table align="left"> <%--ALLDEV-644--%>
                                <tr>
                                    <td align="left" style="width:100%;">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                            <Triggers><asp:AsyncPostBackTrigger ControlID="btnfrmSearch" /></Triggers>
                                            <ContentTemplate>
                                                <input type="hidden" runat="server" id="hdnTopTier" name="hdnTopTier"  /><%--ZD 102358--%>
                                                <input type="hidden" runat="server" id="hdnMiddleTier" name="hdnMiddleTier"  /><%--ZD 102358--%>
                                                <asp:Panel style="display:block;" ID="PopupRoomPanel" Width="194px" runat="server" HorizontalAlign="Left"  CssClass="treeSelectedNode">
                                                    <input runat="server" id="selectedloc" type="hidden" />
                                                    <div align="left" id="conftypeDIV" style="width:100%;Height:325px;overflow:hidden" class="treeSelectedNode"> <%--ZD 100157 ZD 102358--%>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" class="blackblodtext" valign="top" width="100%">
                                                                    <table border="0" style="width: 100%">
                                                                        <tr>
                                                                            <td valign="top" align="left">                              
                                                                                <input type="button" value="<%$ Resources:WebResources, ExpressConference_btnCompare%>" id="btnCompare" runat="server" onclick="javascript:comparesel();" class="altMedium0BlueButtonFormat" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="display:none;">
                                                                            <td valign="top" align="left" class="blackblodtext">
                                                                              <asp:RadioButtonList ID="rdSelView" runat="server" CssClass="blackblodtext"
                                                                                  RepeatDirection="Horizontal" AutoPostBack="true" RepeatLayout="Flow" ToolTip="<%$ Resources:WebResources, Clicktoshowroomresources%>" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged">
                                                                                  <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, RoomTree_Level%>"></asp:ListItem>
                                                                                  <asp:ListItem  Value="2" Text="<%$ Resources:WebResources, RoomTree_List%>"></asp:ListItem>
                                                                               </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>                    
                                                                </td>
                                                            </tr>
                                                            <tr> <%--ZD 102358--%>
                                                                <td width="100%" align="left" style="font-weight: bold; font-size: small; color: green; font-family: arial;display:block" valign="top" >
                                                                    <asp:Panel ID="pnlLevelView" runat="server" Width="190px" ScrollBars="Auto"  Height="270px"  HorizontalAlign="Left" Visible="false" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"><%--Edited For FF--%> <%--ZD 100157--%>
                                                                        
                                                                        <dx:ASPxTreeList ID="treeRoomSelector" runat="server" AutoGenerateColumns="false" KeyFieldName="ID"  
                                                                         ParentFieldName="ParentID" OnNodeExpanding="nodeExpanding" OnSelectionChanged="selectionChanged" >
                                                                            <Columns>
                                                                                <dx:TreeListDataColumn FieldName="ID" Visible="false" />
                                                                                <dx:TreeListDataColumn FieldName="ParentID" Visible="false" />
                                                                                <dx:TreeListDataColumn FieldName="Name" ReadOnly="true" />
                                                                            </Columns>
                                                                            <Settings ShowColumnHeaders="false"  />    
                                                                            <SettingsLoadingPanel Enabled="False" />                                                                        
                                                                            <SettingsSelection Enabled="True" Recursive="true" />
                                                                            <SettingsBehavior AllowFocusedNode="true" />
                                                                            <ClientSideEvents BeginCallback="function(s, e) {fnInitiateDiv('block')}" SelectionChanged="function(s, e) {initGetCalendar();}" EndCallback="function(s, e) {getTabCalendarLocalNew();}" /><%--ZD 104491--%>
                                                                            <Styles>                   
                                                                                <Footer BackColor="Transparent"></Footer>
                                                                                <Indent BackColor="Transparent"></Indent>
                                                                                <IndentWithButton BackColor="Transparent"></IndentWithButton>
                                                                                <Header BackColor="Transparent"></Header>
                                                                                <FocusedNode BackColor="Transparent"></FocusedNode>
                                                                                <Cell BackColor="Transparent"></Cell>
                                                                                <SelectedNode BackColor="Transparent"></SelectedNode>
                                                                                <Node BackColor="Transparent"></Node>
                                                                            </Styles>
                                                                        </dx:ASPxTreeList>
                                                                        
                                                                        <asp:TreeView ID="treeRoomSelection" runat="server"  SkipLinkText ="" BorderColor="White" ShowCheckBoxes="All"  onclick="javascript:getTabCalendarLocal(event)"
                                                                            ShowLines="True" Width="100%"   > <%--ZD 100420--%>
                                                                            <NodeStyle CssClass="tabtreeNode"  />
                                                                            <RootNodeStyle Font-Size="XX-Small" CssClass="tabtreeRootNode"  />
                                                                            <ParentNodeStyle Font-Size="XX-Small" CssClass="tabtreeParentNode" />
                                                                            <LeafNodeStyle Font-Size="XX-Small" CssClass="tabtreeLeafNode" />
                                                                        </asp:TreeView>
                                                                    </asp:Panel>
                                                                    <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                                        BorderWidth="1px"  Height="100px" ScrollBars="Auto" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="arial" Font-Size="Small" ForeColor="Green" Visible="false"> <%--ZD 100157--%>
                                                                        
                                                                        <input type="checkbox" id="selectAllCheckBox" runat="server" onclick="CheckBoxListSelectTab('lstRoomSelection',this);" /><font size="2"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_SelectAll%>" runat="server"></asp:Literal></font>
                                                                        <br />
                                                                        <asp:CheckBoxList ID="lstRoomSelection" runat="server" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" onclick="javascript:datechg();" Font-Names="arial" RepeatLayout="Flow">
                                                                        </asp:CheckBoxList>
                                                                    </asp:Panel>                              
                                                                    <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                                        BorderWidth="1px" Height="290px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Size="Small">
                                                                        <table><tr align="center"><td>
                                                                        <asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_YouhavenoRoo%>" runat="server"></asp:Literal> 
                                                                        </td></tr></table>                            
                                                                    </asp:Panel>  
                                                                    <asp:TextBox runat="server" ID="txtTemp" Text="<%$ Resources:WebResources, RoomCalendar_txtTemp%>" Visible="false" ></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="false" ControlToValidate="txtTemp" runat="server" ></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>                  
                                                        </table>
                                                    </div>
                                                    <input align="middle" type="button" runat="server" style="width:150px;display:none;" id="ClosePUp" value=" Close " class="altButtonFormat" />
                                                    <asp:Button ID="btnMiddleTier" style="display:none" runat="server"/>
                                                    <asp:Button ID="btnfrmSearch" style="display:none" OnClick="FromRoomSearch" runat="server"/>
                                                </asp:Panel> 
                                           </ContentTemplate>
                                         </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td> 
                    </tr>
                </table>
            </div>
            <div>
              <% if (Request.QueryString["gen"] != null && Request.QueryString["gen"].ToString() == "1" ) 
                    {%>
              <table border="0"  align="left">
              <tr><td></td></tr>
                 <tr><td align="left">
                          
                        <button id="btnGoBack1" runat="server" style="width:0px;height:0px; background-color:transparent; border-color:transparent" onfocus="document.getElementById('btnGoBack').setAttribute('onfocus', '');document.getElementById('btnGoBack').focus();" ></button>
                        <input type="button" value="<%$ Resources:WebResources, ConferenceList_btnGoBack%>" id="btnGoBack" runat="server" onclick="javascript:GetbrowserLang();" onserverclick="GoToLogin" class="altMedium0BlueButtonFormat" />
                       </td></tr></table>
                    <%}
                    %>
            </div>
          </td>
          
          
          <td  style="width:2%"></td> 
          
          <td style="width:83%" valign="top">  <%--ZD 100806--%>
             <asp:UpdatePanel ID="PanelTab" runat="server" RenderMode="Inline" UpdateMode="Conditional" >
                <Triggers><asp:AsyncPostBackTrigger ControlID="btnDate" /></Triggers>
                <ContentTemplate>
                    <input type="hidden" id="HdnMonthlyXml" runat="server" />
     <input runat="server" id="IsSettingsChange" type="hidden" />
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                     </div><%--ZD 100678 End--%>
                    <input runat="server" id="selectedList" type="hidden" />                    
                    <input runat="server" id="selectedTierList" type="hidden" /> <%--ZD 102358--%>
                    <input runat="server" id="roomstrnames" type="hidden" /> <%--ZD 102086--%>
                    <input runat="server" id="selectedListRoomName" type="hidden" /> <%--ZD 101175--%>
                    <input type="hidden" id="IsMonthChanged" runat="server" />
                    <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
                   
                    <input name="Sellocstrname" type="hidden" id="Sellocstrname" runat="server"  />
                    <dxtc:ASPxPageControl ID="CalendarContainer" runat="server" ActiveTabIndex="0" Width="99%" EnableHierarchyRecreation="True">
                        <ClientSideEvents ActiveTabChanged="function(s,e){ChangeTabsIndex(s,e);}" />
                        <TabPages>
                             <dxtc:TabPage Text="<%$ Resources:WebResources, PersonalCalendar_DailyView%>" Name="Daily">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl1" runat="server">
                                        <table style="width:90%;vertical-align:top;overflow:auto;height:450px" border="0" cellpadding="0" cellspacing="0"> <%--ZD 100157--%>
                                            <tr valign="top" style="height:15px" id="trlegend1" runat="server"> <%--FB 1985 FB 2050--%>
                                                <td>
                                                    <table width="100%">
                                                      <tr id="trprivateview" runat="server"><%--ZD 100963--%>
                                                        <td id="TdAV1" runat="server" width="" height="15" bgcolor="#BBB4FF"></td>
														<%--ZD 100085 start--%>
                                                        <td id="TdAV" nowrap="nowrap" runat="server"><span id="spnAV" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnAV%>" runat="server"></asp:Literal></span></td>
                                                        <td width="" height="15" bgcolor="#F16855"></td>
                                                        <td nowrap="nowrap"><span id="spnRmHrg" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnRmHrg%>" runat="server"></asp:Literal></span></td>
                                                        <td id="TdP2p" runat="server" width="" height="15" bgcolor="#EAA2D4"></td>
                                                        <td id="TdA" nowrap="nowrap" runat="server"><span id="spnAudCon" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnAudCon%>" runat="server"></asp:Literal></span></td>
                                                        <td id="TdA1" runat="server" width="" height="15" bgcolor="#85EE99"></td>
                                                        <td id="TdP2p1" nowrap="nowrap" runat="server"><span id="spnPpConf"  runat="server"><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, RoomCalendar_spnPpConfW%>" runat="server"></asp:Literal></span></td>
                                                        <td id="TdDailyHotdeskingColor" runat="server" width="" height="" bgcolor= "#cc0033"></td><%--FB 2694--%>	
			                                            <td id="TdDailyHotdesking" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_TdDailyHotdesking%>" runat="server"></asp:Literal></td>
                                                        
                                                      </tr>
                                                      <tr id="trprivateconfview" runat="server"><%--ZD 100963--%>
                                                        
                                                        <td id="Td31" runat="server" width="" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                        <td id="Td32" nowrap="nowrap" runat="server"><span id="Span3"   runat="server"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, RoomCalendar_TdVMR%>" runat="server"></asp:Literal></span></td> <%--ZD 100806--%> 
			                                            
			                                            <td id="Td23" runat="server" width="" height="15" bgcolor= "#01DFD7"></td>
	                                                    <td id="Td24" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Td24%>" runat="server"></asp:Literal></td>
                                                        <td id="Tdbuffer1" runat="server" width="" height="15" bgcolor="#ffcc99"></td>
			                                            <td id="Tdbuffer2" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Tdbuffer2%>" runat="server"></asp:Literal></td>
			                                            <td id="Tdbuffer3" runat="server" width="" height="15" bgcolor="#cccc99"></td>
			                                            <td id="Tdbuffer4" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Tdbuffer4%>" runat="server"></asp:Literal></td><%-- Organization Css Module --%>
			                                            <td id="Td33" runat="server" width="" height="15" bgcolor="#FFFF00"></td> <%--ZD 100085--%>
			                                            <td id="Td34" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Td34%>" runat="server"></asp:Literal></td>
														<%--ZD 100085 End--%>
                                                      </tr>
                                                      
                                                      <%--ZD 100963 START--%>
                                                      <tr id="trReserved" runat="server">
                                                      <td id="TdReservedbgcolor" runat="server" width="15" height="15" bgcolor="#00FFFF"></td>
                                                        <td id="TdPrivateReserved" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, Reserved%>" runat="server"></asp:Literal></td>
                                                      </tr>
                                                      <%--ZD 100963 END--%>
                                                    
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr valign="top" >
                                                <td><%--ZD 100085 --%>
                                                  <% if (Request.QueryString["gen"] == null || Request.QueryString["gen"].ToString() != "1") 
                                                        {%>
                                                    <div id="imgInfo" name="imgInfo" align="left">
                                                        <span ID="lblRoomCal" runat="server" style="color: #666666;" >  <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, PersonalCalendar_lbl1%>" runat="server"> </asp:Literal></span> <%--ZD 100422--%>
                                                         <%} %>
                                                    </div>
                                                    <div id="DayPilotCalendar" style="vertical-align:middle;Width:99%;"><%--ZD 100151--%> <%--ZD 102089--%>
                                                        <table width="100%"><tr><td align="center"><asp:Label id="DailyViewDate" runat="server"></asp:Label></td></tr></table>
                                                         <DayPilot:DayPilotScheduler ID="schDaypilot" runat="server"  EmptyBackColor="#FFFFD5"
                                                          RowHeaderColumnWidths="70" DataResourceField="RoomID" Days="1" OnBeforeEventRender="BeforeEventRenderhandler"  CellDuration="15" 
                                                          ClientObjectName="dps" DataStartField="start" DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                          DataTagFields="ConferenceType,CustomDescription,Heading,confName,ID,Setup,MCUPreStart,ConfTime,MCUPreEnd,Tear,Hours,Minutes,Location,ConfSupport,CustomOptions,ConferenceTypeName,owner,requestor,HostName,RequesterName"
                                                          TimeRangeSelectedHandling="JavaScript" TimeRangeSelectedJavaScript="getConfType(start, end, resource)"  Height="340px"
                                                          Crosshair="Full" OnBeforeTimeHeaderRender="BeforeTimeHeaderRenderDaily" CellWidth="50"  Layout="TableBased" EventHeight="70" CellGroupBy="Hour" UseEventBoxes="Never" 
                                                          Visible="false" HeaderFontSize="8pt" EventFontSize="8pt"  BubbleID="Details" ShowToolTip="false" OnBeforeCellRender="BeforeCellRenderhandlerClosedDate" HeightSpec="Fixed" 
                                                          ><%--ZD 103918 ALLDEV-857--%>
                                                           
                                                          </DayPilot:DayPilotScheduler><%--FB 2588--%><%--FB 2804--%><%--ZD 100157--%><%-- ZD 100421--%>  
                                                        <DayPilot:DayPilotBubble ID="Details" runat="server" width="0"  Visible="true" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxw:ContentControl>   
                                </ContentCollection>
                              </dxtc:TabPage>
                              <dxtc:TabPage Text="<%$ Resources:WebResources, PersonalCalendar_WeekView%>" Name="Weekly">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl2" runat="server">
                                         <table style="width:90%;vertical-align:top;overflow:auto;height:450px" border="0" cellpadding="0" cellspacing="0"><%--ZD 100157--%>
                                            <tr valign="top" style="height:15px" id="trlegend2" runat="server"><%--FB 1985 FB 2050--%>
                                                <td>
                                                    <table  width="100%">
                                                         <tr id="trprivateweeklyview" runat="server"><%--ZD 100963--%>
                                                            <td  id="Td1" runat="server" width="" height="15" bgcolor="#BBB4FF"></td>
															<%--ZD 100085 start--%>
                                                            <td id="Td2" nowrap="nowrap" runat="server"><span id="spnAVW" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnAVW%>" runat="server"></asp:Literal></span></td>
                                                            <td width="" height="15" bgcolor="#F16855"></td>
                                                            <td nowrap="nowrap"><span id="spnRmHrgW" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnRmHrgW%>" runat="server"></asp:Literal></span></td>
                                                            <td id="Td3" runat="server" width="" height="15" bgcolor="#EAA2D4"></td>
                                                            <td id="Td4" nowrap="nowrap" runat="server"><span id="spnAudConW"  runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnAudConW%>" runat="server"></asp:Literal></span></td>
                                                            <td id="Td5" runat="server" width="" height="15" bgcolor="#85EE99"></td>
                                                            <td id="Td6" nowrap="nowrap" runat="server"><span id="spnPpConfW"  runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnPpConfW%>" runat="server"></asp:Literal></span></td>
                                                            <td id="TdWeeklyHotdeskingColor" runat="server" width="" height="15" bgcolor= "#cc0033"></td><%--FB 2694--%>	
			                                                <td id="TdWeeklyHotdesking" nowrap="nowrap" runat="server"><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, RoomCalendar_TdWeeklyHotdesking%>" runat="server"></asp:Literal></td>
                                                            
			                                                </tr>
			                                                <tr id="trprivateweeklyconf" runat="server"><%--ZD 100963--%>
			                                                <td id="Td27" runat="server" width="" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                            <td id="Td28" nowrap="nowrap" runat="server"><span id="Span1"   runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_TdVMR%>" runat="server"></asp:Literal></span></td> <%--ZD 100806--%>
			                                                <td id="Td21" runat="server" width="" height="15" bgcolor= "#01DFD7"></td>
		                                                    <td id="Td22" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Td22%>" runat="server"></asp:Literal></td>
                                                            <td id="Td7" runat="server" width="" height="15" bgcolor="#ffcc99"></td>
			                                                <td id="Td8" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Tdbuffer2%>" runat="server"></asp:Literal></td>
			                                                <td id="Td9" runat="server" width="" height="15" bgcolor="#cccc99"></td>
			                                                <td id="Td10" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Tdbuffer4%>" runat="server"></asp:Literal></td><%-- Organization Css Module --%>
			                                                <td id="Td35" runat="server" width="" height="15" bgcolor= "#FFFF00"></td><%--ZD 100085--%>
			                                                <td id="Td36" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Td34%>" runat="server"></asp:Literal></td>
			                                                </tr>
															<%--ZD 100085 End--%>
															<%--ZD 100963 START--%>
                                                      <tr id="trReservedweekly" runat="server">
                                                            <td id="Td39" runat="server" width="15" height="15" bgcolor="#00FFFF"></td>
                                                            <td id="Td40" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, Reserved%>" runat="server"></asp:Literal></td>
                                                      </tr>
                                                    
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td><%--ZD 100085 start--%>
                                                    <div id="Div2" align="left"    style="vertical-align:bottom;width:99%;"><%--ZD 100151--%>
                                                    <DayPilot:DayPilotScheduler  ID="schDaypilotweek" runat="server" EmptyBackColor="#FFFFD5"
                                                      RowHeaderColumnWidths="70" DataResourceField="RoomID" Days="7" CellDuration="1440"  OnBeforeEventRender="BeforeEventRenderhandler"  
                                                      ClientObjectName="dps" DataStartField="start" DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                      DataTagFields="ConferenceType,CustomDescription,Heading,confName,ID,Setup,MCUPreStart,ConfTime,MCUPreEnd,Tear,Hours,Minutes,Location,ConfSupport,CustomOptions,ConferenceTypeName,owner,requestor,HostName,RequesterName" 
                                                      Layout="TableBased" EventHeight="100" HeaderFontSize="8pt" Width="1024px"  
                                                      ShowNonBusiness="false" CellWidth="223" CellGroupBy="Week" Visible="false" UseEventBoxes="ShortEventsOnly" Crosshair="Full"  
                                                      BubbleID="DetailsWeekly" ShowToolTip="false" OnBeforeCellRender="BeforeCellRenderhandler" Height="330px"
                                                      EventFontSize="8pt" OnBeforeTimeHeaderRender="BeforeTimeHeaderRender" HeaderHeight ="33" HeightSpec="Fixed">                                                      
                                                      </DayPilot:DayPilotScheduler><%--FB 1851--%><%--FB 2311--%><%--ZD 100157--%><%-- ZD 100421 ALLDEV-857--%>  
                                                    <DayPilot:DayPilotBubble ID="DetailsWeekly" runat="server" width="0"  Visible="true" OnRenderContent="BubbleRenderhandler" ZIndex="999"/> 
                                                      
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxw:ContentControl>    
                                </ContentCollection>
                             </dxtc:TabPage>
                             <dxtc:TabPage Text="<%$ Resources:WebResources, PersonalCalendar_MonthView%>" Name="Monthly">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl3" runat="server">
                                         <table style="width:90%;vertical-align:top;overflow:auto;height:450px" border="0" cellpadding="0" cellspacing="0"><%--ZD 100157--%>
                                            <tr valign="top" style="height:15px" id="trlegend3" runat="server"><%--FB 1985 FB 2050--%>
                                                <td>
                                                    <table  width="100%">
                                                         <tr id="trprivatemonthlyview" runat="server"><%--ZD 100963--%>
															<%--ZD 100085 start--%>
                                                            <td id="Td11" runat="server" width="" height="15" bgcolor="#BBB4FF"></td>
                                                            <td id="Td12" nowrap="nowrap" runat="server"><span id="spnAVM"  runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnAVW%>" runat="server"></asp:Literal></span></td>
                                                            <td width="" height="15" bgcolor="#F16855"></td>
                                                            <td nowrap="nowrap"><span id="spnRmHrgM"  runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnRmHrgW%>" runat="server"></asp:Literal></span></td>
                                                            <td id="Td13" runat="server" width="" height="15" bgcolor="#EAA2D4"></td>
                                                            <td id="Td14" nowrap="nowrap" runat="server"><span id="spnAudConM"   runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnAudConW%>" runat="server"></asp:Literal></span></td>
                                                            <td id="Td15" runat="server" width="" height="15" bgcolor="#85EE99"></td>
                                                            <td id="Td16" nowrap="nowrap" runat="server"><span id="spnPpConfM"   runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_spnPpConfW%>" runat="server"></asp:Literal></span></td>
                                                            <td id="TdMonthlyHotdeskingColor" runat="server" width="" height="15" bgcolor= "#cc0033"></td><%--FB 2694--%>
		                                                    <td id="TdMonthlyHotdesking" nowrap="nowrap" runat="server"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, RoomCalendar_TdDailyHotdesking%>" runat="server"></asp:Literal></td>
                                                        </tr>
                                                        <tr id="trprivatemonthlyconf" runat="server"><%--ZD 100963--%>
                                                            <td id="Td29" runat="server" width="" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                            <td id="Td30" nowrap="nowrap" runat="server"><span id="Span2"   runat="server"><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, RoomCalendar_TdVMR%>" runat="server"></asp:Literal></span></td> <%--ZD 100806--%>
		                                                    <td id="Td25" runat="server" width="" height="15" bgcolor= "#01DFD7"></td>
		                                                    <td id="Td26" runat="server" nowrap="nowrap" runat="server"><asp:Literal Text="<%$ Resources:WebResources, RoomCalendar_Td22%>" runat="server"></asp:Literal></td>
                                                            <td id="Td17" runat="server" width="" height="15" bgcolor="#ffcc99"></td>
			                                                <td id="Td18" nowrap="nowrap" runat="server"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, RoomCalendar_Tdbuffer2%>" runat="server"></asp:Literal></td>
			                                                <td id="Td19" runat="server" width="" height="15" bgcolor="#cccc99"></td>
			                                                <td id="Td20" nowrap="nowrap" runat="server"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, RoomCalendar_Tdbuffer4%>" runat="server"></asp:Literal></td><%-- Organization Css Module --%>
		                                                    <td id="Td37" runat="server" width="" height="15" bgcolor= "#FFFF00"></td><%--ZD 100085--%>
		                                                    <td id="Td38" nowrap="nowrap" runat="server"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, RoomCalendar_Td34%>" runat="server"></asp:Literal></td>
															<%--ZD 100085 End--%>
                                                        </tr>
                                                      
                                                      <%--ZD 100963 START--%>
                                                      <tr id="trReservedmonthly" runat="server">
                                                      <td id="Td41" runat="server" width="15" height="15" bgcolor="#00FFFF"></td>
                                                        <td id="Td42" nowrap="nowrap" runat="server"><asp:Literal  Text="<%$ Resources:WebResources, Reserved%>" runat="server"></asp:Literal></td>
                                                      </tr>
                                                      <%--ZD 100963 END--%>
                                                    
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td><%--ZD 100085--%>
                                                     <div id="DayPilotCalendarMonth" align="left"    style="vertical-align:bottom;width:99%;"><%--ZD 100151--%>
                                                         <DayPilot:DayPilotScheduler ID="schDaypilotMonth" runat="server" Days="31" RowHeaderColumnWidths="70"  EmptyBackColor="#FFFFD5"
                                                          DataResourceField="RoomID" OnBeforeEventRender="BeforeEventRenderhandler" ClientObjectName="dps" DataStartField="start" 
                                                          DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                          DataTagFields="ConferenceType,CustomDescription,Heading,confName,ID,Setup,MCUPreStart,ConfTime,MCUPreEnd,Tear,Hours,Minutes,Location,ConfSupport,CustomOptions,ConferenceTypeName,owner,requestor,HostName,RequesterName" 
                                                          ViewType="Days" CellGroupBy="Month" CellDuration="1440" CellWidth="50"  Layout="TableBased" EventHeight="50" Height="320px"
                                                          ShowNonBusiness="false" HeaderFontSize="8pt" UseEventBoxes="ShortEventsOnly" Visible="false" Crosshair="Full" HeightSpec="Fixed"
                                                          BubbleID="DetailsMonthly" ShowToolTip="false" OnBeforeCellRender="BeforeCellRenderhandler"
                                                          EventFontSize="8pt" OnBeforeTimeHeaderRender="BeforeTimeHeaderRender" HeaderHeight ="33" /><%--1851--%><%--ZD 100157--%><%-- ZD 100421 ALLDEV-857--%>  
                                                         <DayPilot:DayPilotBubble ID="DetailsMonthly" runat="server" width="0"  Visible="true" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxw:ContentControl>
                                </ContentCollection>
                             </dxtc:TabPage>
                         </TabPages>
                    </dxtc:ASPxPageControl>
            <asp:Button UseSubmitBehavior="false" ID="btnDate" style="display:none" OnClick="ChangeCalendarDate" runat="server"/>
            <asp:Button ID="btnWeekly" style="display:none" OnClick="WeeklyBind" runat="server"/>
            <asp:Button ID="btnbsndhrs" style="display:none" OnClick="ChangeBusinessHour" runat="server"/>
            <div class="btprint" align="center" id="Div1" style="z-index:1;display:none;"></div>
        </ContentTemplate>
   </asp:UpdatePanel>
    <div class="btnprint" align="center" id="roomnumDIV" style="z-index:1;display:none;"></div>
 </td>
</tr>
</table>
<%--ZD 102008 Start--%>
     <div id="PopupFormList" align="center" style="position: absolute; overflow: hidden; z-index:1200;
            border: 1px; width: 450px; display: none; top: 300px; left: 510px; height: 450px;">
            <table align="center" class="tableBody" width="80%" cellpadding="5" cellspacing="5">
                <tr class="tableHeader">
                    <td colspan="2" align="left" class="blackblodtext">
                        <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ChooseFormHeading1%>" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" nowrap="nowrap">
                        <asp:RadioButtonList ID="rdEditForm" runat="server" onfocus="javascript:void(0);"
                            RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="1" CellPadding="3"
                            CellSpacing="3" Width="100%">
                            <asp:ListItem Text="<%$ Resources:WebResources, LongForm%>" Value="1"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, ExpressForm%>" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="btnOk" runat="server" type="button" class="altMedium0BlueButtonFormat"
                            onclick="javascript:return fnRedirectForm();" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" />
                    </td>
                    <td>
                        <input id="btnCancel" runat="server" type="button" class="altMedium0BlueButtonFormat"
                            onclick="javascript:fnDivClose();" value="<%$ Resources:WebResources, Cancel%>" />
                    </td>
                </tr>
            </table>
        </div>
        <%--ZD 102008 End--%>
        <%--ZD 101942 Starts--%>
        <div id="EditPopUp" align="center" style="position: absolute; overflow: hidden; z-index:1200;
        border: 1px; width: 450px; display: none; top: 300px; left: 510px; height: 450px;">
        <table align="center" class="tableBody" width="80%" cellpadding="5" cellspacing="5">
            <tr class="tableHeader">
                <td colspan="2" align="left" class="blackblodtext">
                    <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ChooseEditForm%>" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" nowrap="nowrap">
                    <asp:RadioButtonList ID="rdEditOption" runat="server" onfocus="javascript:void(0);" Width="100%"
                        RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="1" CellPadding="3"
                        CellSpacing="3">
                        <asp:ListItem Text="<%$ Resources:WebResources, LongFormEdit%>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, ExpressFormEdit%>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, ManageConference%>" Value="3"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="btnEditOk" runat="server" type="button" class="altMedium0BlueButtonFormat"
                        onclick="javascript:return fnEditOptionForm();" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" />
                </td>
                <td>
                    <input id="btnEditCancel" runat="server" type="button" class="altMedium0BlueButtonFormat"
                        onclick="javascript:fnEditClose();" value="<%$ Resources:WebResources, Cancel%>" />
                </td>
            </tr>
        </table>
    </div>
    <%--ZD 101942 Ends--%>
    <table>
</table>
<asp:TextBox ID="txtType" Visible="false"  runat="server"></asp:TextBox>  
<input type="hidden" ID="txtSelectedDate" runat="server" />
<input type="hidden" ID="txtSelectedDate1" runat="server" />
</form>
</body>
</html>
  
  
<script language="javascript">

var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_initializeRequest(initializeRequest);

prm.add_endRequest(endRequest);

var postbackElement;

//ZD 104491 - Start 
function initializeRequest(sender, args) 
{
    fnInitiateDiv('block');
    if (prm.get_isInAsyncPostBack())
    {
        args.set_cancel(true);
    }
    DataLoading("1");
    document.body.style.cursor = "wait";
}

function endRequest(sender, args) 
{
    fnInitiateDiv('none');
    document.body.style.cursor = "default";
    DataLoading("0"); 
}
//ZD 104491 - End

function ChangeTabsIndex(s,e)
{
    var activetab = s.GetActiveTab();
   var tabname;
   tabname = activetab.name;
   var hour = document.getElementById('trofficehrDaily');
   var week = document.getElementById('trofficehrWeek');
   var month = document.getElementById('trofficehrMonth');
   var selectedListViewRooms = document.getElementById("selectedListViewRooms").value; 
   //ZD 100151
   var IsSettingsChange = document.getElementById("IsSettingsChange"); 
   if(IsSettingsChange)
        IsSettingsChange.value = 'Y';
   
   selectedRooms = $('#selectedRooms').val(); 
   var hdnRoomType = $('#hdnRoomType').val();
      
   if(tabname == "Daily")//FB 2804
   {
   document.getElementById("hdnRoomView").value="1"; //FB 2948
  // if(document.getElementById("selStatus").value == "0")    
  if(document.getElementById("selectedListRoomName").value=="" && document.getElementById("selectedList").value == "") //ZD 101175 //ZD 102358
   {
   //ZD 102358
       //document.getElementById("tdClosed").style.display='block'; 
       document.getElementById("tdSelectRooms").style.display='block';
    }
   else   
       document.getElementById("tdClosed").style.display='none'; 
     
     //ZD 100151  
     document.getElementById ("btnDate").click();
   if( hour != null ) //ALLDEV-644
     hour.style.display = 'block';
   if( week != null )    //ALLDEV-644
      week.style.display = 'none';
   if( month != null )  //ALLDEV-644
     month.style.display = 'none';
   }
   else if(tabname == "Weekly")
   {
    document.getElementById("hdnRoomView").value="2";//FB 2948
    //if(hdnRoomType == 1) //ZD 102560       
//     if(document.getElementById("selectedListRoomName").value=="" && hdnRoomType == 1) //ZD 101175
//        {
//      if(selectedListViewRooms == 0)  
//        document.getElementById("tdSelectRooms").style.display='block';
//      else          
//        document.getElementById("tdSelectRooms").style.display='none';            
//    }
//    else
//    {    
//      //if(selectedRooms == 0)  
//       if(document.getElementById("selectedListRoomName").value=="" && selectedRooms == "0") //ZD 101175
//        document.getElementById("tdSelectRooms").style.display='block';
//      else          
//        document.getElementById("tdSelectRooms").style.display='none';
//    }

   document.getElementById("tdClosed").style.display='none';

    //ZD 102397 - Start //ZD 102324
   if (document.getElementById('selectedList') != null) 
    {
        if (document.getElementById('selectedList').value != "")
            document.getElementById("tdSelectRooms").style.display = 'none';
        else
            document.getElementById("tdSelectRooms").style.display = 'block';
    }
    //ZD 102397 - End
   if( hour != null )   //ALLDEV-644
     hour.style.display = 'none';
   if( week != null )   //ALLDEV-644
     week.style.display = 'block';
   if( month != null )  //ALLDEV-644
     month.style.display = 'none';
   //ZD 100151  
   //if(document.getElementById("IsWeekOverLap").value == "Y")
       document.getElementById ("btnDate").click();
   }
   else if(tabname== "Monthly")
   {
    document.getElementById("hdnRoomView").value="3"; //FB 2948    
    if(hdnRoomType == 1)
    {            
        //if(selectedListViewRooms == 0)  
         if(document.getElementById("selectedListRoomName").value=="" && selectedListViewRooms == 0) //ZD 101175
          document.getElementById("tdSelectRooms").style.display='block';
        else          
          document.getElementById("tdSelectRooms").style.display='none';            
    }
    else
    {    
        //if(selectedRooms == 0)  
         if(document.getElementById("selectedListRoomName").value=="" && document.getElementById("selectedList").value == "") // selectedRooms == 0) //ZD 101175 //ZD 102458
          document.getElementById("tdSelectRooms").style.display='block';
        else          
          document.getElementById("tdSelectRooms").style.display='none';
    }
   document.getElementById("tdClosed").style.display='none';   
     //ZD 100151  
     document.getElementById ("btnDate").click();

   if( hour != null )//ALLDEV-644
     hour.style.display = 'none';
   if( week != null )//ALLDEV-644   
     week.style.display = 'none';
   if( month != null )//ALLDEV-644
     month.style.display = 'block';
   }

  

   if(fnTimeScroll()) //ZD 100157
   fnTimeScroll();
   } 
var myPopup;
var IE4 = (document.all) ? true : false;


var	dayname = new Array();
var pageleft = 10;
var totaltop = 190, topleft = 245, totalh = 960, calendarh = 180, totalw = getwinwid() - topleft - pageleft, yaxlwid = 65, lineh = 15, splitwid=1;


var allowedrmnum = 0,	isOhOn = false;
var strConfs = "", startDate; 
var aryRooms="", selConfTypeCode;

var isWdOn = false, confmonthfirst = "";

function getwinwid() 
{
	var myWidth = 0, myHeight = 0;
	if( typeof( window.innerWidth ) == 'number' ) {
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
	  
	return (myWidth);
}


function OpenModal()
{
    if(document.getElementById("RoomPoupup"))
        document.getElementById("RoomPoupup").click();
}


function getConfType(start, end, rmID) //ZD 102089 //ZD 103918
{

  if(document.getElementById("selStatus").value == 0)//FB 2804
     return false;
   
   document.getElementById("locstrname").value = rmID //ZD 102089

  //ZD 102008 - Start
  var time = start.toString(); //Edited for FF
  var conftime = new Date(time);
  
  //ZD 103918
  var etime = end.toString();
  var confetime = new Date(etime);
  var diffMin = Math.floor(((confetime.getTime() - conftime.getTime())/1000)/60);
  
  var hour,min;
  
  var splttim = conftime.toTimeString().split(':');
  
  hour = splttim[0];
  min = splttim[1];
  
  if(timeFormat == "1")
  {
  
      hour = conftime.getHours();
      min = conftime.getMinutes();
      
      
      if(eval(hour) < 10)
        hour = "0"+hour
        
      if(eval(min) < 10)
        min = "0"+min
        
      //min = min +" "+conftime.toLocaleTimeString().toString().split(" ")[1]
      min = min +" "+ conftime.format("tt"); //FB 2108
  }
  
  
    
  if (queryField("hf") == "1")
    {
        alert(CannotSChedule);
        return false;
    }
	
    else {
        
        if ('<%=Session["hasConference"]%>' == '1' && '<%=Session["hasExpConference"]%>' == "1") 
        {
            document.getElementById('hdnConfTime').value = confdate + "_" + hour + "_" + min + "_" + diffMin;//ZD 103918
            document.getElementById("ModalDiv").style.display = 'block';
            document.getElementById("PopupFormList").style.display = 'block';
            if(document.getElementById('rdEditForm_1') != null)
              document.getElementById('rdEditForm_1').checked = false;
            if(document.getElementById('rdEditForm_0') != null)
              document.getElementById('rdEditForm_0').checked = false;       
                        
        }
        else if ('<%=Session["hasConference"]%>' == '1') 
        {
            //url = "ConferenceSetup.aspx?t=n&sd=" + confdate + "&st=" + hour+":"+min +"&rms=" + rmID; //document.getElementById("selectedList").value; //ZD 102089
            url = "ConferenceSetup.aspx?t=n&sd=" + confdate + "&st=" + hour+":"+min +"&dur=" + diffMin +"&rms=" + document.getElementById("selectedList").value; //ZD 102357 //ZD 103918
            DataLoading("1");
            window.location.href = url;
        }
        else if ('<%=Session["hasExpConference"]%>' == '1') 
        {
            //url = "ExpressConference.aspx?t=n&sd=" + confdate + "&st=" + hour+":"+min +"&rms="+ rmID;; //ZD 102086 //ZD 102089
            url = "ExpressConference.aspx?t=n&sd=" + confdate + "&st=" + hour+":"+min +"&dur=" + diffMin +"&rms=" + document.getElementById("selectedList").value; //ZD 102357 //ZD 103918
            DataLoading("1");
            window.location.href = url;
        }
        else 
        {
            return false;
        }        
        
  }
     
//        var mmm_numc, mm_strc, mm_aryc,mmm_strc,mmm_intc,menu;
//        
//        var menuset = "0";
//        var time ;
//        

//        mm_aryc = mm_strc.split("-");
//        mmm_strc = mm_aryc[0];
//        mmm_aryc = mmm_strc.split("*");
//        mmm_numc = parseInt(mmm_aryc[0], 10);
//        mmm_intc = parseInt(mmm_aryc[1], 10);
//        for (i=1; i<=mmm_numc; i++) {
//        menu = ( mmm_intc & (1 << (mmm_numc-i)) ) ? ("menu_" + i) : "";
//	    //FB 1929
//        if(menu == "menu_3")
//            menuset = "1";
//	    
//        }
//        //FB 1929
//        if(menuset > 0)
//        {     
//           
////        if(confirm("Are you sure you want to create a new conference beginning " + confdate + " @ " + hour+":"+min  + "?"))
////        {
//              //FB 1723
//              
//               if(document.getElementById("selectedList"))	             
//	            url =  url = "ConferenceSetup.aspx?t=n&sd=" + confdate + "&st=" + hour+":"+min +"&rms=" + document.getElementById("selectedList").value;//Code added for FB 1452
//            
//	        
//            DataLoading("1");
//            window.location.href=url;
////        }

//         }
//    }

}

function fnRedirectForm() {
      
      var a,b;
      if(document.getElementById('hdnConfTime') != null)
          a = document.getElementById('hdnConfTime').value;

      if(a != null)
        b = a.split('_');

      if (document.getElementById('rdEditForm_0').checked == true) 
      {
          url = "ConferenceSetup.aspx?t=n&sd=" + b[0] + "&st=" + b[1]+":"+b[2] +"&dur=" + b[3] +"&rms=" + document.getElementById("selectedList").value; //ZD 102357 //ZD 103918
          DataLoading("1");       
          window.location.href = url;   
      }
      else if (document.getElementById('rdEditForm_1').checked == true)
      {
          url = "ExpressConference.aspx?t=n&sd=" + b[0] + "&st=" + b[1]+":"+b[2] +"&dur=" + b[3] +"&rms=" + document.getElementById("selectedList").value; //ZD 102357 //ZD 103918
          DataLoading("1");
          window.location.href = url;
      }
      

  }
  function fnDivClose() {      
      document.getElementById("PopupFormList").style.display = 'None';
      document.getElementById("ModalDiv").style.display = 'None';
  }
  //ZD 102008 - End

function ViewDetails(e)
{
 
  url = "ManageConference.aspx?confid="+e.value()+"&t=hf";
  myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
  myPopup.focus();
  
}  
//ZD 101942 Starts
function  ViewConfDetails(e) {
    var confid;
    var conferenceID = new Array();
    confid = ViewConfDetails.arguments;
    conferenceID = confid[0].split('|');

    if (document.getElementById('hdnConfID') != null)
        document.getElementById('hdnConfID').value = conferenceID[0];
    
    if(conferenceID[1] == "0")
    {
       url = "ManageConference.aspx?confid=" + conferenceID[0] + "&t=hf";
        myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
        myPopup.focus();
    }
    else  if ('<%=Session["hasConference"]%>' == '1' && '<%=Session["hasExpConference"]%>' == "1") {
        document.getElementById("EditPopUp").style.display = 'block';
        document.getElementById("ModalDiv").style.display = 'block';
        //ZD 103051 START
        if (document.getElementById('rdEditOption_1') != null)
        {
            document.getElementById('rdEditOption_1').checked = false;
            if(conferenceID[2] == "0")
            {
                document.getElementById('rdEditOption_1').parentNode.style.display = "none";
            }
            else if(document.getElementById('rdEditOption_1').parentNode.style.display == "none")
            {
                document.getElementById('rdEditOption_1').parentNode.style.display = "block";
            }
        }
        if (document.getElementById('rdEditOption_0') != null)
        {
            document.getElementById('rdEditOption_0').checked = false;
            if(conferenceID[2] == "0")
            {
                document.getElementById('rdEditOption_0').parentNode.style.display = "none";
            }
            else if(document.getElementById('rdEditOption_0').parentNode.style.display == "none")
            {
                document.getElementById('rdEditOption_0').parentNode.style.display = "block";
            }
        }
        if (document.getElementById('rdEditOption_2') != null)
            document.getElementById('rdEditOption_2').checked = false;
    }
    else if ('<%=Session["hasConference"]%>' == '1') {
        document.getElementById("EditPopUp").style.display = 'block';
        document.getElementById("ModalDiv").style.display = 'block';
        if (document.getElementById('rdEditOption_0') != null)
         {
            document.getElementById('rdEditOption_0').checked = false;
            if(conferenceID[2] == "0")
            {
                document.getElementById('rdEditOption_0').parentNode.style.display = "none";
            }
            else if(document.getElementById('rdEditOption_0').parentNode.style.display == "none")
            {
               document.getElementById('rdEditOption_0').parentNode.style.display = "block";
            }
        }
        if (document.getElementById('rdEditOption_2') != null)
            document.getElementById('rdEditOption_2').checked = false;

        var element = document.getElementById('rdEditOption');
        if (document.getElementById('rdEditOption_1') != null) //104126
            element.deleteRow(1);
        
    }
    else if ('<%=Session["hasExpConference"]%>' == '1') {

        document.getElementById("EditPopUp").style.display = 'block';
        document.getElementById("ModalDiv").style.display = 'block';
        if (document.getElementById('rdEditOption_1') != null)
        {
            document.getElementById('rdEditOption_1').checked = false;
            if(conferenceID[2] == "0")
            {
                document.getElementById('rdEditOption_1').parentNode.style.display = "none";
            }
            else if(document.getElementById('rdEditOption_1').parentNode.style.display == "none")
            {
               document.getElementById('rdEditOption_1').parentNode.style.display = "block";
            }
        }
        //ZD 103051 END
        if (document.getElementById('rdEditOption_2') != null)
            document.getElementById('rdEditOption_2').checked = false;

        var element = document.getElementById('rdEditOption');
        if (document.getElementById('rdEditOption_0') != null)
            element.deleteRow(0);
    }
    else {
        return false;
    }
}
function fnEditOptionForm() {
var confid;

if (document.getElementById('hdnConfID') != null)
    confid = document.getElementById('hdnConfID').value;

if (document.getElementById('rdEditOption_0') != null) {
    if (document.getElementById('rdEditOption_0').checked == true) {
        url = "ConferenceSetup.aspx?t=&confid=" + confid;
        DataLoading("1");
        window.location.href = url;
    }
}
if (document.getElementById('rdEditOption_1') != null) {
    if (document.getElementById('rdEditOption_1').checked == true) {
        url = "ExpressConference.aspx?t=&id=" + confid;
        DataLoading("1");
        window.location.href = url;
    }
}
if (document.getElementById('rdEditOption_2') != null) {
    if (document.getElementById('rdEditOption_2').checked == true) {
        url = "ManageConference.aspx?t=&confid=" + confid;
        DataLoading("1");
        window.location.href = url;
    }
}
}
function fnEditClose() {
    document.getElementById("EditPopUp").style.display = 'None';
    document.getElementById("ModalDiv").style.display = 'None';
}

//ZD 101942 Ends
/* FB 1659 code starts ... */
function ViewConfDetailsOId()
{
    var args = ViewConfDetails.arguments;
    url = "ManageConference.aspx?confid="+ args[0] +"&t=hf";
    myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
    myPopup.focus();
  
}  
/* FB 1659 code ends ... */

function initsubtitle ()
{
   
   var dd, m = "";
    var monthNew,yrNew
 	// ZD 103182
    if('<%=Session["FormatDateType"]%>' == "dd/MM/yyyy")
    {
        var dateVal = confdate.split('/');
        confdate = dateVal[1] + "/" + dateVal[0] + "/" + dateVal[2];
    }
    
    dd = new Date(confdate);
                     
    monthNew = (dd.getMonth() + 1);
    yrNew = dd.getFullYear();
    document.getElementById ("IsWeekOverLap").value = "";
    document.getElementById ("IsMonthChanged").value = "";
    
    if(WeekOverlap(confdate))
           document.getElementById ("IsWeekOverLap").value = "Y";
    if(document.getElementById ("MonthNum").value != monthNew || document.getElementById ("YrNum").value != yrNew )
    {
        document.getElementById ("YrNum").value = yrNew ;
        document.getElementById ("MonthNum").value = monthNew;
        document.getElementById ("IsMonthChanged").value = "Y";
    }
    else
    {
    
        document.getElementById ("IsMonthChanged").value = "";
        
    }
    
}
//FB 2804
var selectedListArray = new Array();
var selectedListViewRooms;
function datechg() {
if(datechg.arguments[0])
{
    
    if(datechg.arguments[0] == "1")
    {
    
    if(queryField("hf") == "1")
    {
       document.getElementById("txtSelectedDate").value = GetDefaultDate(queryField("d"),dtFormatType);;
        
     }
     
     document.getElementById ("IsMonthChanged").value = "Y"; 
     document.getElementById ("btnDate").click();       
    }
}
else
{
	//document.getElementById ("flatCalendarDisplay").style.display = "none";
    //ZD 102358
    var dateChange = false; // ZD 103182
    var selecDate = document.getElementById("txtSelectedDate").value

    if('<%=Session["FormatDateType"]%>' == "dd/MM/yyyy")
    {
        var dateVal = selecDate.split('/');
        selecDate = dateVal[1] + "/" + dateVal[0] + "/" + dateVal[2];
    }

    if(selecDate != confdate)
        dateChange = true;
    
    if(document.getElementById("txtSelectedDate").value == "")
        document.getElementById("txtSelectedDate").value=confdate;
    else
        confdate = document.getElementById("txtSelectedDate").value;
    
    //FB 2804 - Start
     var SelectedWeekdays, Selectedday, str="", selectedRooms1;
     var isClosed = new Boolean();
     var selDays = new Array();
     isClosed = false;
     //Latest Issue
     var confstdate = '';
     confstdate = GetDefaultDate(confdate,'<%=dtFormatType%>');
     
     SelectedWeekdays = new Date(confstdate);     
     Selectedday = SelectedWeekdays.getDay()+1;
     
     if( "<%=Session["DaysClosed"]%>" != null)
        str = "<%=Session["DaysClosed"].ToString()%>";       
        
        selDays = str.split(',');
        
        for(var i =0; i<selDays.length; i++)
        {
            if( selDays[i] == Selectedday)        
                isClosed = true;
        }
        if(isClosed)
        {
           if(document.getElementById("hdnRoomView").value == 2 || document.getElementById("hdnRoomView").value == 3) //FB 2948
                document.getElementById("tdClosed").style.display='none';
           else
                document.getElementById("tdClosed").style.display='block';
            document.getElementById("selStatus").value = "0";
        }
        else
        {
            document.getElementById("tdClosed").style.display='none';                  
            document.getElementById("selStatus").value = "1";
        }
        //FB 2804 - End
    initsubtitle();
    if(document.getElementById ("IsMonthChanged").value == "Y")//ZD 102560
        document.getElementById ("btnDate").click(); 
    else //if(dateChange) // ZD 103169 //Latest Issue 
    {
        document.getElementById ("IsMonthChanged").value = "Y";
        document.getElementById ("btnDate").click(); 
    }
    
 }
 //FB 2804


 //ZD 102397 102324
    if (document.getElementById('selectedList') != null) 
    {
        if (document.getElementById('selectedList').value != "")
            document.getElementById("tdSelectRooms").style.display = 'none';
        else
            document.getElementById("tdSelectRooms").style.display = 'block';

    }
//ZD 102560
// $('#lstRoomSelection input:checked').each(function() {
//            selectedListArray.push($(this).attr('name'));
//        });
//        $('#selectedListViewRooms').val(selectedListArray.length);
//      selectedListViewRooms = $('#selectedListViewRooms').val();             

//    selectedRooms = $('#selectedRooms').val();       
//    if(isClosed)    
//    { 
//      document.getElementById("tdSelectRooms").style.display='none';
//    }
//    else
//    {                      
//        var hdnRoomType = $('#hdnRoomType').val();            
//        if(hdnRoomType == 1)
//        {        
//          if(selectedListViewRooms == 0)  
//           if(document.getElementById("selectedListRoomName").value=="" && selectedListViewRooms == 0) //ZD 101175
//            document.getElementById("tdSelectRooms").style.display='block';
//          else          
//            document.getElementById("tdSelectRooms").style.display='none';            
//        }
//        else
//        {        
//            if(selectedRooms == 0)                  
//             if(document.getElementById("selectedListRoomName").value=="" && selectedRooms == 0) //ZD 101175
//                document.getElementById("tdSelectRooms").style.display='block';                
//            else          
//                document.getElementById("tdSelectRooms").style.display='none';
//        }
//    }   
//     
// selectedListArray = new Array();

    

}

//FB 2804 - Start
/*function showhide(bool)
{
    if(bool == '0')
    {    
        document.getElementById("CalendarContainer_schDaypilot").childNodes[0].style.visibility = "hidden";
        document.getElementById("CalendarContainer_schDaypilotweek").childNodes[0].style.visibility = "hidden";
        document.getElementById("CalendarContainer_schDaypilotMonth").childNodes[0].style.visibility = "hidden";
        return false;        
     }   
    else
    {
        document.getElementById("CalendarContainer_schDaypilot").childNodes[0].style.visibility = "visible";
        document.getElementById("CalendarContainer_schDaypilotweek").childNodes[0].style.visibility = "visible";
        document.getElementById("CalendarContainer_schDaypilotMonth").childNodes[0].style.visibility = "visible";
        
    }
}*/
//FB 2804 - End

function DataLoading(val)
{
    if (val=="1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        //document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif'  alt='Loading..'> "; // FB 2742
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
        
     //FB 2804
    //var stat = document.getElementById("selStatus").value;
    //if(stat != "" )
    //setTimeout("showhide('" + stat +"')",250);                       
}
function DataLoading1(val)
 {
    //alert(val);
    if (val == "1")
     document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
     document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}   
function showcalendar()
{
	if (document.getElementById ("flatCalendarDisplay").style.display == "")
		document.getElementById ("flatCalendarDisplay").style.display = "none";
	else
		document.getElementById ("flatCalendarDisplay").style.display = "";
}

startDate = queryField("d");




            
var dtFormatType, convDate ,calendarTyp ,timeFormat;
dtFormatType = '<%=dtFormatType%>';
calendarTyp = '<%=CalendarType%>';
timeFormat = '<%=timeFormat%>';
var confdate,confweekmon,curwkDate
var curdate = new Date();
if (document.getElementById("txtSelectedDate").value == "") {
	confdate = (curdate.getMonth() + 1) + "/" + curdate.getDate() + "/" + curdate.getFullYear();
    
    if(dtFormatType =="dd/MM/yyyy") //ZD 103171
        confdate = curdate.getDate()+ "/" +(curdate.getMonth() + 1) + "/" + curdate.getFullYear();

	confweekmon = addDates(curdate.getFullYear(), curdate.getMonth() + 1, curdate.getDate(), (curdate.getDay() == 0) ? -6 : (1-curdate.getDay()) );
	document.getElementById("<%=txtSelectedDate.ClientID %>").value=confdate;
	//showFlatCalendar(0, '%m/%d/%Y'); //ZD 102358
} else {
	confdate = document.getElementById("txtSelectedDate").value;
	curwkDate = new Date(confdate);
	confweekmon = addDates(curwkDate.getFullYear(), curwkDate.getMonth() + 1, curwkDate.getDate(), (curwkDate.getDay() == 0) ? -6 : (1-curwkDate.getDay()) );
	//showFlatCalendar(0, '%m/%d/%Y', document.getElementById("txtSelectedDate").value); //ZD 102358
}
//ZD 102358
//document.getElementById ("flatCalendarDisplay").style.position = 'relative';
//document.getElementById ("flatCalendarDisplay").style.top = 0;
//document.getElementById ("flatCalendarDisplay").style.height = 150;
//document.getElementById ("flatCalendarDisplay").style.textAlign = "center";
//document.getElementById ("flatCalendarDisplay").style.display = "none";


DataLoading("0");
initsubtitle();

//datechg("1");



function goToCal()
{
        if(document.getElementById("lstCalendar") != null)
        {
		    //ZD 101388 Starts Commeted
		    /*if (document.getElementById("lstCalendar").value == "2")//FB 1779 
		    {
			   if( "<%=Session["isExpressUser"]%>" == 1 ) //FB 1779
			      window.location.href = "ConferenceList.aspx?t=3";
			   else
			     window.location.href = "ConferenceList.aspx?t=2";
		    }*/
		    if (document.getElementById("lstCalendar").value == "1"){
			    viewrmdy(1);
			    //window.location.href = "PersonalCalendar.aspx?v=1&r=1&hf=&d=" ; //code changed for calendar conversion FB 412
		    }
			//FB 2501 Call Monitoring
		    /*if (document.getElementById("lstCalendar").value == "4"){
                   window.location.href = "MonitorMCU.aspx";
            } 
            //FB 2501 P2P Call Monitoring
		    if (document.getElementById("lstCalendar").value == "5"){
                   window.location.href = "point2point.aspx";
           }*/
           //ZD 101388 End Commeted
		}
}

if(queryField("hf") == "1")
{

   if(document.getElementById("lstCalendar") != null)
    {
        document.getElementById("lstCalendar").style.display = 'none'  
    		    
    }
}
 //ALLDEV-644 Start
if(queryField("gen") != null && queryField("gen") == "1")
{

   if(document.getElementById("lstCalendar") != null)
    {
        document.getElementById("lstCalendar").style.display = 'none'  
    		    
    }
    if(document.getElementById("orgRow") != null)
    {
      document.getElementById("orgRow").style.display = ''  
    }

    if(document.getElementById("btnCompare") != null)
    {
      document.getElementById("btnCompare").style.display = 'none'  
    }
}
//ALLDEV-644 End
function WeekOverlap(weekBeginDate) {
		var overlaps = false;
		var week = new Date(weekBeginDate);
		var nextweeks =  new Date(weekBeginDate);
		var weekbegin = new Date();
		weekbegin.setDate(week.getDate() - week.getDay());
		nextweeks.setDate(weekbegin.getDate() + 7);
		var nextweek = new Date(nextweeks);
		if (nextweek.getMonth() != weekbegin.getMonth()) {
			overlaps = true;
		}
		var weeknew = getWeek(weekbegin);
		document.getElementById ("IsWeekChanged").value = "";
		(document.getElementById ("Weeknum").value != weeknew)
		{
		    document.getElementById ("IsWeekChanged").value = "Y";
		    
		}
		
		return overlaps;
		}
		
function getWeek (weekbegin)
 {
 var wkbegin = new Date(weekbegin);
 var onejan = new Date(wkbegin.getFullYear(),0,1);
 return Math.ceil((((wkbegin - onejan) / 86400000) + onejan.getDay()+1)/7);
 }
 


</script>

<script language="javascript" type="text/javascript">

 //************************** Treeview Parent-Child check behaviour ****************************//  

   function OnTreeClick(evt)
   {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if(isChkBoxClick)
        {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if(nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
        
        var btn = document.getElementById('btnDate');
        var hdn = document.getElementById('IsSettingsChange');
        
        if(hdn)
           hdn.value = "Y"; 
        
        if(btn)
            btn.click();
   } 

   function CheckUncheckChildren(childContainer, check)
   {
      var childChkBoxes = childContainer.getElementsByTagName("input");
      var childChkBoxCount = childChkBoxes.length;
      for(var i = 0; i<childChkBoxCount; i++)
      {
        childChkBoxes[i].checked = check;
      }
   }

   function CheckUncheckParents(srcChild, check)
   {
       var parentDiv = GetParentByTagName("div", srcChild);
       var parentNodeTable = parentDiv.previousSibling;
       
       if(parentNodeTable)
        {
            var checkUncheckSwitch;
            
            if(check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if(isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else    
                    return; //do not need to check parent if any child is not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }
            
            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if(inpElemsInParentTable.length > 0)
            {
                var parentNodeChkBox = inpElemsInParentTable[0]; 
                parentNodeChkBox.checked = checkUncheckSwitch; 
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
   }

   function AreAllSiblingsChecked(chkBox)
   {
     var parentDiv = GetParentByTagName("div", chkBox);
     var childCount = parentDiv.childNodes.length;
     for(var i=0; i<childCount; i++)
     {
        if(parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
        {
            if(parentDiv.childNodes[i].tagName.toLowerCase() =="table")
            {
               var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
              //if any of sibling nodes are not checked, return false
              if(!prevChkBox.checked) 
              {
                return false;
              } 
            }
        }
     }
     return true;
   }

   //utility function to get the container of an element by tagname
   function GetParentByTagName(parentTagName, childElementObj)
   {
      var parent = childElementObj.parentNode;
      while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())
      {
         parent = parent.parentNode;
      }
    return parent;    
   } 
   
   function chnghrs()
   {
   
   if (!fncheckTime())
            return false;
    //Latest Issue
    document.getElementById ("IsMonthChanged").value = "Y";
    document.getElementById ("btnDate").click(); 
//    var hourbtn = document.getElementById('btnbsndhrs');
//    if(hourbtn)
//        hourbtn.click();
   } 
   //ZD 100157
   function ShowHrs()
   {
       var ShwHrs= document.getElementById("officehrDaily");
       if(!ShwHrs.checked)
       {
           if(document.getElementById("lstStartHrs_Container")!= null)
            document.getElementById("lstStartHrs_Container").style.display="none";
           if(document.getElementById("lstEndHrs_Container")!= null)
            document.getElementById("lstEndHrs_Container").style.display="none";
           }
       else
       {
           if(document.getElementById("lstStartHrs_Container")!= null)
            document.getElementById("lstStartHrs_Container").style.display="inline";
           if(document.getElementById("lstEndHrs_Container")!= null)
            document.getElementById("lstEndHrs_Container").style.display="inline";
       
       } 
   }
   
   function OpenRoomSearchCalen()
   {
        if(OpenRoomSearchCalen.arguments != null)
        {
            var sellst=""; var boolval=false;
            if(document.getElementById('selectedList').value!="")
                 sellst = document.getElementById('selectedList').value;
            else
            {                 
                var data=document.getElementById('selectedListRoomName').value;
                var splitroom1=null; var locsname="";
                if(data.value!="")
                {
                    if(data.indexOf("�") > -1)
                    {
                            splitroom1=data.split('�'); //ALT 147
                            for(var i=0;i<splitroom1.length;i++)
                            if(sellst=="")
                            sellst=splitroom1[i].split('|')[0];
                            else
                            sellst+=","+splitroom1[i].split('|')[0];
                
                       
                    }
                    else
                        sellst=data.split('|')[0];
                }
                else
                    sellst="";
            }
                
            //var sellst = document.getElementById('selectedListRoomName');
            var rmargs = OpenRoomSearchCalen.arguments;
            var prnt = rmargs[0];
            var url = "";
           // var locs = document.getElementById("selectedloc");
           // sellst.value =  locs.value;
            url = "RoomSearch.aspx?rmsframe="+sellst+"&hf=1&frm="+prnt;//frmCalendarRoom";
            window.open(url, "RoomSearch", "width="+ screen. availWidth +",height=666px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
        }
    }

function comparesel()
{
	rms = document.getElementById("selectedList").value;
	rms = getListViewChecked(rms);
	rms = rms.replace(", ,",",");
    //rms += ",";
	if (rms.split(",").length <=2)
		alert(CompareCntAlert);
	else
	    if (rms.split(",").length > 6) //fogbugz case 179 kapil said it is 5
	        alert(selectRmCnt);
	else {
		url = "roomresourcecomparesel.aspx?wintype=pop&f=pop&rms=" + rms;
		rmresPopup = window.open(url,'roomresource','status=yes,width=500,height=280,resizable=yes,scrollbars=yes');
			
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}
/* //ZD 102782 - Start
//FB 2804
$(document).ready(function(){ 
   $('#selectAllCheckBox').live('click',function(){          
         fnselectAllCheckBox();   
     });

     function fnselectAllCheckBox()
     {
        var sList = new Array();
        $('#lstRoomSelection input:checked').each(function() {
            sList.push($(this).attr('name'));
        });       
        
        $('#selectedListViewRooms').val(sList.length); 
        var selRoom = sList.length;                   
        if(selRoom == 0)
             document.getElementById("tdSelectRooms").style.display='block';                 
        else
             document.getElementById("tdSelectRooms").style.display='none';                 
     }
     
     $('#rdSelView input').live('click',function(){             
        $('#hdnRoomType').val($('#rdSelView input').index(this));

        if(document.getElementById('selectedRooms').value != "0")        
            document.getElementById('selectedListViewRooms').value = document.getElementById('selectedRooms').value;

        if(document.getElementById('selectedListViewRooms').value != "0")        
            document.getElementById('selectedRooms').value = document.getElementById('selectedListViewRooms').value;
        
     });
     
     // Level View Rooms
     getSelectedRoomListCount();
             
    $('#treeRoomSelection').live('click',function(){           
       getSelectedRoomListCount();       
    });
    
    function getSelectedRoomListCount()
    {   
        var selectedList = new Array();
        $('#treeRoomSelection input:checked').each(function() {
            rmselected.push($(this).attr('name'));
        });
        $('#selectedRooms').val(rmselected.length);
    }    
    var selectedRooms = $('#selectedRooms').val();
    
    
    
     rmselected = new Array();
 $('#treeRoomSelection input:checked').each(function() { 
            rmselected.push($(this).attr('name'));
        });
        $('#selectedRooms').val(rmselected.length);
      var selectedRooms = $('#selectedRooms').val();      
    if(document.getElementById("selStatus").value != 0)//FB 2804                   
        if(selectedRooms == 0)
            document.getElementById("tdSelectRooms").style.display='block';                 
        else
            document.getElementById("tdSelectRooms").style.display='none';
      
 rmselected = new Array();
 
 
  //---------------------------------------------------
  
// List View Rooms

 getSelectedListViewRoomCount();
 
 $('#lstRoomSelection').live('click',function(){    
    getSelectedListViewRoomCount();       
 });
 
 function getSelectedListViewRoomCount()
 {
     var selectedListCount = new Array();
     $('#lstRoomSelection input:checked').each(function() {
         selectedListCount.push($(this).attr('name'));
     });
     $('#selectedListViewRooms').val(selectedListCount.length);        
 }
 var selectedListViewRooms = $('#selectedListViewRooms').val();
 
 
 selectedListArray = new Array();    
   $('#lstRoomSelection input:checked').each(function() {
            selectedListArray.push($(this).attr('name'));
        });
        $('#selectedListViewRooms').val(selectedListArray.length);
      var selectedListViewRooms = document.getElementById("selectedListViewRooms").value;                   

  //if(selectedListViewRooms == 0)  
  if(document.getElementById("selectedListRoomName").value=="")
  {
//   if(isClosed)  
//      document.getElementById("tdSelectRooms").style.display='none';
//   else
      document.getElementById("tdSelectRooms").style.display='block';         
   }   
 else
     document.getElementById("tdSelectRooms").style.display='none';
 
 selectedListArray = new Array();
 
//    //---------------------------------------------------

fnSelRoom();//ZD 102397
    
});
*/ //ZD 102782 - End

 var rmselected = new Array();

//ZD 102358
var invoke = false;
function initGetCalendar()
{
    invoke = true;
    DataLoading(1);//ZD 102560
    if(document.getElementById("selectedList").value == "")  
        document.getElementById("tdSelectRooms").style.display='block';
     else          
        document.getElementById("tdSelectRooms").style.display='none'; 
}

function getTabCalendarLocalNew()
{
    fnInitiateDiv('none');//ZD 104491
    if(invoke == true)
    {
        invoke = false;
        document.getElementById ("IsSettingsChange").value = "Y";
        document.getElementById ("IsMonthChanged").value = "";     

        Sys.WebForms.PageRequestManager.getInstance().abortPostBack();

        document.getElementById ("btnDate").click();
    }

     if(document.getElementById("selectedList").value == "")  
        document.getElementById("tdSelectRooms").style.display='block';
     else          
        document.getElementById("tdSelectRooms").style.display='none';     
}
//ZD 104491 - Start
function fnInitiateDiv(parVal)
{   
    
    var divObj = document.getElementById("transDiv");
    divObj.style.display = parVal;
}
//ZD 104491 - End
 //Code added for Tab Calendar pages in daypilot
function getTabCalendarLocal(evt)
{
// obj gives us the node on which check or uncheck operation has performed 
    Sys.WebForms.PageRequestManager.getInstance().abortPostBack(); // ZD 102458
    //ZD 100284 - Start
    var obj;
    if (window.event != window.undefined)
       obj =  window.event.srcElement;
    else
       obj = evt.target; 
    
    var treeNodeFound = false; 
    var checkedState;
    
    if(obj.tagName == "IMG")
    {
        document.getElementById("btnMiddleTier").click();
    }
    
    if (obj.tagName == "INPUT" && obj.type == "checkbox") 
    { 
        //1 checking whether obj consists of checkbox or not 
        var treeNode = obj; 
        //record the checked state of the TreeNode 
        checkedState = treeNode.checked; 
        //work our way back to the parent <table> element 
        do {
            obj = obj.parentNode; 
        } while (obj.tagName != "TABLE") 
        //keep track of the padding level for comparison with any children 
        var parentTreeLevel = obj.rows[0].cells.length; 
        var parentTreeNode = obj.rows[0].cells[0]; 
        //get all the TreeNodes inside the TreeView (the parent <div>) 
        var tables = obj.parentNode.getElementsByTagName("TABLE"); 
        //checking for any node is checked or unchecked during operation 
        if(obj.tagName == "TABLE") 
        { //2
            // if any node is unchecked then their parent node are unchecked 
            if (!treeNode.checked) 
            { //3
                //head1 gets the parent node of the unchecked node 
                //document.getElementById("tdSelectRooms").style.display='block';//FB 2802
                var head1 = obj.parentNode.previousSibling; 
                
                if(head1 != undefined && head1 != null)
                {
                    if(head1.tagName == "TABLE") 
                    {
                        //checks for the input tag which consists of checkbox 
                        var matchElement1 = head1.getElementsByTagName("INPUT"); 
                        //matchElement1[0] gives us the checkbox and it is unchecked 
                        if(matchElement1.length > 0)
                            matchElement1[0].checked = false; 
                    } 
                }
                else 
                    head1 = obj.parentNode.previousSibling;
                if(head1 != undefined && head1 != null)
                {
                    if(head1.tagName == "TABLE") 
                    { //4
                    //head2 gets the parent node of the unchecked node 
                        var head2 = obj.parentNode.parentNode.previousSibling; 
                        if(head2.tagName == "TABLE") 
                        { //5
                            //checks for the input tag which consists of checkbox 
                            var matchElement2 = head2.getElementsByTagName("INPUT"); 
                            if(matchElement2.length > 0)
                                matchElement2[0].checked = false; 
                        } //5
                    }//4
                }
                else 
                    head2 = obj.parentNode.previousSibling;
                if(head2 != undefined && head2 != null)
                {   
                    if(head2.tagName == "TABLE") 
                    {//6
                        //head3 gets the parent node of the unchecked node 
                        var head3 = obj.parentNode.parentNode.parentNode.previousSibling; 
                        if(head3.tagName == "TABLE") 
                        {//7
                            //checks for the input tag which consists of checkbox 
                            var matchElement3 = head3.getElementsByTagName("INPUT"); 
                            if(matchElement3.length > 0)
                                matchElement3[0].checked = false; 
                        }//7
                    }//6
                 }
                else 
                    head3 = obj.parentNode.previousSibling;
                if(head3 != undefined && head3 != null)
                { 
                    if(head3.tagName == "TABLE") 
                    { //7
                       //head4 gets the parent node of the unchecked node 
                        var head4 = obj.parentNode.parentNode.parentNode.parentNode.previousSibling; 
                        if(head4 != null) 
                        {//8 
                            if(head4.tagName == "TABLE") 
                            {//8
                                //checks for the input tag which consists of checkbox 
                                var matchElement4 = head4.getElementsByTagName("INPUT"); 
                                if(matchElement4.length > 0)
                                    matchElement4[0].checked = false; 
                            }//8
                        }//7
                    } //6
                 }
            } //end if - unchecked
            //total number of TreeNodes 
            var numTables = tables.length 
            if (numTables >= 1) 
            {
                for (i=0; i < numTables; i++) 
                {
                    if (tables[i] == obj) 
                    {
                        treeNodeFound = true; 
                        i++;
                        if (i == numTables) 
                            break;
                    }
                    if (treeNodeFound == true) 
                    {
                        var childTreeLevel = tables[i].rows[0].cells.length; 
                        if (childTreeLevel > parentTreeLevel) 
                        { 
                            var cell = tables[i].rows[0].cells[childTreeLevel - 1]; 
                            var inputs = cell.getElementsByTagName("INPUT"); 
                            if(inputs.length > 0)
                                inputs[0].checked = checkedState; 
                        }
                        else 
                            break;
                    } //end if 
                    if (childTreeLevel == "5")
                    {
                        if(inputs.length > 0)
                        {
                            if (inputs[0].checked)
                            {
                                var tnName = inputs[0].id; var nodeNum;
                                if (tnName.indexOf("Wizard1") >= 0)
                                   nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                                else
                                    nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
                                if (nodeNum != "")
                                {
                                    var obbj;
                                    //alert(tnName.indexOf("Wizard1"));
                                    if (tnName.indexOf("Wizard1") >= 0)
                                        obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                                    else
                                        obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                                    if (obbj.innerText != "")
                                      if (document.getElementById("selectedList").value.indexOf(" " + obbj.innerText + ", ") < 0)
                                        document.getElementById("selectedList").value += " " + obbj.innerText + ", ";

                                    
                                }   
                            }
                            else
                            {
                                var tnName = inputs[0].id;
                                var nodeNum;
                                if (tnName.indexOf("Wizard1") >= 0)
                                   nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                                else
                                    nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
                                if (nodeNum != "")
                                {
                                    var obbj;
                                
                                    if (tnName.indexOf("Wizard1") >= 0)
                                        obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                                    else
                                        obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                                    var obbbj = document.getElementById("selectedList");
                                    if ((obbbj.value.indexOf(" " + obbj.innerText + ", ") >=0) && (obbj.innerText != ""))
                                         obbbj.value = obbbj.value.replace(" " + obbj.innerText + ", ", "");
                                }
                            }
                        }
                    }
                }//end for 
                
                
            } //end if - numTables >= 1

        // if all child nodes are checked then their parent node is checked
        //Wizard1_treeRoomSelectiont4
        //Wizard1_treeRoomSelectionn4Checkbox
        if (treeNode.checked) 
        {
           //document.getElementById("tdSelectRooms").style.display='none';//FB 2802
            var tnName = treeNode.id;
            var nodeNum;
            if (tnName.indexOf("Wizard1") >= 0)
               nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
            else
                nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
            if (nodeNum != "")
            {
                var obbj;
                if (tnName.indexOf("Wizard1") >= 0)
                    obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                else
                    obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                    
                if (obbj == null)
                {
                    obbj = document.getElementById(nodeNum);
                    tle = obbj.parentNode.innerText;
                }
                else
                    tle = obbj.innerText;
                if (obbj != null)
                if (tle != "")
                  if (document.getElementById("selectedList").value.indexOf(" " + tle + ", ") < 0)
                    document.getElementById("selectedList").value += " " + tle + ", ";
            }
            var chk1 = true;
            var head1 = obj.parentNode.previousSibling;
            var pTreeLevel1 = obj.rows[0].cells.length;
            if(head1 != undefined && head1 != null)
            {
            if(head1.tagName == "TABLE") 
            {
                var tbls = obj.parentNode.getElementsByTagName("TABLE");
                var tblsCount = tbls.length;
                for (i=0; i < tblsCount; i++) 
                {
                    var childTreeLevel = tbls[i].rows[0].cells.length;
                    if (childTreeLevel = pTreeLevel1) 
                    {
                        var chld = tbls[i].getElementsByTagName("INPUT");

                        if (chld.length > 0 && chld[0].checked == false) 
                        {
                            chk1 = false;
                            break;
                        }
                    }
                }
                var nd = head1.getElementsByTagName("INPUT");
                if(nd.length > 0)
                    nd[0].checked = chk1;
            }
            }
            else
                head1 = obj.parentNode.previousSibling;
            var chk2 = true;
            if(head1 != undefined && head1 != null)
            {
            if(head1.tagName == "TABLE") 
            {
                var head2 = obj.parentNode.parentNode.previousSibling; 
                if(head2 != undefined && head2 != null)
                {
                if(head2.tagName == "TABLE") 
                {
                    var tbls = head1.parentNode.getElementsByTagName("TABLE");
                    var pTreeLevel2 = head1.rows[0].cells.length;
                    var tblsCount = tbls.length;
                    for (i=0; i < tblsCount; i++) 
                    {
                        var childTreeLevel = tbls[i].rows[0].cells.length;
                        if (childTreeLevel = pTreeLevel2) 
                        {
                            var chld = tbls[i].getElementsByTagName("INPUT");
                            if (chld.length > 0 && chld[0].checked == false) 
                            {
                                chk2 = false;
                                break;
                            }
                        }
                    }
                    var nd = head2.getElementsByTagName("INPUT");
                    if(nd.length > 0)
                        nd[0].checked = (chk2 && chk1);
                }
                }
            }
            }
            else 
                head2 = obj.parentNode.previousSibling;
            var chk3 = true;
            if(head2 != undefined && head2 != null)
            {
            if(head2.tagName == "TABLE") 
            {
                var head3 = obj.parentNode.parentNode.parentNode.previousSibling; 
                if(head3 != undefined && head3 != null)
                {
                if(head3.tagName == "TABLE") 
                {
                    var tbls = head2.parentNode.getElementsByTagName("TABLE");
                    var pTreeLevel3 = head2.rows[0].cells.length;
                    var tblsCount = tbls.length;
                    for (i=0; i < tblsCount; i++) 
                    {
                        var childTreeLevel = tbls[i].rows[0].cells.length;
                        if (childTreeLevel = pTreeLevel3) 
                        {
                            var chld = tbls[i].getElementsByTagName("INPUT");                            
                            if (chld.length > 0 && chld[0].checked == false) 
                            {
                                chk3 = false;
                                break;
                            }
                        }
                    }
                    var nd = head3.getElementsByTagName("INPUT");
                    if(nd.length > 0)
                        nd[0].checked = (chk3 && chk2 && chk1);
                }
                }
            }
            }
            else 
                head3 = obj.parentNode.previousSibling;
            var chk4 = true;
            if(head3 != undefined && head3 != null)
            {
            if(head3.tagName == "TABLE") 
            {
                var head4 = obj.parentNode.parentNode.parentNode.parentNode.previousSibling; 
                //Added for FB 1415,1416,1417,1418 Start
                if(head4 != null && head4 != undefined)
                {
                //Added for FB 1415,1416,1417,1418 End
                    if(head4.tagName == "TABLE") 
                    {
                        var tbls = head3.parentNode.getElementsByTagName("TABLE");
                        var pTreeLevel4 = head3.rows[0].cells.length;
                        var tblsCount = tbls.length;
                        for (i=0; i < tblsCount; i++) 
                        {
                            var childTreeLevel = tbls[i].rows[0].cells.length;
                            if (childTreeLevel = pTreeLevel4) 
                            {
                                var chld = tbls[i].getElementsByTagName("INPUT");
                                if (chld.length > 0 && chld[0].checked == false) 
                                {
                                    chk4 = false;
                                    break;
                                }
                            }
                        }
                        var nd = head4.getElementsByTagName("INPUT");
                        if(nd.length > 0)
                            nd[0].checked = (chk4 && chk3 && chk2 && chk1);
                        //alert(chk1.value + " : " + chk2.value + " : " + chk3.value + " : " + chk4.value);
                    }
                }
                }  //Added for FB 1415,1416,1417,1418 
            }
        }//end if - checked
        
        document.getElementById ("IsSettingsChange").value = "Y";
        document.getElementById ("IsMonthChanged").value = "";     
        document.getElementById ("btnDate").click();
    }
    //ZD 100284 - End
  
 } //end if
 //FB 2804

 rmselected = new Array();
 $('#treeRoomSelection input:checked').each(function() {
            rmselected.push($(this).attr('name'));
        });
        $('#selectedRooms').val(rmselected.length);
      var selectedRooms = $('#selectedRooms').val();                 
      
        if((selectedRooms == 0 && (document.getElementById("hdnRoomView").value == 2 || document.getElementById("hdnRoomView").value == 3)) || (document.getElementById("selStatus").value != 0 && (document.getElementById("hdnRoomView").value !=1 || document.getElementById("hdnRoomView").value ==1) && selectedRooms == 0 ))//FB 2948
            document.getElementById("tdSelectRooms").style.display='block';                 
        else
            document.getElementById("tdSelectRooms").style.display='none';
       
} //end function
 
 //FB 2598 Starts
    //ZD 101388 Starts Commeted
    /*function removeOption()
    {
        if("<%=Session["EnableCallmonitor"]%>" == "0")
        {
        
        var x=document.getElementById("lstCalendar");
        x.remove(4);
        x.remove(3);
        }
       
    }*/

 //removeOption();
 //ZD 101388 End Commeted
//FB 2598 Ends
//ZD 100157 Start
if(document.getElementById("reglstStartHrs") != null)
  document.getElementById("reglstStartHrs").controltovalidate = "lstStartHrs_Text"; 
    if (document.getElementById("reqlstStartHrs") != null)
        document.getElementById("reqlstStartHrs").controltovalidate = "lstStartHrs_Text";
    if (document.getElementById("reqlstEndHrs") != null)
        document.getElementById("reqlstEndHrs").controltovalidate = "lstEndHrs_Text"; 
    if (document.getElementById("reglstEndHrs") != null)
        document.getElementById("reglstEndHrs").controltovalidate = "lstEndHrs_Text";
    /*
    if(document.getElementById("lstStartHrs_Text") != null)
        document.getElementById("lstStartHrs_Text").setAttribute("onchange","javascript:__doPostBack('btnbsndhrs', '');");
    if(document.getElementById("lstEndHrs_Text") != null)
        document.getElementById("lstEndHrs_Text").setAttribute("onchange","javascript:__doPostBack('btnbsndhrs', '');");
    */
    
  function fnDetectChange()
  {

    var hdn1 = document.getElementById("hdnlstStartHrs");
    var hdn2 = document.getElementById("hdnlstEndHrs");
    var obj1 = document.getElementById("lstStartHrs_Text");
    var obj2 = document.getElementById("lstEndHrs_Text");
    
    if(hdn1.value != obj1.value || hdn2.value != obj2.value)
    {
          hdn1.value = obj1.value;
          hdn2.value = obj2.value;
          //__doPostBack('btnbsndhrs', '');
          chnghrs();
    }
    setTimeout("fnDetectChange()",250);
    
  }
  
  function fnStartDetectChange()
  {
    document.getElementById("hdnlstStartHrs").value = document.getElementById("lstStartHrs_Text").value;
    document.getElementById("hdnlstEndHrs").value = document.getElementById("lstEndHrs_Text").value;
    if(document.getElementById("officehrDaily").checked == true)
        fnDetectChange();
  }
  setTimeout("fnStartDetectChange()",1000);
  
  function fncheckTime() {
        var stdate = '';
         var stime = document.getElementById("lstStartHrs_Text").value;
        var etime = document.getElementById("lstEndHrs_Text").value;
          if('<%=Session["timeFormat"]%>' == "2") 
        {
             stime = stime.replace('Z', '')
            stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);
            
            etime = etime.replace('Z', '')
            etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);
        }
        if (document.getElementById("lstEndHrs_Text") && document.getElementById("lstStartHrs_Text")) {
            stdate = GetDefaultDate('01/01/1901', '<%=((Session["timeFormat"] == null) ? "1" : Session["timeFormat"])%>');
            
            if (Date.parse(stdate + " " + etime) < Date.parse(stdate + " " + stime)) {
                alert(EndTimeValidation); //FB 2148
                document.getElementById("lstStartHrs_Text").focus();
                return false;
            }
            else if (Date.parse(stdate + " " + etime) == Date.parse(stdate + " " + stime)) {
                alert(EndTimeValidation);
                document.getElementById("lstEndHrs_Text").focus();
                return false;
            }
        }
        return true;
    }
    
setTimeout("document.getElementById('officehrDaily').style.marginLeft = '1px'",100);

function fnTimeScroll()
{
fnHidelbl();

var StartHr;
var n = 0;
var d = new Date();
var t = d.getHours();
if(document.getElementById("lstStartHrs_Text") !=null)
    StartHr = document.getElementById("lstStartHrs_Text").value.toLowerCase();
else
   StartHr = "12:00 am";//ALLDEV-644
    
if(StartHr.indexOf("z") > -1)
    n = parseInt(StartHr.substring(0,2),10);
    
else if(StartHr.indexOf("am") > -1 || StartHr.indexOf("pm") > -1)
{
    if(StartHr.indexOf("12") > -1 && StartHr.indexOf("am") > -1)
        n = 0;
    else
        n = parseInt(StartHr.substring(0,2),10);
    
    if(StartHr.indexOf("pm") > -1)
        n += 12;
}
else
{
    n = parseInt(StartHr.substring(0,2),10);
}

var pos;

if(document.getElementById("officehrDaily") != null  && document.getElementById("officehrDaily").checked == true) ////ALLDEV-644
    pos = (t * 200) - (n * 200);
else
    pos = t * 200;

pos -= 400;
var dv = document.getElementById("CalendarContainer_schDaypilot").childNodes[0].childNodes[0].childNodes[1].childNodes[1].childNodes[0];
dv.scrollLeft=pos;

//alert("n: " + n + ", pos:" + pos);

if(document.getElementById("selectedList").value == "")  
    document.getElementById("tdSelectRooms").style.display='block';
else          
    document.getElementById("tdSelectRooms").style.display='none'; 
}
ShowHrs();
//ZD 100157 Ends

//ZD 100284 - Start
    if (document.getElementById("lstStartHrs_Text")) {
        var confstarttime_text = document.getElementById("lstStartHrs_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('lstStartHrs_Text', 'reglstStartHrs',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("lstEndHrs_Text")) {
        var confstarttime_text = document.getElementById("lstEndHrs_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('lstEndHrs_Text', 'reglstEndHrs',"<%=Session["timeFormat"]%>")
        };
    }

 </script>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<script language="javascript" type="text/javascript">
//  ALLDEV-644 start
    document.onkeydown = function (evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    //  ALLDEV-644 End

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
        if(document.getElementById("close") != null)
            document.getElementById("close").click();
        if (document.getElementById("btnCancel") != null)
            document.getElementById("btnCancel").click();
        if (document.getElementById("btnEditCancel") != null) //ZD 101942 
            document.getElementById("btnEditCancel").click();  
        }
    }
    
    //ZD 100393 start
    function refreshStyle() {
        var i, a, s;
        a = document.getElementsByTagName('link');
        for (i = 0; i < a.length; i++) {
            s = a[i];
            if (s.rel.toLowerCase().indexOf('stylesheet') >= 0 && s.href) {
                var h = s.href.replace(/(&|\\?)forceReload=d /, '');
                s.href = h + (h.indexOf('?') >= 0 ? '&' : '?') + 'forceReload=' + (new Date().valueOf());
            }
        }
    }
    if (navigator.userAgent.indexOf('Safari') > -1)
        refreshStyle();
    //ZD 100393 End

    //ZD 102008
    function fnSetModalDiv() {
        var w = screen.width;
        var h = screen.height;
        document.getElementById("ModalDiv").style.width = w + 'px';
        document.getElementById("ModalDiv").style.height = h + 'px';        
    }
    fnSetModalDiv();

    function fnHidelbl() {
        if ('<%=Session["hasConference"]%>' == '0' && '<%=Session["hasExpConference"]%>' == "0") {
            if (document.getElementById('CalendarContainer_lblRoomCal') != null)
                document.getElementById('CalendarContainer_lblRoomCal').style.display = 'none';
        }
    }
    fnHidelbl();

    //ZD 102397 - Start
    function fnSelRoom() 
    {
        if (window.opener != null) 
        {
            if (window.opener.location.href.toLowerCase().indexOf('conferencesetup.aspx') > -1) 
            {
                if (document.getElementById('selectedList') != null)

                    if (document.getElementById('selectedList').value != "")
                        document.getElementById("tdSelectRooms").style.display = 'none';
                    else
                        document.getElementById("tdSelectRooms").style.display = 'block';

                if (document.getElementById("btnfrmSearch"))
                    document.getElementById('btnfrmSearch').click();

                if (document.getElementById("selStatus") != null) 
                {
                    var SelectedWeekdays, Selectedday, str = "", selectedRooms1;
                    var isClosed = new Boolean();
                    var selDays = new Array();
                    var confdate = new Date();

                    isClosed = false;
                    confdate = (confdate.getMonth() + 1) + "/" + confdate.getDate() + "/" + confdate.getFullYear();
                    SelectedWeekdays = new Date(confdate);
                    Selectedday = SelectedWeekdays.getDay() + 1

                    if ('<%=Session["DaysClosed"]%>' != null) 
                    {
                        str = '<%=Session["DaysClosed"].ToString()%>';

                        selDays = str.split(',');

                        for (var i = 0; i < selDays.length; i++) 
                        {
                            if (selDays[i] == Selectedday)
                                isClosed = true;
                        }
                        if (isClosed) 
                        {
                            if (opener.document.getElementById("selStatus") != null)
                                opener.document.getElementById("selStatus").value = "0";
                        }
                        else 
                        {
                            if (opener.document.getElementById("selStatus") != null)
                                opener.document.getElementById("selStatus").value = "1";
                        }
                    }
                }
            }
        }
    }
    //ZD 102397 - End

    //ZD 102358
    function ShowHideRow() {

        if (document.getElementById('selectedList') != null) {
            if (document.getElementById('selectedList').value != "")
                document.getElementById("tdSelectRooms").style.display = 'none';

            else
                document.getElementById("tdSelectRooms").style.display = 'block';
        }
        //setTimeout("datechg()", 100); //ZD 102560 //Latest Issue 
    }


    //ALLDEV-644 start
    if (document.getElementById('RoomPoupup') != null && document.getElementById('btnCompare') != null)
        document.getElementById('RoomPoupup').setAttribute("onblur", "document.getElementById('btnCompare').focus(); document.getElementById('btnCompare').setAttribute('onfocus', '');");
   //ALLDEV-644 End
    
</script>
<%--ZD 100428 END--%>


<% if (Request.QueryString["hf"] != null)
   {
       if(Request.QueryString["hf"] != "1")
       {

        if(Request.QueryString["gen"] == null || Request.QueryString["gen"] != "1")
        {
%>
        <div class="btprint">        
        <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
        <%--ZD 101388 Commented--%>
        <%--<!-- FB 2719 Starts -->
        <% if (Session["isExpressUser"].ToString() == "1"){%>
        <!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
        <%}else{%>
        <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
        <%}%>
        <!-- FB 2719 Ends -->--%>
        </div>
<%     } 
       }
   
   }
%>