/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9216.3.0  Starts(1st April 2016)         */
/*                                                                                              */
/* ******************************************************************************************** */


/* **********************ALLDEV-498 - 1st April 2016 Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	ConfAdministrator varchar(MAX) NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


update Org_Settings_D set ConfAdministrator = ''


/* **********************ALLDEV-498 -1st April 2016 2015 Ends************ */


/* **********************ALLDEV-839 - 4th April 2016 Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	AllowRequestortoEdit smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


update Org_Settings_D set AllowRequestortoEdit = 0


/* **********************ALLDEV-839 - 4th April 2016 Ends************ */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9216.3.0  Ends  (12th April 2016)         */
/*                              Features & Bugs for V2.9216.3.1  Starts(12th April 2016)         */
/*                                                                                              */
/* ******************************************************************************************** */

/* **********************ALLDEV-807 - 13th April 2016 Starts************ */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Loc_Assistant_D
	(
	ID int NOT NULL IDENTITY (1, 1),
	RoomId int NOT NULL,
	AssistantId int NOT NULL,
	EmailId nvarchar(250) NULL,
	AssitantName nvarchar(250) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Loc_Assistant_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Insert into Loc_Assistant_D (RoomId,AssistantId,EmailId,AssitantName) 
select RoomID,Assistant,GuestContactEmail,GuestContactName from Loc_Room_D

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	isBusy int NULL
GO
ALTER TABLE dbo.Loc_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Loc_Room_D set isBusy =0 


IF EXISTS (
SELECT * 
FROM sys.indexes 
WHERE name='IX_Usr_List_D_Email' AND object_id = OBJECT_ID('Usr_List_D'))
Begin
DROP INDEX [IX_Usr_List_D_Email] ON Usr_List_D;
ENd

ALTER TABLE Usr_Inactive_D ALTER COLUMN Email  nvarchar(255)  NULL
ALTER TABLE Usr_List_D ALTER COLUMN Email  nvarchar(255)  NULL
ALTER TABLE Usr_GuestList_D ALTER COLUMN Email  nvarchar(255)  NULL




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Archive_Conf_Conference_D ADD
	isHDBusy smallint NULL
GO
ALTER TABLE dbo.Archive_Conf_Conference_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Archive_Conf_Conference_D set isHDBusy = 0

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isHDBusy int NULL
GO
ALTER TABLE dbo.Conf_Conference_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Conf_Conference_D set isHDBusy = 0

--Update  Icons_Ref_S set IconTarget ='ConferenceList.aspx?t=10' where IconID = 4 --Public Conference


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Loc_Room_D ADD
	isBusy int NULL
GO
ALTER TABLE dbo.Audit_Loc_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Audit_Loc_Room_D set isBusy = 0



CREATE TABLE [dbo].[Audit_Loc_Assistant_D](
	ID int NOT NULL IDENTITY (1, 1),
	RoomId int NOT NULL,
	AssistantId int NOT NULL,
	EmailId nvarchar(250) NULL,
	AssitantName nvarchar(250) NULL,
	[LastModifiedDate] [datetime] NULL
	)  ON [PRIMARY]

Insert into [Audit_Loc_Assistant_D] ([RoomId], AssistantId, EmailId,AssitantName,[LastModifiedDate])
( select [RoomId], AssistantId,EmailId, AssitantName,GETUTCDATE() from Loc_Assistant_D)



insert into icons_ref_s values('53','../en/img/PublicConferenceIcon.png','Manage My Hotdesk(s)','ConferenceList.aspx?t=10')

/* **********************ALLDEV-807 - 13th April 2016 Ends************ */


/* **********************ALLDEV-854 - 19th April 2016  Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MCU_List_D ADD
	EnableAlias smallint NULL,
	Alias2 nvarchar(MAX) NULL,
	Alias3 nvarchar(MAX) NULL
GO
COMMIT


update MCU_List_D set EnableAlias = 0, Alias2='', Alias3='' 

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Mcu_List_D ADD
	EnableAlias smallint NULL,
	Alias2 nvarchar(MAX) NULL,
	Alias3 nvarchar(MAX) NULL
GO
COMMIT


update Audit_Mcu_List_D set EnableAlias = 0, Alias2='', Alias3=''
 

/* **********************ALLDEV-854 - 19th April 2016 Ends************ */
/* **********************ALLDEV-814 - 21th April 2016 Starts************ */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	audioaddonUID int NULL,
	profileID int NULL,
	ConferenceCode nvarchar(50) NULL,
	LeaderPin nvarchar(50) NULL
GO
ALTER TABLE dbo.Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Conf_User_D set profileID = 1

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Conf_User_D ADD
	audioaddonUID int NULL,
	profileID int NULL,
	ConferenceCode nvarchar(50) NULL,
	LeaderPin nvarchar(50) NULL
GO
ALTER TABLE dbo.Audit_Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Archive_Conf_User_D ADD
	audioaddonUID int NULL,
	profileID int NULL,
	ConferenceCode nvarchar(50) NULL,
	LeaderPin nvarchar(50) NULL
GO
ALTER TABLE dbo.Archive_Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Archive_Conf_User_D set profileID = 1


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	IsUserAudio int NULL,
	ParticipantCode nvarchar(50) NULL
GO
ALTER TABLE dbo.Ept_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


update Ept_List_D set IsUserAudio = 0, profileId = 1  where endpointId in (select endpointId from Usr_List_D where Deleted = 0)

update Ept_List_D set ParticipantCode =  (select ParticipantCode from  Usr_List_D b where b.Audioaddon = 1  and b.deleted = 0 and Ept_List_D.endpointId = b.endpointId)

update Conf_User_D set audioaddonUID = e.uId, profileID = e.profileId, ConferenceCode = e.ConferenceCode, LeaderPin=e.LeaderPin
from Ept_List_D e where endpointId in (select endpointId from Usr_List_D u where u.UserID = Conf_User_D.UserID and u.Audioaddon = 1)
/* **********************ALLDEV-814 - 21th April 2016 Ends************ */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9216.3.2  Ends  (29th April 2016)        */
/*                              Features & Bugs for V2.9216.3.3  Starts(2nd May 2016)           */
/*                                                                                              */
/* ******************************************************************************************** */

--/* ********************** ALLDEV-846 - 4th May 2016 Starts************ */

----3 Sydney
--update gen_timezone_s set 
--CurDSTstart8 = '2016-04-03 03:00:00.000', CurDSTend8 = '2016-10-02 02:00:00.000',
--CurDSTstart9 = '2017-04-02 03:00:00.000', CurDSTend9 = '2017-10-01 02:00:00.000'
--where TimeZoneID in (63,8,13)

----2 Brasilia
--update gen_timezone_s set 
--CurDSTstart8 = '2016-02-21 00:00:00.000', CurDSTend8 = '2016-10-16 00:00:00.000',
--CurDSTstart9 = '2017-02-19 00:00:00.000', CurDSTend9 = '2017-10-15 00:00:00.000'
--where TimeZoneID in (80,25)


----NO DST changes (12) 
--update gen_timezone_s set 
--CurDSTstart8 = '1980-01-01 00:00:00.000', CurDSTend8 = '1980-01-01 00:00:00.000',
--CurDSTstart9 = '1980-01-01 00:00:00.000', CurDSTend9 = '1980-01-01 00:00:00.000'
--where TimeZoneID in (88)

----1 Auckland
--update gen_timezone_s set 
--CurDSTstart8 = '2016-04-03 03:00:00.000', CurDSTend8 = '2016-09-25 02:00:00.000',
--CurDSTstart9 = '2017-04-02 03:00:00.000', CurDSTend9 = '2017-09-24 02:00:00.000'
--where TimeZoneID in (46)

----1 Santiago
--update gen_timezone_s set 
--CurDSTstart8 = '2016-05-15 00:00:00.000', CurDSTend8 = '2016-08-14 00:00:00.000',
--CurDSTstart9 = '2017-05-14 00:00:00.000', CurDSTend9 = '2017-08-13 00:00:00.000'
--where TimeZoneID in (50)

---- 2 Guadalajara, Mexico City, Monterrey
--update gen_timezone_s set 
--CurDSTstart8 = '2016-04-03 02:00:00.000', CurDSTend8 = '2016-10-30 02:00:00.000',
--CurDSTstart9 = '2017-04-02 02:00:00.000', CurDSTend9 = '2017-10-29 02:00:00.000'
--where TimeZoneID in (81,89)

----1 Asuncion
--update gen_timezone_s set 
--CurDSTstart8 = '2016-04-03 02:00:00.000', CurDSTend8 = '2016-09-04 02:00:00.000',
--CurDSTstart9 = '2017-04-02 02:00:00.000', CurDSTend9 = '2017-09-03 02:00:00.000'
--where TimeZoneID in (90)

----1 Damascus
--update gen_timezone_s set 
--CurDSTstart8 = '2016-04-27 00:00:00.000', CurDSTend8 = '2016-10-02 00:00:00.000',
--CurDSTstart9 = '2017-04-26 00:00:00.000', CurDSTend9 = '2017-10-01 00:00:00.000'
--where TimeZoneID in (93)

-----------------------------------------------------------------------------------


--Declare @tzone as int, @confid as int, @instanceid as int, @chDate as datetime
--Declare @chTime as datetime, @setupTime as datetime, @tearTime as datetime

--DECLARE getDSTCursor CURSOR for

--select a.timezone, a.confid, a.instanceid
--, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.confdate)) as changeddate
--, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.conftime)) as changedtime
--, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.SetupTime)) as changedSetupTime
--, dbo.changeToGMTTime(a.timezone,dateadd(minute, -t.Bias, a.TearDownTime)) as changedTearDownTime
--from Conf_Conference_D a , gen_timezone_s t where a.timezone = t.timezoneid and t.DST = 1 and t.TimeZoneID in (8,13,25,46,50,63,80,81,88,90,93)
--and status in (0,1)and confdate >= '2016-04-01 00:00:00.000'
----and settingtime < '2017-12-12 00:00:00.000'
--order by a.confid , a.instanceid
--OPEN getDSTCursor

--FETCH NEXT FROM getDSTCursor INTO  @tzone, @confid, @instanceid, @chDate, @chTime, @setupTime, @tearTime
--WHILE @@FETCH_STATUS = 0
--BEGIN

--update Conf_Conference_D set confdate = @chDate, conftime = @chTime, SetupTime = @setupTime, TearDownTime = @tearTime
--where confid = @confid  and instanceid = @instanceid

--update Conf_Room_D set StartDate = @chDate where ConfID = @confid and instanceID = @instanceid

--update Conf_User_D set ConfStartDate = @chDate where ConfID = @confid and instanceID = @instanceid

--if(@instanceid = 1)
--Begin
--	update Conf_RecurInfo_D set startTime = @chDate where confid = @confid 
--End

--FETCH NEXT FROM getDSTCursor INTO @tzone, @confid, @instanceid, @chDate, @chTime, @setupTime, @tearTime
--END
--CLOSE getDSTCursor
--DEALLOCATE getDSTCursor

--/* ********************** ALLDEV-846 - 4th May 2016 Ends************** */