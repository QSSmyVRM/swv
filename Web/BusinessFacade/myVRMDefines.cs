//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Collections;

using System.Runtime.InteropServices;

namespace ASPIL
{
    /// <summary>
    /// Summary description for myVRMDefines.
    /// </summary>
    public class myVRMDefines
    {
        internal static int CommandOffset = 1100;
        internal enum CommandTypes
        {
            ACCOUNTING = 100, CONFERENCE = 200, GENERAL = 300, WORKORDER = 400,
            HARDWARE = 500, MISC = 600, REPORTS = 700, SEARCH = 800,
            USER = 900, SYSTEM = 1000, ORGANIZATION = 1100, RoomFactory = 1200,IMAGEFACTORY = 1300 //FB 2027 //FB 2136
        };

        internal enum GeneralCommands
        {
            GetTimezones = 1, GetDialingOptions, Feedback, UpdateManageDepartment, GetCountryCodes, GetCountryStates,
            GetPublicCountries, GetPublicCountryStates, GetPublicCountryStateCities, //FB 2671
            GetManageDepartment, SetCustomAttribute, DeleteCustomAttribute, GetCustomAttributes, GetCustomAttributeDescription, DeleteAllData, GetSQLServerInfo,
            GetCustomAttributeByID, IsCustomAttrLinkedToConf, GetConfListByCustOptID, DeleteConfsAttributeByID,GetAllTimezones,
            GetAllImages, SetImage, DeleteImage, GetLanguages, GetEmailTypes, GetEmailContent, SetEmailContent, GetEmailLanguages, GetEmailsBlockStatus, SetBlockEmail, DeleteEmails,
            DeleteEmailLanguage , GetIconsReference, SetIconReference //FB 1830 - DeleteEmailLang // New Lobby
            /*FB 1830, FB 1860*/
            , GetFeedback, SaveFiles, SetEntityCode, UpdateEntityCode, GetEntityCode, DeleteEntityCode, GetConfListByEntityOptID, EditConfEntityOption, DeleteConfEntityOptionID //FB 2027 & FB 2153 //FB 2045
            , HelpRequestMail, GetAllDepartments, GetSecurityBadgeType //FB 2268 FB 2047 //FB 22366
            , GetPCDetails, GetGMTTime //FB 2693 //ZD 101120
        }
        //Diagonostics Module
        internal enum SystemCommands
        {
            GetLicenseDetails = 1, GetActivation, GetLDAPUsers, SetImagekey, GetSiteImage, DeleteSiteImage, GetLastMailRunDate, GetSysMailData, SetSuperAdmin,
            SearchLog, GetSystemDateTime, GetSuperAdmin, GenerateESUserReport, GenerateESErrorReport, DeltePublicRoomEP, KeepAlive, SetDefaultLicense, GetDefaultLicense, SyncWithLdap, SendMailtoAdmin, SendPasswordRequest, GetOrgLDAP //FB 2027 //FB 2363 //FB 2594 //FB 2616//FB 2659 //FB 2462 //ZD 100230 //ZD 100781 //ZD 101525 //ZD 102045
        }
        internal enum ConferenceCommands
        {
            ConfirmConferenceInvitation = 1, GetAdvancedAVSettings, GetConferenceEndpoint, GetOldConference,
            SetAdvancedAVSettings, SetConference, SetConferenceEndpoint, SetTemplateOrder, DeletePastConference, CheckApprovalEntity,SendPartyReminder, //FB 1830 
            DeleteCiscoICAL, GetEndpointStatus, CreateCiscoICALOnApproval, DeleteParticipantICAL, CreateParticipantICALOnApproval,UpdateICalID,//FB 1782 //Cisco ICAL FB 1602
            DeleteRecurInstance, DelRecurInstanceByUID, EditRecurInstance, EditRecurInstanceByUID, DeleteTerminal, GetTemplateList, PartyInvitation, TerminateConference, DeleteTemplate, CounterInvite, DisplayTerminal, MuteTerminal,//FB 1772 //FB 2027
            DeleteConference, GetNewTemplate, GetNewConference, GetApprovalStatus, GetTerminalControl, GetOldTemplate, GetConfGMTInfo, SetDynamicUser, SetTerminalControl,GetAvailableRoom,GetApproveConference, //FB 2027
            GetTemplate, ResponseInvite, GetInstances, SetTemplate, SetApproveConference //FB 2027
            , GetRecurDateList, GetRoombyMediaService, GetConfMCUDetails, GetConfMsg, GetPacketLossTerminal, LockTerminal, MonitorMuteTerminal, SetP2PCallerEndpoint, SetP2PCallerLinerate //FB 2027 SetConference FB 2038 FB 2448 //FB 2486 //FB 2501 Call Monitoring //FB 2501 p2p Call Monitoring
            , GetConfAvailableRoom, MuteUnMuteParticipants, ConferenceRecording, SetLeaderPerson, SetLectureParty, TDGetAvailableSeats, TDGetMaximumAvailableSeats, SendSynchronousBridgeemails, TDGetSlotAvailableSeats, TDGetDefaultSettings, SetConferenceSignin, ChangeConfMCU, CheckConferenceApprovalStatus, ForceConfDelete, SetInstantConferenceDetails, CheckForExtendTimeConflicts //FB 2392 //Fb 2441 //FB 2553-RMX //FB 2724 //ZD 100369:MCU FailOver //ZD 100642 //ZD 100221 //ZD 100167 //ZD 100819 //ZD 102195
            , CheckDialNumber, SetConferenceFromDateImport, SetConferenceDetails, SendBJNBridgeemails, InsertConfAuditDetails, SetHDBusyConferences, ScheduleHDBusyConferences //ZD 101657 //ZD 101879 //ZD 102195 //ZD 103263 //ZD 102754 //ALLDEV-807
        }
        internal enum WorkOrderCommands
        {
            GetInventoryDetails = 1, GetItemsList, GetInventoryList,
            DeleteInventory, GetRoomSets, DeleteWorkOrder,
            GetWorkOrderDetails, SetCategoryItem, DeleteCategoryItem, SetConferenceWorkOrders,
            SetInventoryDetails, SendWorkOrderReminder, SearchConferenceWorkOrders, GetDeliveryTypes,
            GetProviderDetails, SetProviderDetails, GetProviderWorkorderDetails, SetProviderWorkorderDetails,
            GetCateringServices, SearchProviderMenus,DeleteProviderMenu
        };
        internal enum UserCommands
        {
            GetMultipleUsers = 1, SetMultipleUsers, DeleteLDAPUser,
            GetUserTemplateList, GetUserTemplateDetails, SetUserTemplate,
            DeleteUserTemplate, GetUserDetails, SetUserDetails, GetUserList, ConvertToGMT, ConvertFromGMT,
            ChangeUserStatus, ChangeGuestStatus, GetManageUser, GetOldUser, GetAllManageUser//Code added for FB 826
            , SetBulkUserUpdate, ChkUsrAuthentication, BlockAllUsers, UnBlockAllUsers, CheckUserCredentials, GetAudioUserList, //FB 1642
            RequestPassword, RequestVRMAccount, GetUserEmails, SetUserEmailsBlockStatus, ChangePasswordRequest,SetExpirePassword,//ZD 103954
            GetRequestIDUser //FB 1830 ,FB 1860 //FB 2027 , GetUserEmailsBlockStatus //ZD 100263
            , SetPreferedRoom, GetPreferedRoom, SetUserRoles, SearchUserOrGuest, RetrieveUsers, RetrieveGuest, GetManageGuest, GetUsers,SetOldUser,GetUserPreference //FB 1959, FB 2027
            , DeleteAllGuests, GetAllocation, SetGroup, GetGroup, SearchGroup, SetUserStatus, GuestRegister, GetUserRoles, DeleteGroup, GetEmailList, GetGuestList, GetManageBulkUsers, GetSettingsSelect //FB 2027
            , SetBulkUserAddMinutes, SetBulkUserBridge, SetBulkUserDelete, SetBulkUserDepartment, SetBulkUserExpiryDate, SetBulkUserLanguage, SetBulkUserLock, SetBulkUserRole, SetBulkUserTimeZone, SetUser, GetHome, GetLanguageTexts, SetUserLobbyIcons, SetBridgenumbers  //FB 1959 //FB 1830 - Translation //New Lobby  //FB 2027 FB 2227
            , SetPhoneNumber, UserAuthentication, GetVNOCUserList, FetchSelectedPCDetails, SetCalendarTimes, GetCalendarTimes, PasswordChangeRequest, SetUserToken, GetGoogleChannelExpiredList, GetGoogleChannelDetails, GetUserRoomViewType, SetUserRoomViewType //ZD 100621 //FB 2268 //FB 2558 WhyGo //FB 2670 //FB 2693 //ZD 100157 //ZD 100781 //ZD 100152
            , GetUserType, SearchUserInfo, SetAssignedUserAdmin, ChkADAuthentication, SetUsrBlockStatusFrmDataimport, DeleteUserAssignedDetails, FetchGoogleDetails //ZD 104862 //ZD 100815//ZD 101443//ZD 100815 //ALLDEV-498 //ALLDEV-856
        }

        internal enum HardwareCommands
        {
            DeleteEndpoint = 1,
            GetAddressType, GetBridges, GetEndpointDetails, GetLineRate, GetVideoEquipment,
            SetEndpoint, SearchEndpoint, GetAudioCodecs, GetVideoCodecs, GetVideoModes,
            GetMediaTypes, GetVideoProtocols, GetMCUAvailableResources, GetMCUCards,
            GetBridge, GetBridgeList, GetNewBridge, GetOldBridge, SetBridge, FetchBridgeInfo, GetAllEndpoints   //FB 1462,Endpoint Search
            , GetCompleteMCUusageReport, DeleteBridge, GetEndpoint, SetBridgeList //FB 1938, FB 2027
            , GetAllMessage, SetMessage, DeleteMessage, SetFavouriteMCU, //FB 2486 //FB 2501 - Call Monitoring
            GetMCUProfiles, GetExtMCUSilo, GetExtMCUServices, GetManufacturer, GetManufacturerModel, GetBridgeDetails, GetEndPointP2PQuery, GetSystemLocation //FB 2591 //FB 2556 //ZD 100736 //ZD 100522 //ZD 101525
            , GetSyncEndpoints, SetSyncEndpoints,GetEptResolution //ZD 100040 //ZD 101527
            , SetMCUGroup, GetMCUGroup, SearchMCUGroup, DeleteMCUGroup, GetMCUGroups, GetVirtualBridges //ZD 100040
			, GetMCUPoolOrders //ZD 104256
        };
        internal enum ReportCommands
        {
            GetConferenceReportList = 1, GetReportTypeList, GetInputParameters,
            GenerateReport, runReport, DeleteConferenceReport,
            GetOldScheduledReport, ConferenceReports,   //Report fixes
            GetTotalUsageMonth, GetSpecificUsageMonth, GetLocationsUsage, GetMCUUsage, FetchUserLogin //Added for Graphical Report FB 1969
            , GetUsageReports, GetMCCReport, GetMonthlydays, SetMonthlydays, GetWeeklydays, SetWeeklydays, Setdays //FB 2047 //FB 2155//FB 2343
            , UpdateCancelEvents, SetBatchReportConfig, GetAllBatchReports, GenerateBatchReport, DeleteBatchReport, GetEntityCodes /*FB 2410*/ //FB 2363 //FB 2501 P2P Call Monitoring GetEventLog FB 2569
        };
        internal enum SearchCommands
        {
            SearchConference = 1, GetLocationsList, GetSearchTemplateList, SetSearchTemplate, McuUsage, //FB 2027 McuUsage
            GetSearchTemplate, DeleteSearchTemplate, GetLocations, GetLocations2, SetTier1, SetTier2,
            DeleteTier1, DeleteTier2, GetRoomProfile, SetRoomProfile, GetDepartmentsForLocation, GetRoomTimeZoneMapping /*FB 1048*/ , 
            SearchAllConference, GetConfBaseDetails /*FB 826 */, GetIfDirtyorPast /*Added for Reccurance*/, GetIfDirty/*FB 1391*/, 
            GetRoomsDepts, GetBusyRooms, GetRoomBasicDetails, GetAllRooms, GetDeactivatedRooms, GetRoomLicenseDetails, SearchRooms,
            GetEncrpytedText, GetAllRoomsBasicInfo,/*fB 1756*/ Isconferenceschedulable, GetImages /*FB 1728*/ , GetOngoing, SearchTemplate,GetAllRoomsInfo, //ZD 100563
            GetSearchConference, GetOngoingMessageOverlay, GetCallsforMonitor, GetP2PCallsforMonitor,GetCallsforFMS //FB 2027 //FB 2501 Call Monitoring //FB 2501 p2p Call Monitoring //FB 2616
            , GetPrivatePublicRoomProfile, SetPrivatePublicRoomProfile, GetPrivatePublicRoomID, GetCallsForSwitching, UpdateCompletedConference, GetDecrpytedText, SelectedRooms //FB 2392//FB 2595 //Tik 100036 //ZD 100152 //ZD 101175
            , GetLocationsTier1, GetLocationsTier2, setRoomResizeImage, GetRPRMConfforLaunch, GetAllRoomsInfoforCache, GetAllRoomsInfoOpt //ZD 101611//ZD 101244 102600 //ZD 103790//ZD 104482
        }; 


        internal enum Organization
        {
            GetOrgSettings = 1, SetOrgSettings, GetOrgOptions, GetAllOrgSettings, SetOrgOptions, 
            GetOrganizationList, GetOrganizationProfile, SetOrganizationProfile, DeleteOrganizationProfile,
            GetActiveOrgDetails, SetPurgeDetails, GetPurgeDetails, CheckAPIEnable, UpdateOrgImages, SetTextChangeXML, SetDefaultCSSXML,  //Image Project ,CSS Module
            GetOrgImages, GetOrgEmails, GetOrgHolidays, SetOrgHolidays, SendAutomatedReminders, GetLogPreferences, DeleteModuleLog, GetOrgEmailDomains, SaveEmailDomain, GetHolidayType, DeleteHolidayDetails, SetDayOrderList, //FB 2052 //FB 2154//Login Management , FB 1860, FB 1861, FB 1926, FB 2027
            PurgeOrganization, GetOrgLicenseAgreement, SetOrgLicenseAgreement, SetOrgTextMsg, SetTheme, SetLDAPGroup, GetLDAPGroup, UpdateLDAPGroupRole //FB 2337 //FB 2074 //FB 2486 // FB 2719 Theme //ZD 101525
            , SetConfArchiveConfiguration, GetConfArchiveConfiguration //ZD 101835
            , OrgOptionsInfo // ZD 103398
        }; 
        //Added for FB 2027
        internal enum RoomFactory
        {
            GetRoomMonthlyView = 1, GetRoomWeeklyView, GetRoomDailyView, GetOldRoom, ManageConfRoom, GetLastModifiedRoom, 
            DeleteRoom, ActiveRoom, GetServiceType //FB 2027 DeleteRoom, ActiveRoom//FB 2149, FB 2219 
            , GetRoomDetails, GetRoomQueue, SetGuesttoNormalRoom, DeleteGuestRoom, //FB 2361 EWS //FB 2426
            GetRoomConferenceMonthlyView, RoomValidation, GetModifiedConference, GetAllRoomsList, ChkRoomAuthentication, GetRoomEWSDetails, UpdateRoomEWSDetails //FB 2724 //FB 2593  //ZD 100196 //ZD 101736
            , SetSyncRooms, GetSyncRooms, GetIcontrolconference //ZD 101527 //ZD 103460
			,GetTopTiers, GetMiddleTiersByTopTier, GetRoomsByTiers, //ZD 102358
			SetFloorPlan, GetFloorPlans, DeleteFloorPlan //ZD 101527 //ZD 102123
        }
        internal Hashtable CommandReference = new Hashtable();

        //FB 2136
        internal enum ImageFactory
        {
            GetAllSecImages = 1, GetSecurityImage, SetSecurityImage, DeleteSecurityImage, GetSecImages
        }

        internal myVRMDefines()
        {

            // conference commands
            CommandReference.Add("ConfirmConferenceInvitation", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.ConfirmConferenceInvitation);
            CommandReference.Add("GetAdvancedAVSettings", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.GetAdvancedAVSettings);
            CommandReference.Add("GetConferenceEndpoint", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.GetConferenceEndpoint);
            CommandReference.Add("SetConference", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.SetConference);
            CommandReference.Add("GetOldConference", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.GetOldConference);
            CommandReference.Add("SetAdvancedAVSettings", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.SetAdvancedAVSettings);
            CommandReference.Add("SetConferenceEndpoint", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.SetConferenceEndpoint);
            CommandReference.Add("SetTemplateOrder", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.SetTemplateOrder);
            CommandReference.Add("DeletePastConference", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.DeletePastConference);
            CommandReference.Add("DeleteCiscoICAL", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.DeleteCiscoICAL);
            CommandReference.Add("GetEndpointStatus", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.GetEndpointStatus);
            CommandReference.Add("CreateCiscoICALOnApproval", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.CreateCiscoICALOnApproval);
            //FB 2501 Call Monitoring
            CommandReference.Add("GetPacketLossTerminal", (int)CommandTypes.CONFERENCE * CommandOffset
                                                          + (int)ConferenceCommands.GetPacketLossTerminal);
            CommandReference.Add("LockTerminal", (int)CommandTypes.CONFERENCE * CommandOffset
                                                         + (int)ConferenceCommands.LockTerminal);
            //FB 2441 Starts
            CommandReference.Add("ConferenceRecording", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.ConferenceRecording);
            //FB 2441 Ends
            CommandReference.Add("MonitorMuteTerminal", (int)CommandTypes.CONFERENCE * CommandOffset
                                                         + (int)ConferenceCommands.MonitorMuteTerminal);
            CommandReference.Add("SetLeaderPerson", (int)CommandTypes.CONFERENCE * CommandOffset //FB 2553-RMX
                                                         + (int)ConferenceCommands.SetLeaderPerson);
            CommandReference.Add("SetLectureParty", (int)CommandTypes.CONFERENCE * CommandOffset //FB 2553-RMX
                                                         + (int)ConferenceCommands.SetLectureParty);

            //FB 1830 start
            CommandReference.Add("CheckApprovalEntity", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.CheckApprovalEntity);
            CommandReference.Add("TDGetAvailableSeats", (int)CommandTypes.CONFERENCE * CommandOffset //FB 2659 - Starts
                                                           + (int)ConferenceCommands.TDGetAvailableSeats);
            CommandReference.Add("TDGetMaximumAvailableSeats", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.TDGetMaximumAvailableSeats);
            CommandReference.Add("SendSynchronousBridgeemails", (int)CommandTypes.CONFERENCE * CommandOffset
                                                          + (int)ConferenceCommands.SendSynchronousBridgeemails);
            CommandReference.Add("TDGetSlotAvailableSeats", (int)CommandTypes.CONFERENCE * CommandOffset
                                                          + (int)ConferenceCommands.TDGetSlotAvailableSeats);//FB 2659 - End

            CommandReference.Add("TDGetDefaultSettings", (int)CommandTypes.CONFERENCE * CommandOffset
                                                          + (int)ConferenceCommands.TDGetDefaultSettings);//FB 2659 - End
			//FB 2724
            CommandReference.Add("SetConferenceSignin", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.SetConferenceSignin);
            CommandReference.Add("ChangeConfMCU", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.ChangeConfMCU); //ZD 100369:MCU FailOver
            CommandReference.Add("ForceConfDelete", (int)CommandTypes.CONFERENCE * CommandOffset
                                                          + (int)ConferenceCommands.ForceConfDelete); //ZD 100221
            CommandReference.Add("SetConferenceDetails", (int)CommandTypes.CONFERENCE * CommandOffset
                                                         + (int)ConferenceCommands.SetConferenceDetails); //ZD 102195
            CommandReference.Add("SendBJNBridgeemails", (int)CommandTypes.CONFERENCE * CommandOffset
                                                          + (int)ConferenceCommands.SendBJNBridgeemails);//ZD 103263
            CommandReference.Add("InsertConfAuditDetails", (int)CommandTypes.CONFERENCE * CommandOffset
                                              + (int)ConferenceCommands.InsertConfAuditDetails);//ZD 102754 
            CommandReference.Add("SetHDBusyConferences", (int)CommandTypes.CONFERENCE * CommandOffset
                                             + (int)ConferenceCommands.SetHDBusyConferences);//ALLDEV-807 
            CommandReference.Add("ScheduleHDBusyConferences", (int)CommandTypes.CONFERENCE * CommandOffset
                                             + (int)ConferenceCommands.ScheduleHDBusyConferences); //ALLDEV-807
            
            CommandReference.Add("GetEmailTypes", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetEmailTypes);
            CommandReference.Add("GetEmailContent", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetEmailContent);
            CommandReference.Add("SetEmailContent", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.SetEmailContent);
            CommandReference.Add("GetEmailLanguages", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetEmailLanguages);
			CommandReference.Add("GetLanguages", (int)CommandTypes.GENERAL * CommandOffset
                                                                    + (int)GeneralCommands.GetLanguages);
			CommandReference.Add("GetIconsReference", (int)CommandTypes.GENERAL * CommandOffset
                                                                    + (int)GeneralCommands.GetIconsReference); //NewLobby
            CommandReference.Add("SetIconReference", (int)CommandTypes.GENERAL * CommandOffset
                                                                    + (int)GeneralCommands.SetIconReference);//NewLobby
            
            CommandReference.Add("SendPartyReminder", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.SendPartyReminder);
            CommandReference.Add("DeleteEmailLanguage", (int)CommandTypes.GENERAL * CommandOffset
                                                        + (int)GeneralCommands.DeleteEmailLanguage); //FB 1830 - DeleteEmailLang
            //FB 1830 end
            CommandReference.Add("GetFeedback", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.GetFeedback); //FB 2027
            CommandReference.Add("SaveFiles", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.SaveFiles); //FB 2153
            CommandReference.Add("SetEntityCode", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.SetEntityCode); //FB 2045
            CommandReference.Add("UpdateEntityCode", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.UpdateEntityCode); //FB 2045
            CommandReference.Add("GetEntityCode", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.GetEntityCode); //FB 2045
            CommandReference.Add("DeleteEntityCode", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.DeleteEntityCode); //FB 2045
            CommandReference.Add("GetConfListByEntityOptID", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.GetConfListByEntityOptID); //FB 2045
            CommandReference.Add("EditConfEntityOption", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.EditConfEntityOption); //FB 2045
            CommandReference.Add("DeleteConfEntityOptionID", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.DeleteConfEntityOptionID); //FB 2045
            CommandReference.Add("HelpRequestMail", (int)CommandTypes.GENERAL * CommandOffset
                                                      + (int)GeneralCommands.HelpRequestMail); //FB 2268
            CommandReference.Add("GetSecurityBadgeType", (int)CommandTypes.GENERAL * CommandOffset
                                                       + (int)GeneralCommands.GetSecurityBadgeType); //FB 2136
            CommandReference.Add("GetPCDetails", (int)CommandTypes.GENERAL * CommandOffset
                                                                    + (int)GeneralCommands.GetPCDetails); //FB 2693 
            CommandReference.Add("GetGMTTime", (int)CommandTypes.GENERAL * CommandOffset
                                                                  + (int)GeneralCommands.GetGMTTime); //ZD 101120

            CommandReference.Add("DeleteRecurInstance", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.DeleteRecurInstance); //FB 1772
            CommandReference.Add("DelRecurInstanceByUID", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.DelRecurInstanceByUID); //FB 1772
            CommandReference.Add("EditRecurInstance", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.EditRecurInstance); //FB 1772
            CommandReference.Add("EditRecurInstanceByUID", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.EditRecurInstanceByUID); //FB 1772
            CommandReference.Add("DeleteParticipantICAL", (int)CommandTypes.CONFERENCE * CommandOffset
                                               + (int)ConferenceCommands.DeleteParticipantICAL); //FB 1782
            CommandReference.Add("CreateParticipantICALOnApproval", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.CreateParticipantICALOnApproval); //FB 1782
            CommandReference.Add("UpdateICalID", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.UpdateICalID); //FB 1782
            CommandReference.Add("DeleteTerminal", (int)CommandTypes.CONFERENCE * CommandOffset     
                                                           + (int)ConferenceCommands.DeleteTerminal);  //FB 2027
            CommandReference.Add("GetTemplateList", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.GetTemplateList);   //FB 2027
            CommandReference.Add("PartyInvitation", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.PartyInvitation); //FB 2027
            CommandReference.Add("TerminateConference", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.TerminateConference);//FB 2027
			CommandReference.Add("DeleteTemplate", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.DeleteTemplate);//FB 2027
			CommandReference.Add("CounterInvite", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.CounterInvite); //FB 2027
            CommandReference.Add("DisplayTerminal", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.DisplayTerminal);//FB 2027
			CommandReference.Add("MuteTerminal", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.MuteTerminal);//FB 2027
            CommandReference.Add("DeleteConference", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.DeleteConference); //FB 2027
            CommandReference.Add("GetNewTemplate", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.GetNewTemplate); //FB 2027
            CommandReference.Add("GetNewConference", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.GetNewConference); //FB 2027                                               
            CommandReference.Add("GetApprovalStatus", (int)CommandTypes.CONFERENCE * CommandOffset
                                                                       + (int)ConferenceCommands.GetApprovalStatus); //FB 2027
			CommandReference.Add("GetTerminalControl", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.GetTerminalControl);//FB 2027
			CommandReference.Add("GetOldTemplate", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.GetOldTemplate); //FB 2027 
			CommandReference.Add("GetConfGMTInfo", (int)CommandTypes.CONFERENCE * CommandOffset
                                                                       + (int)ConferenceCommands.GetConfGMTInfo); //FB 2027
            CommandReference.Add("SetDynamicUser", (int)CommandTypes.CONFERENCE * CommandOffset
                                                                       + (int)ConferenceCommands.SetDynamicUser); //FB 2027
            CommandReference.Add("SetTerminalControl", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.SetTerminalControl); //FB 2027
			CommandReference.Add("GetAvailableRoom", (int)CommandTypes.CONFERENCE * CommandOffset
                                                                       + (int)ConferenceCommands.GetAvailableRoom); //FB 2027            
			CommandReference.Add("GetTemplate", (int)CommandTypes.CONFERENCE * CommandOffset
                                                           + (int)ConferenceCommands.GetTemplate); //FB 2027
			CommandReference.Add("ResponseInvite", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.ResponseInvite);//FB 2027
			CommandReference.Add("GetInstances", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.GetInstances);//FB 2027
			CommandReference.Add("GetApproveConference", (int)CommandTypes.CONFERENCE * CommandOffset
                                                                       + (int)ConferenceCommands.GetApproveConference); //FB 2027
            CommandReference.Add("SetTemplate", (int)CommandTypes.CONFERENCE * CommandOffset
                                                                       + (int)ConferenceCommands.SetTemplate); //FB 2027
            CommandReference.Add("SetApproveConference", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.SetApproveConference); //FB 2027  
            CommandReference.Add("GetRecurDateList", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.GetRecurDateList); //FB 2027 SetConference 
            CommandReference.Add("GetRoombyMediaService", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.GetRoombyMediaService); //FB 2038  
            CommandReference.Add("GetConfMCUDetails", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.GetConfMCUDetails); //FB 2448
            CommandReference.Add("GetConfMsg", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.GetConfMsg); //FB 2486
            CommandReference.Add("SetP2PCallerEndpoint", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.SetP2PCallerEndpoint); //FB 2501 p2p Call Monitoring
            CommandReference.Add("SetP2PCallerLinerate", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.SetP2PCallerLinerate); //FB 2501 p2p Call Monitoring
			CommandReference.Add("GetConfAvailableRoom", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.GetConfAvailableRoom); //FB 2392
            CommandReference.Add("CheckConferenceApprovalStatus", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.CheckConferenceApprovalStatus); //ZD 100642
            CommandReference.Add("SetInstantConferenceDetails", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.SetInstantConferenceDetails); //ZD 100167 102195
            CommandReference.Add("CheckForExtendTimeConflicts", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.CheckForExtendTimeConflicts); //ZD 100819
            CommandReference.Add("CheckDialNumber", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.CheckDialNumber); //ZD 101657
            CommandReference.Add("SetConferenceFromDateImport", (int)CommandTypes.CONFERENCE * CommandOffset
                                                        + (int)ConferenceCommands.SetConferenceFromDateImport); //ZD 101879
            // --- Work Order Commands ---
            CommandReference.Add("GetInventoryDetails", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetInventoryDetails);
            CommandReference.Add("GetItemsList", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetItemsList);
            CommandReference.Add("GetInventoryList", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetInventoryList);
            CommandReference.Add("SetInventoryDetails", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.SetInventoryDetails);
            CommandReference.Add("DeleteInventory", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.DeleteInventory);
            CommandReference.Add("DeleteWorkOrder", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.DeleteWorkOrder);
            CommandReference.Add("GetWorkOrderDetails", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetWorkOrderDetails);
            CommandReference.Add("DeleteCategoryItem",     (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.DeleteCategoryItem);
            CommandReference.Add("SetCategoryItem", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.SetCategoryItem);
            CommandReference.Add("SetConferenceWorkOrders", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.SetConferenceWorkOrders);
            CommandReference.Add("GetRoomSets", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetRoomSets);
            CommandReference.Add("SendWorkOrderReminder", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.SendWorkOrderReminder);
            CommandReference.Add("SearchConferenceWorkOrders", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.SearchConferenceWorkOrders);
            CommandReference.Add("GetDeliveryTypes", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetDeliveryTypes);
            CommandReference.Add("GetProviderDetails", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetProviderDetails);
            CommandReference.Add("SetProviderDetails", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.SetProviderDetails);
            CommandReference.Add("GetCateringServices", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetCateringServices);
            CommandReference.Add("GetProviderWorkorderDetails", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.GetProviderWorkorderDetails);
            CommandReference.Add("SetProviderWorkorderDetails", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.SetProviderWorkorderDetails);
            CommandReference.Add("SearchProviderMenus", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.SearchProviderMenus);
            // FB# 854, 882
            CommandReference.Add("DeleteProviderMenu", (int)CommandTypes.WORKORDER * CommandOffset
                                                            + (int)WorkOrderCommands.DeleteProviderMenu);
            //user commands
            CommandReference.Add("GetMultipleUsers", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.GetMultipleUsers);
            CommandReference.Add("SetMultipleUsers", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.SetMultipleUsers);
            CommandReference.Add("DeleteLDAPUser", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.DeleteLDAPUser);
            CommandReference.Add("GetUserTemplateList", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.GetUserTemplateList);
            CommandReference.Add("GetUserTemplateDetails", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.GetUserTemplateDetails);
            CommandReference.Add("SetUserTemplate", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.SetUserTemplate);
            CommandReference.Add("DeleteUserTemplate", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.DeleteUserTemplate);
            CommandReference.Add("GetUserDetails", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.GetUserDetails);
            CommandReference.Add("SetUserDetails", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.SetUserDetails);
            CommandReference.Add("GetUserList", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.GetUserList);
            CommandReference.Add("ConvertToGMT", (int)CommandTypes.USER * CommandOffset
                                                                      + (int)UserCommands.ConvertToGMT);
            CommandReference.Add("ConvertFromGMT", (int)CommandTypes.USER * CommandOffset
                                                                      + (int)UserCommands.ConvertFromGMT);
            CommandReference.Add("ChangeUserStatus", (int)CommandTypes.USER * CommandOffset
                                                                      + (int)UserCommands.ChangeUserStatus);
            CommandReference.Add("ChangeGuestStatus", (int)CommandTypes.USER * CommandOffset
                                                                     + (int)UserCommands.ChangeUserStatus);
            CommandReference.Add("GetManageUser", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetManageUser);
            CommandReference.Add("GetOldUser", (int)CommandTypes.USER * CommandOffset
            
                                                        + (int)UserCommands.GetOldUser);
            CommandReference.Add("GetUserPreference", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetUserPreference); //FB 2027
			CommandReference.Add("GetAllocation", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetAllocation); //FB 2027
            
            CommandReference.Add("SetUserLobbyIcons", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.SetUserLobbyIcons);//NewLobby

            CommandReference.Add("SetBridgenumbers", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.SetBridgenumbers);// FB 2227
            
             //Code added for FB Issue 826 - start
            CommandReference.Add("GetAllManageUser", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.GetAllManageUser);
            //Code added for FB Issue 826 - end                                                        

            CommandReference.Add("SetBulkUserUpdate", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserUpdate);
            //Added for Webservice Start
            CommandReference.Add("ChkUsrAuthentication", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.ChkUsrAuthentication);

            CommandReference.Add("CheckUserCredentials", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.CheckUserCredentials);

            CommandReference.Add("BlockAllUsers", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.BlockAllUsers);

            CommandReference.Add("UnBlockAllUsers", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.UnBlockAllUsers);
            CommandReference.Add("GetAudioUserList", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.GetAudioUserList);//Code added for audio addon
            CommandReference.Add("RequestPassword", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.RequestPassword); //FB 1830 - start

            CommandReference.Add("SetExpirePassword", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetExpirePassword);
            CommandReference.Add("ChangePasswordRequest", (int)CommandTypes.USER * CommandOffset // ZD 103954
                                                                   + (int)UserCommands.ChangePasswordRequest); //ZD 100263
            CommandReference.Add("GetRequestIDUser", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.GetRequestIDUser); //ZD 100263
            CommandReference.Add("RequestVRMAccount", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.RequestVRMAccount);  //FB 1830 - end
            CommandReference.Add("GetUserEmails", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.GetUserEmails);  //FB 1860
            //CommandReference.Add("GetUserEmailsBlockStatus", (int)CommandTypes.USER * CommandOffset
            //                                                      + (int)UserCommands.GetUserEmailsBlockStatus);  //FB 1860 //FB 2027
            CommandReference.Add("SetUserEmailsBlockStatus", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.SetUserEmailsBlockStatus);  //FB 1860            
            CommandReference.Add("SetPreferedRoom", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.SetPreferedRoom); //FB 1959
            CommandReference.Add("GetPreferedRoom", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.GetPreferedRoom); //FB 1959
            CommandReference.Add("SetUserRoles", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.SetUserRoles);  //FB 2027
            CommandReference.Add("SearchUserOrGuest", (int)CommandTypes.USER * CommandOffset
                                                                      + (int)UserCommands.SearchUserOrGuest);//FB 2027
            
            CommandReference.Add("RetrieveUsers", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.RetrieveUsers); //FB 2027
            CommandReference.Add("RetrieveGuest", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.RetrieveGuest); //FB 2027
			CommandReference.Add("GetManageGuest", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetManageGuest); //FB 2027
            CommandReference.Add("GetUsers", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetUsers); //FB 2027
            CommandReference.Add("SetOldUser", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.SetOldUser); //FB 2027
			CommandReference.Add("DeleteAllGuests", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.DeleteAllGuests);  //FB 2027
            CommandReference.Add("SetGroup", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetGroup);   //FB 2027
            CommandReference.Add("GetGroup", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.GetGroup);  //FB 2027     
            CommandReference.Add("SearchGroup", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.SearchGroup);  //FB 2027     
            CommandReference.Add("SetUserStatus", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.SetUserStatus); //FB 2027
			CommandReference.Add("GuestRegister", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.GuestRegister); //FB 2027
            CommandReference.Add("GetUserRoles", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.GetUserRoles);  //FB 2027
            CommandReference.Add("DeleteGroup", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.DeleteGroup);  //FB 2027
			CommandReference.Add("GetEmailList", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetEmailList); //FB 2027
            CommandReference.Add("GetGuestList", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetGuestList); //FB 2027
			CommandReference.Add("GetManageBulkUsers", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.GetManageBulkUsers); //FB 2027
			CommandReference.Add("GetSettingsSelect", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetSettingsSelect); //FB 2027
			CommandReference.Add("SetBulkUserAddMinutes", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserAddMinutes); //FB 2027
			CommandReference.Add("SetBulkUserBridge", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserBridge); //FB 2027
			CommandReference.Add("SetBulkUserDelete", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserDelete); //FB 2027
			CommandReference.Add("SetBulkUserDepartment", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserDepartment); //FB 2027
			CommandReference.Add("SetBulkUserExpiryDate", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserExpiryDate); //FB 2027
			CommandReference.Add("SetBulkUserLanguage", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserLanguage); //FB 2027
			CommandReference.Add("SetBulkUserLock", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserLock); //FB 2027
			CommandReference.Add("SetBulkUserRole", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserRole); //FB 2027
			CommandReference.Add("SetBulkUserTimeZone", (int)CommandTypes.USER * CommandOffset
                                                                   + (int)UserCommands.SetBulkUserTimeZone); //FB 2027
			CommandReference.Add("SetUser", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.SetUser); //FB 2027
            CommandReference.Add("GetHome", (int)CommandTypes.USER * CommandOffset
                                                                    + (int)UserCommands.GetHome); //FB 2027

            CommandReference.Add("GetLanguageTexts", (int)CommandTypes.USER * CommandOffset
                                                                  + (int)UserCommands.GetLanguageTexts);//FB 1830 - Translation 
            CommandReference.Add("SetPhoneNumber", (int)CommandTypes.USER * CommandOffset
                                                                 + (int)UserCommands.SetPhoneNumber);//FB 2268
            CommandReference.Add("UserAuthentication", (int)CommandTypes.USER * CommandOffset
                                                                 + (int)UserCommands.UserAuthentication);//FB 2558 WhyGo
            CommandReference.Add("GetVNOCUserList", (int)CommandTypes.USER * CommandOffset
                                                                + (int)UserCommands.GetVNOCUserList);//FB 2670
            CommandReference.Add("FetchSelectedPCDetails", (int)CommandTypes.USER * CommandOffset
                                                                 + (int)UserCommands.FetchSelectedPCDetails);//FB 2693
            CommandReference.Add("SetCalendarTimes", (int)CommandTypes.USER * CommandOffset
                                                                 + (int)UserCommands.SetCalendarTimes);//ZD 100157
            CommandReference.Add("GetCalendarTimes", (int)CommandTypes.USER * CommandOffset
                                                                 + (int)UserCommands.GetCalendarTimes); //ZD 100157
			CommandReference.Add("PasswordChangeRequest", (int)CommandTypes.USER * CommandOffset
                                                                 + (int)UserCommands.PasswordChangeRequest); //ZD 100781
			//ZD 100152 Starts
			CommandReference.Add("SetUserToken", (int)CommandTypes.USER * CommandOffset
                                                                + (int)UserCommands.SetUserToken);
            CommandReference.Add("GetGoogleChannelExpiredList", (int)CommandTypes.USER * CommandOffset
                                                               + (int)UserCommands.GetGoogleChannelExpiredList);
            CommandReference.Add("GetGoogleChannelDetails", (int)CommandTypes.USER * CommandOffset
                                                               + (int)UserCommands.GetGoogleChannelDetails);
            //ZD 100621 Start
            CommandReference.Add("SetUserRoomViewType", (int)CommandTypes.USER * CommandOffset
                                                       + (int)UserCommands.SetUserRoomViewType);

            CommandReference.Add("GetUserRoomViewType", (int)CommandTypes.USER * CommandOffset
                                                      + (int)UserCommands.GetUserRoomViewType);
            //ZD 100621 end 
			//ZD 100152 Ends

            CommandReference.Add("GetUserType", (int)CommandTypes.USER * CommandOffset
                                                    + (int)UserCommands.GetUserType); //ZD 100815
            CommandReference.Add("SearchUserInfo", (int)CommandTypes.USER * CommandOffset
                                                    + (int)UserCommands.SearchUserInfo); //ZD 101443
            CommandReference.Add("SetAssignedUserAdmin", (int)CommandTypes.USER * CommandOffset
                                                    + (int)UserCommands.SetAssignedUserAdmin); //ZD 101443
			//ZD 101308
            CommandReference.Add("ChkADAuthentication", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.ChkADAuthentication);

            CommandReference.Add("SetUsrBlockStatusFrmDataimport", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.SetUsrBlockStatusFrmDataimport);//ZD 104862
            CommandReference.Add("DeleteUserAssignedDetails", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.DeleteUserAssignedDetails); //ALLDEV-498
            CommandReference.Add("FetchGoogleDetails", (int)CommandTypes.USER * CommandOffset
                                                            + (int)UserCommands.FetchGoogleDetails); //ALLDEV-856
            
            //Added for Webservice End
            // Hardware commands
            CommandReference.Add("DeleteEndpoint", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.DeleteEndpoint);
            CommandReference.Add("GetAddressType", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetAddressType);
            CommandReference.Add("GetBridges", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetBridges);
            CommandReference.Add("GetEndpointDetails", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetEndpointDetails);
            CommandReference.Add("GetLineRate", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetLineRate);
            CommandReference.Add("GetVideoEquipment", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetVideoEquipment);
            CommandReference.Add("GetManufacturer", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetManufacturer);//ZD 100736 
            CommandReference.Add("GetManufacturerModel", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetManufacturerModel);//ZD 100736
            CommandReference.Add("SetEndpoint", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SetEndpoint);
            CommandReference.Add("SearchEndpoint", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SearchEndpoint);
            CommandReference.Add("GetAudioCodecs", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetAudioCodecs);
            CommandReference.Add("GetVideoCodecs", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetVideoCodecs);
            CommandReference.Add("GetVideoModes", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetVideoModes);
            CommandReference.Add("GetMediaTypes", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetMediaTypes);
            CommandReference.Add("GetVideoProtocols", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetVideoProtocols);
            CommandReference.Add("GetMCUAvailableResources", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetMCUAvailableResources);
            CommandReference.Add("GetMCUCards", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetMCUCards);
            CommandReference.Add("GetBridge", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetBridge);
            CommandReference.Add("GetBridgeList", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetBridgeList);
            CommandReference.Add("GetNewBridge", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetNewBridge);
            CommandReference.Add("GetOldBridge", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetOldBridge);
            CommandReference.Add("SetBridge", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SetBridge);
            CommandReference.Add("FetchBridgeInfo", (int)CommandTypes.HARDWARE * CommandOffset      //FB 1462
                                                            + (int)HardwareCommands.FetchBridgeInfo);
            CommandReference.Add("GetAllEndpoints", (int)CommandTypes.HARDWARE * CommandOffset      //Endpoint Search
                                                            + (int)HardwareCommands.GetAllEndpoints);
            CommandReference.Add("GetCompleteMCUusageReport", (int)CommandTypes.HARDWARE * CommandOffset      //FB 1938
                                                            + (int)HardwareCommands.GetCompleteMCUusageReport);
			CommandReference.Add("DeleteBridge", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.DeleteBridge); //FB 2027
			CommandReference.Add("GetEndpoint", (int)CommandTypes.HARDWARE * CommandOffset         //FB 2027
                                                            + (int)HardwareCommands.GetEndpoint); 
            CommandReference.Add("SetBridgeList", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SetBridgeList);//FB 2027
            CommandReference.Add("GetAllMessage", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetAllMessage);//FB 2486
            CommandReference.Add("SetMessage", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SetMessage);//FB 2486
            CommandReference.Add("DeleteMessage", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.DeleteMessage);//FB 2486
            CommandReference.Add("SetFavouriteMCU", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SetFavouriteMCU);//FB 2501 - Call Monitoring

            CommandReference.Add("GetMCUProfiles", (int)CommandTypes.HARDWARE * CommandOffset
                + (int)HardwareCommands.GetMCUProfiles); //FB 2591

            CommandReference.Add("GetExtMCUSilo", (int)CommandTypes.HARDWARE * CommandOffset
                + (int)HardwareCommands.GetExtMCUSilo); //FB 2556

            CommandReference.Add("GetExtMCUServices", (int)CommandTypes.HARDWARE * CommandOffset
                + (int)HardwareCommands.GetExtMCUServices); //FB 2556
            CommandReference.Add("GetBridgeDetails", (int)CommandTypes.HARDWARE * CommandOffset
                + (int)HardwareCommands.GetBridgeDetails); //ZD 100522 

            CommandReference.Add("GetEndPointP2PQuery", (int)CommandTypes.HARDWARE * CommandOffset //ZD 100815
               + (int)HardwareCommands.GetEndPointP2PQuery);


            CommandReference.Add("GetSystemLocation", (int)CommandTypes.HARDWARE * CommandOffset //ZD 100815
               + (int)HardwareCommands.GetSystemLocation);
            //ZD 101527 Starts
            CommandReference.Add("SetSyncEndpoints", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SetSyncEndpoints);
            CommandReference.Add("GetSyncEndpoints", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetSyncEndpoints);
            //ZD 101527 Ends
            CommandReference.Add("GetEptResolution", (int)CommandTypes.HARDWARE * CommandOffset
                                                           + (int)HardwareCommands.GetEptResolution);//ZD 100040
            //ZD 100040 Starts
            CommandReference.Add("SetMCUGroup", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SetMCUGroup);
            CommandReference.Add("GetMCUGroup", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetMCUGroup);
            CommandReference.Add("SearchMCUGroup", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.SearchMCUGroup);
            CommandReference.Add("DeleteMCUGroup", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.DeleteMCUGroup);
            CommandReference.Add("GetMCUGroups", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetMCUGroups);
            CommandReference.Add("GetVirtualBridges", (int)CommandTypes.HARDWARE * CommandOffset
                                                            + (int)HardwareCommands.GetVirtualBridges);
            //ZD 100040 Ends
			CommandReference.Add("GetMCUPoolOrders", (int)CommandTypes.HARDWARE * CommandOffset
               + (int)HardwareCommands.GetMCUPoolOrders); //ZD 104256
            // Report commands
            CommandReference.Add("GetConferenceReportList", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetConferenceReportList);
            CommandReference.Add("GetReportTypeList", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetReportTypeList);
            CommandReference.Add("GetInputParameters", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetInputParameters);
            CommandReference.Add("GenerateReport", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GenerateReport);
            CommandReference.Add("runReport", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.runReport);
            CommandReference.Add("DeleteConferenceReport", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.DeleteConferenceReport);
            CommandReference.Add("GetOldScheduledReport", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetOldScheduledReport);
            CommandReference.Add("FetchUserLogin", (int)CommandTypes.REPORTS * CommandOffset     //FB 1969
                                                                  + (int)ReportCommands.FetchUserLogin);
            //Code Added for SQL Reports
            CommandReference.Add("ConferenceReports", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.ConferenceReports);
            //Added for Graphical Reports START
            CommandReference.Add("GetTotalUsageMonth", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetTotalUsageMonth);
            CommandReference.Add("GetSpecificUsageMonth", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetSpecificUsageMonth);
            CommandReference.Add("GetLocationsUsage", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetLocationsUsage);
            CommandReference.Add("GetMCUUsage", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetMCUUsage);
            CommandReference.Add("GetUsageReports", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetUsageReports);
			CommandReference.Add("GetMCCReport", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetMCCReport); //FB 2047
            CommandReference.Add("GetEntityCodes", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetEntityCodes); //ZD 102909
            //FB 2343 Start
            CommandReference.Add("GetMonthlydays", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetMonthlydays); 
            CommandReference.Add("SetMonthlydays", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.SetMonthlydays);
            CommandReference.Add("GetWeeklydays", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetWeeklydays);
            CommandReference.Add("SetWeeklydays", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.SetWeeklydays);
            CommandReference.Add("Setdays", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.Setdays);
            CommandReference.Add("UpdateCancelEvents", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.UpdateCancelEvents); //FB 2363
            //CommandReference.Add("GetEventLog", (int)CommandTypes.REPORTS * CommandOffset
            //                                           + (int)ReportCommands.GetEventLog); //FB 2501 P2P Call Monitoring Commented for FB 2569
            CommandReference.Add("MuteUnMuteParticipants", (int)CommandTypes.CONFERENCE * CommandOffset
                                                            + (int)ConferenceCommands.MuteUnMuteParticipants);//FB 2441
            //FB 2343 End
            //Added for Graphical Reports END
			//FB 2410 - Starts
			CommandReference.Add("SetBatchReportConfig", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.SetBatchReportConfig);
            CommandReference.Add("GetAllBatchReports", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GetAllBatchReports);
            CommandReference.Add("GenerateBatchReport", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.GenerateBatchReport);
            CommandReference.Add("DeleteBatchReport", (int)CommandTypes.REPORTS * CommandOffset
                                                            + (int)ReportCommands.DeleteBatchReport);
			//FB 2410 - Ends
            // GeneralComomands commands
            CommandReference.Add("GetTimezones",            (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetTimezones);
            CommandReference.Add("GetDialingOptions",       (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetDialingOptions);
            CommandReference.Add("Feedback",                (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.Feedback);
            CommandReference.Add("UpdateManageDepartment",  (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.UpdateManageDepartment);           
            CommandReference.Add("GetCountryCodes", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetCountryCodes);
            //FB 2671 Starts
            CommandReference.Add("GetPublicCountries", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetPublicCountries);
            CommandReference.Add("GetPublicCountryStates", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetPublicCountryStates);
            CommandReference.Add("GetPublicCountryStateCities", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetPublicCountryStateCities);
            //FB 2671 Ends
            CommandReference.Add("GetCountryStates", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetCountryStates);
            CommandReference.Add("GetManageDepartment", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.GetManageDepartment);
            //code added for custom attribute fixes - start

            CommandReference.Add("SetCustomAttribute", (int)CommandTypes.GENERAL * CommandOffset
                                                            + (int)GeneralCommands.SetCustomAttribute);
            CommandReference.Add("DeleteCustomAttribute", (int)CommandTypes.GENERAL * CommandOffset
                                                                        + (int)GeneralCommands.DeleteCustomAttribute);

            CommandReference.Add("GetCustomAttributes", (int)CommandTypes.GENERAL * CommandOffset
                                                                       + (int)GeneralCommands.GetCustomAttributes);
            CommandReference.Add("GetCustomAttributeDescription", (int)CommandTypes.GENERAL * CommandOffset
                                                                       + (int)GeneralCommands.GetCustomAttributeDescription);
            CommandReference.Add("GetCustomAttributeByID", (int)CommandTypes.GENERAL * CommandOffset
                                                                        + (int)GeneralCommands.GetCustomAttributeByID);
            CommandReference.Add("IsCustomAttrLinkedToConf", (int)CommandTypes.GENERAL * CommandOffset
                                                                        + (int)GeneralCommands.IsCustomAttrLinkedToConf);
            CommandReference.Add("GetConfListByCustOptID", (int)CommandTypes.GENERAL * CommandOffset
                                                                       + (int)GeneralCommands.GetConfListByCustOptID);
            CommandReference.Add("DeleteConfsAttributeByID", (int)CommandTypes.GENERAL * CommandOffset
                                                                        + (int)GeneralCommands.DeleteConfsAttributeByID);
            CommandReference.Add("GetAllDepartments", (int)CommandTypes.GENERAL * CommandOffset
                                                                        + (int)GeneralCommands.GetAllDepartments);  //FB 2047
            
            //code added for custom attribute fixes - end                                                           + (int)GeneralCommands.GetCustomAttributeDescription);
          
            //code added for Add FB 1470 - end

            
            //Diagnostics - start
            CommandReference.Add("DeleteAllData", (int)CommandTypes.GENERAL * CommandOffset
                                                                     + (int)GeneralCommands.DeleteAllData);

            CommandReference.Add("GetSQLServerInfo", (int)CommandTypes.GENERAL * CommandOffset
                                                                     + (int)GeneralCommands.GetSQLServerInfo);
            //Diagnostics - end
            CommandReference.Add("GetAllTimezones", (int)CommandTypes.GENERAL * CommandOffset
                                                                     + (int)GeneralCommands.GetAllTimezones);
            CommandReference.Add("GetAllImages", (int)CommandTypes.GENERAL * CommandOffset
                                                                     + (int)GeneralCommands.GetAllImages);
            CommandReference.Add("SetImage", (int)CommandTypes.GENERAL * CommandOffset
                                                                     + (int)GeneralCommands.SetImage);
            CommandReference.Add("DeleteImage", (int)CommandTypes.GENERAL * CommandOffset
                                                                     + (int)GeneralCommands.DeleteImage);
            // FB 1860
            CommandReference.Add("GetEmailsBlockStatus", (int)CommandTypes.GENERAL * CommandOffset
                                                                        + (int)GeneralCommands.GetEmailsBlockStatus);
            CommandReference.Add("DeleteEmails", (int)CommandTypes.GENERAL * CommandOffset
                                                                     + (int)GeneralCommands.DeleteEmails);
            CommandReference.Add("SetBlockEmail", (int)CommandTypes.GENERAL * CommandOffset
                                                                    + (int)GeneralCommands.SetBlockEmail);

            
            // SearchComomands commands
            CommandReference.Add("SearchConference",        (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.SearchConference);
            CommandReference.Add("GetLocationsList",        (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.GetLocationsList);
            CommandReference.Add("GetSearchTemplateList",   (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.GetSearchTemplateList);
            CommandReference.Add("SetSearchTemplate",       (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.SetSearchTemplate);
            CommandReference.Add("GetSearchTemplate",       (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.GetSearchTemplate);
            CommandReference.Add("DeleteSearchTemplate",    (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.DeleteSearchTemplate);
            CommandReference.Add("GetLocations", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.GetLocations);
            CommandReference.Add("GetLocations2", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.GetLocations2);
            CommandReference.Add("SetTier1", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.SetTier1);
            CommandReference.Add("SetTier2", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.SetTier2);
            CommandReference.Add("DeleteTier1", (int)CommandTypes.SEARCH * CommandOffset
                                                                       + (int)SearchCommands.DeleteTier1);
            CommandReference.Add("DeleteTier2", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.DeleteTier2);
            CommandReference.Add("GetRoomProfile", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.GetRoomProfile);
            CommandReference.Add("SetRoomProfile", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.SetRoomProfile);
            CommandReference.Add("GetDepartmentsForLocation", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.GetDepartmentsForLocation);
            //FB 1048
            CommandReference.Add("GetRoomTimeZoneMapping", (int)CommandTypes.SEARCH * CommandOffset
                                                          + (int)SearchCommands.GetRoomTimeZoneMapping);
            CommandReference.Add("McuUsage", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.McuUsage);//FB 2027 McuUsage
            CommandReference.Add("GetCallsforMonitor", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetCallsforMonitor);//FB 2501 Call Monitoring
            CommandReference.Add("GetP2PCallsforMonitor", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetP2PCallsforMonitor);//FB 2501 p2p Call Monitoring

            CommandReference.Add("GetPrivatePublicRoomProfile", (int)CommandTypes.SEARCH * CommandOffset
                                                          + (int)SearchCommands.GetPrivatePublicRoomProfile);//FB 2392
            CommandReference.Add("SetPrivatePublicRoomProfile", (int)CommandTypes.SEARCH * CommandOffset
                                                            + (int)SearchCommands.SetPrivatePublicRoomProfile);
            CommandReference.Add("GetPrivatePublicRoomID", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetPrivatePublicRoomID);
            CommandReference.Add("GetCallsForSwitching", (int)CommandTypes.SEARCH * CommandOffset
                                                          + (int)SearchCommands.GetCallsForSwitching); //FB 2595
            CommandReference.Add("GetRPRMConfforLaunch", (int)CommandTypes.SEARCH * CommandOffset
                                                          + (int)SearchCommands.GetRPRMConfforLaunch); //ZD 102600
            
            //System Commands
            CommandReference.Add("GetLicenseDetails", (int)CommandTypes.SYSTEM * CommandOffset
                                                           + (int)SystemCommands.GetLicenseDetails);

            CommandReference.Add("SetImagekey", (int)CommandTypes.SYSTEM * CommandOffset
                                                          + (int)SystemCommands.SetImagekey);//Site Logo
            CommandReference.Add("GetActivation", (int)CommandTypes.SYSTEM * CommandOffset
                                                          + (int)SystemCommands.GetActivation);
            CommandReference.Add("GetSiteImage", (int)CommandTypes.SYSTEM * CommandOffset
                                                         + (int)SystemCommands.GetSiteImage);//Site Logo
            CommandReference.Add("DeleteSiteImage", (int)CommandTypes.SYSTEM * CommandOffset
                                                         + (int)SystemCommands.DeleteSiteImage);//Site Logo
			CommandReference.Add("SearchLog", (int)CommandTypes.SYSTEM * CommandOffset
                                                          + (int)SystemCommands.SearchLog);//FB 2027
            //LDAP Fixes
            CommandReference.Add("GetLDAPUsers", (int)CommandTypes.SYSTEM * CommandOffset
                                                          + (int)SystemCommands.GetLDAPUsers);
            CommandReference.Add("GetLastMailRunDate", (int)CommandTypes.SYSTEM * CommandOffset
                                                          + (int)SystemCommands.GetLastMailRunDate);
            CommandReference.Add("GetSysMailData", (int)CommandTypes.SYSTEM * CommandOffset
                                                        + (int)SystemCommands.GetSysMailData);//RSS Fix
            CommandReference.Add("SetSuperAdmin", (int)CommandTypes.SYSTEM * CommandOffset
                                                        + (int)SystemCommands.SetSuperAdmin); //FB 2027
            CommandReference.Add("GetSystemDateTime", (int)CommandTypes.SYSTEM * CommandOffset
                                                        + (int)SystemCommands.GetSystemDateTime);//FB 2027
            CommandReference.Add("GetSuperAdmin", (int)CommandTypes.SYSTEM * CommandOffset
                                                        + (int)SystemCommands.GetSuperAdmin);//FB 2027
            CommandReference.Add("GenerateESUserReport", (int)CommandTypes.SYSTEM * CommandOffset
                                                        + (int)SystemCommands.GenerateESUserReport);//FB 2363
            CommandReference.Add("GenerateESErrorReport", (int)CommandTypes.SYSTEM * CommandOffset
			                                            + (int)SystemCommands.GenerateESErrorReport);//FB 2363
            CommandReference.Add("SyncWithLdap", (int)CommandTypes.SYSTEM * CommandOffset
                                                        + (int)SystemCommands.SyncWithLdap);//FB 2462 ZD102045
            CommandReference.Add("DeltePublicRoomEP", (int)CommandTypes.SYSTEM * CommandOffset
                                                       + (int)SystemCommands.DeltePublicRoomEP);//FB 2594 
            CommandReference.Add("KeepAlive", (int)CommandTypes.SYSTEM * CommandOffset
                                                       + (int)SystemCommands.KeepAlive);//FB 2616
            CommandReference.Add("SetDefaultLicense", (int)CommandTypes.SYSTEM * CommandOffset //FB 2659
                                                          + (int)SystemCommands.SetDefaultLicense);
            CommandReference.Add("GetDefaultLicense", (int)CommandTypes.SYSTEM * CommandOffset
                                                                      + (int)SystemCommands.GetDefaultLicense);
            CommandReference.Add("SendMailtoAdmin", (int)CommandTypes.SYSTEM * CommandOffset
                                                                      + (int)SystemCommands.SendMailtoAdmin); //ZD 100230

            CommandReference.Add("SendPasswordRequest", (int)CommandTypes.SYSTEM * CommandOffset
                                                                     + (int)SystemCommands.SendPasswordRequest); //ZD 100781
            CommandReference.Add("GetOrgLDAP", (int)CommandTypes.SYSTEM * CommandOffset
                                                          + (int)SystemCommands.GetOrgLDAP); //ZD 101525
            
            // FB 826
			CommandReference.Add("SearchAllConference", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.SearchAllConference);
            // FB 826 
            CommandReference.Add("GetConfBaseDetails", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetConfBaseDetails);
            /*Added For Reccurence fix*/
            CommandReference.Add("GetIfDirtyorPast", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetIfDirtyorPast);

            CommandReference.Add("GetIfDirty", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetIfDirty);

            CommandReference.Add("GetRoomsDepts", (int)CommandTypes.SEARCH * CommandOffset
                                                          + (int)SearchCommands.GetRoomsDepts);

            CommandReference.Add("GetBusyRooms", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetBusyRooms);
            CommandReference.Add("GetRoomBasicDetails", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetRoomBasicDetails);

            CommandReference.Add("GetAllRooms", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetAllRooms);
            //FB 1756
            CommandReference.Add("GetAllRoomsBasicInfo", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetAllRoomsBasicInfo);
            CommandReference.Add("GetAllRoomsInfo", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetAllRoomsInfo);
            //ZD 104482
			CommandReference.Add("GetAllRoomsInfoOpt", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetAllRoomsInfoOpt);
            
            //ZD 101175
            CommandReference.Add("SelectedRooms", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.SelectedRooms);
            CommandReference.Add("GetDeactivatedRooms", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetDeactivatedRooms);
            CommandReference.Add("GetRoomLicenseDetails", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetRoomLicenseDetails);
            CommandReference.Add("SearchRooms", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.SearchRooms);

            CommandReference.Add("GetEncrpytedText", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetEncrpytedText);
            CommandReference.Add("GetDecrpytedText", (int)CommandTypes.SEARCH * CommandOffset 
                                                           + (int)SearchCommands.GetDecrpytedText); //ZD 100152
            //FB 1728
            CommandReference.Add("Isconferenceschedulable", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.Isconferenceschedulable);
            CommandReference.Add("GetImages", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetImages);

            CommandReference.Add("GetOngoing", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetOngoing); //Service Manager
            CommandReference.Add("SearchTemplate", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.SearchTemplate);   //FB 2027
            CommandReference.Add("GetSearchConference", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetSearchConference); //FB 2027
            CommandReference.Add("GetOngoingMessageOverlay", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetOngoingMessageOverlay); //Service Manager
            CommandReference.Add("GetCallsforFMS", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetCallsforFMS); //FB 2616
            CommandReference.Add("UpdateCompletedConference", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.UpdateCompletedConference); //Tik 100036

            CommandReference.Add("GetAllRoomsInfoforCache", (int)CommandTypes.SEARCH * CommandOffset
                                                           + (int)SearchCommands.GetAllRoomsInfoforCache); //ZD 103790

            

            CommandReference.Add("GetOrgSettings", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                           + (int)Organization.GetOrgSettings);
            CommandReference.Add("SetOrgSettings", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                           + (int)Organization.SetOrgSettings);
            CommandReference.Add("GetOrgOptions", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                           + (int)Organization.GetOrgOptions);
            CommandReference.Add("GetAllOrgSettings", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                           + (int)Organization.GetAllOrgSettings);
            CommandReference.Add("SetOrgOptions", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                           + (int)Organization.SetOrgOptions);
            CommandReference.Add("GetOrganizationList", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                           + (int)Organization.GetOrganizationList);
            CommandReference.Add("GetOrganizationProfile", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                          + (int)Organization.GetOrganizationProfile);
            CommandReference.Add("SetOrganizationProfile", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                          + (int)Organization.SetOrganizationProfile);
            CommandReference.Add("DeleteOrganizationProfile", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                          + (int)Organization.DeleteOrganizationProfile);
            CommandReference.Add("GetActiveOrgDetails", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                          + (int)Organization.GetActiveOrgDetails);
            CommandReference.Add("SetPurgeDetails", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                         + (int)Organization.SetPurgeDetails);
            CommandReference.Add("GetPurgeDetails", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                          + (int)Organization.GetPurgeDetails);
            //License modification
            CommandReference.Add("CheckAPIEnable", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                          + (int)Organization.CheckAPIEnable);
            CommandReference.Add("UpdateOrgImages", (int)CommandTypes.ORGANIZATION * CommandOffset //Image Project
                                                          + (int)Organization.UpdateOrgImages);
            CommandReference.Add("SetTheme", (int)CommandTypes.ORGANIZATION * CommandOffset // FB 2719 Theme
                                                         + (int)Organization.SetTheme);
            CommandReference.Add("SetTextChangeXML", (int)CommandTypes.ORGANIZATION * CommandOffset //CSS Module
                                              + (int)Organization.SetTextChangeXML);
            CommandReference.Add("SetDefaultCSSXML", (int)CommandTypes.ORGANIZATION * CommandOffset //CSS Module
                                             + (int)Organization.SetDefaultCSSXML);
            CommandReference.Add("GetOrgImages", (int)CommandTypes.ORGANIZATION * CommandOffset //CSS Module
                                             + (int)Organization.GetOrgImages);

            CommandReference.Add("GetOrgEmails", (int)CommandTypes.ORGANIZATION * CommandOffset //FB 1860
                                            + (int)Organization.GetOrgEmails);

            CommandReference.Add("GetOrgHolidays", (int)CommandTypes.ORGANIZATION * CommandOffset //FB 1861
                                            + (int)Organization.GetOrgHolidays);
            CommandReference.Add("SetOrgHolidays", (int)CommandTypes.ORGANIZATION * CommandOffset //FB 1861
                                            + (int)Organization.SetOrgHolidays);

            CommandReference.Add("SendAutomatedReminders", (int)CommandTypes.ORGANIZATION * CommandOffset //FB 1926
                                            + (int)Organization.SendAutomatedReminders);
            CommandReference.Add("GetLogPreferences", (int)CommandTypes.ORGANIZATION * CommandOffset//FB 2027
                                                          + (int)Organization.GetLogPreferences);
            CommandReference.Add("GetOrgEmailDomains", (int)CommandTypes.ORGANIZATION * CommandOffset //FB 2154 
                                           + (int)Organization.GetOrgEmailDomains);
            CommandReference.Add("SaveEmailDomain", (int)CommandTypes.ORGANIZATION * CommandOffset //FB 2154
                                           + (int)Organization.SaveEmailDomain);

            CommandReference.Add("PurgeOrganization", (int)CommandTypes.ORGANIZATION * CommandOffset //FB 2074
                                           + (int)Organization.PurgeOrganization);
            
            CommandReference.Add("DeleteModuleLog", (int)CommandTypes.ORGANIZATION * CommandOffset//FB 2027
                                                          + (int)Organization.DeleteModuleLog);

            //FB 2052 - Start
            CommandReference.Add("GetHolidayType", (int)CommandTypes.ORGANIZATION * CommandOffset 
                                            + (int)Organization.GetHolidayType);
            CommandReference.Add("DeleteHolidayDetails", (int)CommandTypes.ORGANIZATION * CommandOffset  
                                            + (int)Organization.DeleteHolidayDetails);
            CommandReference.Add("SetDayOrderList", (int)CommandTypes.ORGANIZATION * CommandOffset  
                                            + (int)Organization.SetDayOrderList);
            //FB 2052 - End
            CommandReference.Add("GetOrgLicenseAgreement", (int)CommandTypes.ORGANIZATION * CommandOffset
                                            + (int)Organization.GetOrgLicenseAgreement); //FB 2337
            CommandReference.Add("SetOrgLicenseAgreement", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                        + (int)Organization.SetOrgLicenseAgreement); //FB 2337
            CommandReference.Add("SetOrgTextMsg", (int)CommandTypes.ORGANIZATION * CommandOffset
                                                        + (int)Organization.SetOrgTextMsg); //FB 2486
            CommandReference.Add("SetLDAPGroup", (int)CommandTypes.ORGANIZATION * CommandOffset // ZD 101525
                                                         + (int)Organization.SetLDAPGroup);
            CommandReference.Add("GetLDAPGroup", (int)CommandTypes.ORGANIZATION * CommandOffset // ZD 101525
                                                         + (int)Organization.GetLDAPGroup);
            CommandReference.Add("UpdateLDAPGroupRole", (int)CommandTypes.ORGANIZATION * CommandOffset // ZD 101525
                                                         + (int)Organization.UpdateLDAPGroupRole);

            CommandReference.Add("SetConfArchiveConfiguration", (int)CommandTypes.ORGANIZATION * CommandOffset // ZD 101835
                                                         + (int)Organization.SetConfArchiveConfiguration);

            CommandReference.Add("GetConfArchiveConfiguration", (int)CommandTypes.ORGANIZATION * CommandOffset // ZD 101835
                                                         + (int)Organization.GetConfArchiveConfiguration);
            CommandReference.Add("OrgOptionsInfo", (int)CommandTypes.ORGANIZATION * CommandOffset // ZD 103398
                                                         + (int)Organization.OrgOptionsInfo);
            
            //Room Factory Commands
            CommandReference.Add("GetRoomMonthlyView", (int)CommandTypes.RoomFactory * CommandOffset //FB 2027
                                            + (int)RoomFactory.GetRoomMonthlyView);
            CommandReference.Add("GetRoomWeeklyView", (int)CommandTypes.RoomFactory * CommandOffset //FB 2027
                                            + (int)RoomFactory.GetRoomWeeklyView);
            CommandReference.Add("GetRoomDailyView", (int)CommandTypes.RoomFactory * CommandOffset //FB 2027
                                            + (int)RoomFactory.GetRoomDailyView);
            CommandReference.Add("GetOldRoom", (int)CommandTypes.RoomFactory * CommandOffset //FB 2027
                                            + (int)RoomFactory.GetOldRoom);
			CommandReference.Add("ManageConfRoom", (int)CommandTypes.RoomFactory * CommandOffset //FB 2027
                                            + (int)RoomFactory.ManageConfRoom);
            CommandReference.Add("GetLastModifiedRoom", (int)CommandTypes.RoomFactory * CommandOffset //FB 2149
                                            + (int)RoomFactory.GetLastModifiedRoom);

			CommandReference.Add("DeleteRoom", (int)CommandTypes.RoomFactory * CommandOffset //FB 2027 DeleteRoom
                                            + (int)RoomFactory.DeleteRoom);
            CommandReference.Add("ActiveRoom", (int)CommandTypes.RoomFactory * CommandOffset //FB 2027 ActiveRoom
                                            + (int)RoomFactory.ActiveRoom);
            CommandReference.Add("GetServiceType", (int)CommandTypes.RoomFactory * CommandOffset //FB 2219
                                            + (int)RoomFactory.GetServiceType);
            CommandReference.Add("GetRoomDetails", (int)CommandTypes.RoomFactory * CommandOffset //FB 2361
                                            + (int)RoomFactory.GetRoomDetails);
            CommandReference.Add("GetRoomQueue", (int)CommandTypes.RoomFactory * CommandOffset //FB 2361
                                           + (int)RoomFactory.GetRoomQueue);
            //FB 2426 Start
            CommandReference.Add("SetGuesttoNormalRoom", (int)CommandTypes.RoomFactory * CommandOffset
                                           + (int)RoomFactory.SetGuesttoNormalRoom);
            CommandReference.Add("DeleteGuestRoom", (int)CommandTypes.RoomFactory * CommandOffset 
                                           + (int)RoomFactory.DeleteGuestRoom);
            //FB 2426 End

            //FB 2724 Start
            CommandReference.Add("GetRoomConferenceMonthlyView", (int)CommandTypes.RoomFactory * CommandOffset
                                           + (int)RoomFactory.GetRoomConferenceMonthlyView);
            CommandReference.Add("RoomValidation", (int)CommandTypes.RoomFactory * CommandOffset
                                          + (int)RoomFactory.RoomValidation);
            CommandReference.Add("GetModifiedConference", (int)CommandTypes.RoomFactory * CommandOffset
                                          + (int)RoomFactory.GetModifiedConference);
            //FB 2724 End
            //FB 2593
            CommandReference.Add("GetAllRoomsList", (int)CommandTypes.RoomFactory * CommandOffset
                                          + (int)RoomFactory.GetAllRoomsList);
            //ZD 101736 Start
            CommandReference.Add("GetRoomEWSDetails", (int)CommandTypes.RoomFactory * CommandOffset
                                          + (int)RoomFactory.GetRoomEWSDetails);
            CommandReference.Add("UpdateRoomEWSDetails", (int)CommandTypes.RoomFactory * CommandOffset
                                          + (int)RoomFactory.UpdateRoomEWSDetails);
            //ZD 101736 End
            //ZD 100196 
            CommandReference.Add("ChkRoomAuthentication", (int)CommandTypes.RoomFactory * CommandOffset
                                                            + (int)RoomFactory.ChkRoomAuthentication);
            //ZD 101527 Starts
            CommandReference.Add("GetSyncRooms", (int)CommandTypes.RoomFactory * CommandOffset
                                                           + (int)RoomFactory.GetSyncRooms);
            CommandReference.Add("SetSyncRooms", (int)CommandTypes.RoomFactory * CommandOffset
                                                           + (int)RoomFactory.SetSyncRooms);
            //ZD 102358
            CommandReference.Add("GetTopTiers", (int)CommandTypes.RoomFactory * CommandOffset
                                                           + (int)RoomFactory.GetTopTiers);
            CommandReference.Add("GetMiddleTiersByTopTier", (int)CommandTypes.RoomFactory * CommandOffset
                                                           + (int)RoomFactory.GetMiddleTiersByTopTier);
            CommandReference.Add("GetRoomsByTiers", (int)CommandTypes.RoomFactory * CommandOffset
                                                           + (int)RoomFactory.GetRoomsByTiers);


            //ZD 101527 Ends
            //ZD 102123 Starts
            CommandReference.Add("SetFloorPlan", (int)CommandTypes.RoomFactory * CommandOffset
                                                           + (int)RoomFactory.SetFloorPlan);
            CommandReference.Add("GetFloorPlans", (int)CommandTypes.RoomFactory * CommandOffset
                                                           + (int)RoomFactory.GetFloorPlans);
            CommandReference.Add("DeleteFloorPlan", (int)CommandTypes.RoomFactory * CommandOffset
                                                           + (int)RoomFactory.DeleteFloorPlan);
            //ZD 102123 Ends
            CommandReference.Add("GetIcontrolconference", (int)CommandTypes.RoomFactory * CommandOffset
                                                          + (int)RoomFactory.GetIcontrolconference); //ZD 103460
            CommandReference.Add("GetLocationsTier1", (int)CommandTypes.SEARCH * CommandOffset
                                                      + (int)SearchCommands.GetLocationsTier1); //ZD 101244

            CommandReference.Add("GetLocationsTier2", (int)CommandTypes.SEARCH * CommandOffset
                                                      + (int)SearchCommands.GetLocationsTier2); //ZD 101244

            CommandReference.Add("setRoomResizeImage", (int)CommandTypes.SEARCH * CommandOffset
                                                      + (int)SearchCommands.setRoomResizeImage);//ZD 101611


            //Image Factory Commands  FB 2136
            CommandReference.Add("GetAllSecImages", (int)CommandTypes.IMAGEFACTORY * CommandOffset
                                           + (int)ImageFactory.GetAllSecImages);
            CommandReference.Add("GetSecurityImage", (int)CommandTypes.IMAGEFACTORY * CommandOffset
                                            + (int)ImageFactory.GetSecurityImage);
            CommandReference.Add("SetSecurityImage", (int)CommandTypes.IMAGEFACTORY * CommandOffset
                                            + (int)ImageFactory.SetSecurityImage);
            CommandReference.Add("DeleteSecurityImage", (int)CommandTypes.IMAGEFACTORY * CommandOffset
                                            + (int)ImageFactory.DeleteSecurityImage);
            CommandReference.Add("GetSecImages", (int)CommandTypes.IMAGEFACTORY * CommandOffset
                                            + (int)ImageFactory.GetSecImages);
                     
        }
    }
}
